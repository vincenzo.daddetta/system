
"use strict";this.EXPORTED_SYMBOLS=["PerformanceStats"];const{classes:Cc,interfaces:Ci,utils:Cu}=Components;Cu.import("resource://gre/modules/XPCOMUtils.jsm",this);Cu.import("resource://gre/modules/Services.jsm",this);Cu.import("resource://gre/modules/Task.jsm",this);Cu.import("resource://gre/modules/ObjectUtils.jsm",this);XPCOMUtils.defineLazyModuleGetter(this,"PromiseUtils","resource://gre/modules/PromiseUtils.jsm");XPCOMUtils.defineLazyModuleGetter(this,"setTimeout","resource://gre/modules/Timer.jsm");XPCOMUtils.defineLazyModuleGetter(this,"clearTimeout","resource://gre/modules/Timer.jsm");
XPCOMUtils.defineLazyServiceGetter(this,"performanceStatsService","@mozilla.org/toolkit/performance-stats-service;1",Ci.nsIPerformanceStatsService);
XPCOMUtils.defineLazyServiceGetter(this,"finalizer","@mozilla.org/toolkit/finalizationwitness;1",Ci.nsIFinalizationWitnessService);
const FINALIZATION_TOPIC="performancemonitor-finalize";const PROPERTIES_META_IMMUTABLE=["addonId","isSystem","isChildProcess","groupId","processId"];const PROPERTIES_META=[...PROPERTIES_META_IMMUTABLE,"windowId","title","name"];const MAX_WAIT_FOR_CHILD_PROCESS_MS=5000;var isContent=Services.appinfo.processType==Services.appinfo.PROCESS_TYPE_CONTENT;function Probe(name,impl){this._name=name;this._counter=0;this._impl=impl;}
Probe.prototype={acquire:function(){if(this._counter==0){this._impl.isActive=true;Process.broadcast("acquire",[this._name]);}
this._counter++;},release:function(){this._counter--;if(this._counter==0){try{this._impl.isActive=false;}catch(ex){if(ex&&typeof ex=="object"&&ex.result==Components.results.NS_ERROR_NOT_AVAILABLE){return;}
throw ex;}
Process.broadcast("release",[this._name]);}},extract:function(xpcom){if(!this._impl.isActive){throw new Error(`Probe is inactive: ${this._name}`);}
return this._impl.extract(xpcom);},isEqual:function(a,b){if(a==null&&b==null){return true;}
if(a!=null&&b!=null){return this._impl.isEqual(a,b);}
return false;},subtract:function(a,b){if(a==null){throw new TypeError();}
if(b==null){return a;}
return this._impl.subtract(a,b);},importChildCompartments:function(parent,children){if(!Array.isArray(children)){throw new TypeError();}
if(!parent||!(parent instanceof PerformanceDataLeaf)){throw new TypeError();}
return this._impl.importChildCompartments(parent,children);},get name(){return this._name;},compose:function(stats){if(!Array.isArray(stats)){throw new TypeError();}
return this._impl.compose(stats);}};
function lastNonZero(array){for(let i=array.length-1;i>=0;--i){if(array[i]!=0){return i;}}
return-1;}
var Probes={jank:new Probe("jank",{set isActive(x){performanceStatsService.isMonitoringJank=x;},get isActive(){return performanceStatsService.isMonitoringJank;},extract:function(xpcom){let durations=xpcom.getDurations();return{totalUserTime:xpcom.totalUserTime,totalSystemTime:xpcom.totalSystemTime,totalCPUTime:xpcom.totalUserTime+xpcom.totalSystemTime,durations:durations,longestDuration:lastNonZero(durations)}},isEqual:function(a,b){ if(a.totalUserTime!=b.totalUserTime){return false;}
if(a.totalSystemTime!=b.totalSystemTime){return false;}
for(let i=0;i<a.durations.length;++i){if(a.durations[i]!=b.durations[i]){return false;}}
return true;},subtract:function(a,b){ let result={totalUserTime:a.totalUserTime-b.totalUserTime,totalSystemTime:a.totalSystemTime-b.totalSystemTime,totalCPUTime:a.totalCPUTime-b.totalCPUTime,durations:[],longestDuration:-1,};for(let i=0;i<a.durations.length;++i){result.durations[i]=a.durations[i]-b.durations[i];}
result.longestDuration=lastNonZero(result.durations);return result;},importChildCompartments:function(){},compose:function(stats){let result={totalUserTime:0,totalSystemTime:0,totalCPUTime:0,durations:[],longestDuration:-1};for(let stat of stats){result.totalUserTime+=stat.totalUserTime;result.totalSystemTime+=stat.totalSystemTime;result.totalCPUTime+=stat.totalCPUTime;for(let i=0;i<stat.durations.length;++i){result.durations[i]+=stat.durations[i];}
result.longestDuration=Math.max(result.longestDuration,stat.longestDuration);}
return result;}}),cpow:new Probe("cpow",{set isActive(x){performanceStatsService.isMonitoringCPOW=x;},get isActive(){return performanceStatsService.isMonitoringCPOW;},extract:function(xpcom){return{totalCPOWTime:xpcom.totalCPOWTime};},isEqual:function(a,b){return a.totalCPOWTime==b.totalCPOWTime;},subtract:function(a,b){return{totalCPOWTime:a.totalCPOWTime-b.totalCPOWTime};},importChildCompartments:function(){},compose:function(stats){let totalCPOWTime=0;for(let stat of stats){totalCPOWTime+=stat.totalCPOWTime;}
return{totalCPOWTime};},}),ticks:new Probe("ticks",{set isActive(x){},get isActive(){return true;},extract:function(xpcom){return{ticks:xpcom.ticks};},isEqual:function(a,b){return a.ticks==b.ticks;},subtract:function(a,b){return{ticks:a.ticks-b.ticks};},importChildCompartments:function(){},compose:function(stats){let ticks=0;for(let stat of stats){ticks+=stat.ticks;}
return{ticks};},}),compartments:new Probe("compartments",{set isActive(x){performanceStatsService.isMonitoringPerCompartment=x;},get isActive(){return performanceStatsService.isMonitoringPerCompartment;},extract:function(xpcom){return null;},isEqual:function(a,b){return true;},subtract:function(a,b){return true;},importChildCompartments:function(parent,children){parent.children=children;},compose:function(stats){return null;},}),};function PerformanceMonitor(probes){this._probes=probes; for(let probe of probes){probe.acquire();}


this._id=PerformanceMonitor.makeId();this._finalizer=finalizer.make(FINALIZATION_TOPIC,this._id)
PerformanceMonitor._monitors.set(this._id,probes);}
PerformanceMonitor.prototype={get probeNames(){return this._probes.map(probe=>probe.name);},_checkBeforeSnapshot:function(options){if(!this._finalizer){throw new Error("dispose() has already been called, this PerformanceMonitor is not usable anymore");}
let probes;if(options&&options.probeNames||undefined){if(!Array.isArray(options.probeNames)){throw new TypeError();} 
for(let probeName of options.probeNames){let probe=this._probes.find(probe=>probe.name==probeName);if(!probe){throw new TypeError(`I need probe ${probeName} but I only have ${this.probeNames}`);}
if(!probes){probes=[];}
probes.push(probe);}}else{probes=this._probes;}
return probes;},promiseContentSnapshot:function(options=null){this._checkBeforeSnapshot(options);return(new ProcessSnapshot(performanceStatsService.getSnapshot()));},promiseSnapshot:function(options=null){let probes=this._checkBeforeSnapshot(options);return Task.spawn(function*(){let childProcesses=yield Process.broadcastAndCollect("collect",{probeNames:probes.map(p=>p.name)});let xpcom=performanceStatsService.getSnapshot();return new ApplicationSnapshot({xpcom,childProcesses,probes,date:Cu.now()});});},dispose:function(){if(!this._finalizer){return;}
this._finalizer.forget();PerformanceMonitor.dispose(this._id);this._probes=null;this._id=null;this._finalizer=null;}};PerformanceMonitor._monitors=new Map();PerformanceMonitor.make=function(probeNames){ if(!Array.isArray(probeNames)){throw new TypeError("Expected an array, got "+probes);}
let probes=[];for(let probeName of probeNames){if(!(probeName in Probes)){throw new TypeError("Probe not implemented: "+probeName);}
probes.push(Probes[probeName]);}
return(new PerformanceMonitor(probes));};PerformanceMonitor.dispose=function(id){let probes=PerformanceMonitor._monitors.get(id);if(!probes){throw new TypeError("`dispose()` has already been called on this monitor");}
PerformanceMonitor._monitors.delete(id);for(let probe of probes){probe.release();}}

PerformanceMonitor._counter=0;PerformanceMonitor.makeId=function(){return"PerformanceMonitor-"+(this._counter++);}
Services.obs.addObserver(function(subject,topic,value){PerformanceMonitor.dispose(value);},FINALIZATION_TOPIC,false);this.PerformanceStats={getMonitor:function(probes){return PerformanceMonitor.make(probes);}};function PerformanceDataLeaf({xpcom,json,probes}){if(xpcom&&json){throw new TypeError("Cannot import both xpcom and json data");}
let source=xpcom||json;for(let k of PROPERTIES_META){this[k]=source[k];}
if(xpcom){for(let probe of probes){this[probe.name]=probe.extract(xpcom);}
this.isChildProcess=false;}else{for(let probe of probes){this[probe.name]=json[probe.name];}
this.isChildProcess=true;}
this.owner=null;}
PerformanceDataLeaf.prototype={equals:function(to){if(!(to instanceof PerformanceDataLeaf)){throw new TypeError();}
for(let probeName of Object.keys(Probes)){let probe=Probes[probeName];if(!probe.isEqual(this[probeName],to[probeName])){return false;}}
return true;},subtract:function(to=null){return(new PerformanceDiffLeaf(this,to));}};function PerformanceData(timestamp){this._parent=null;this._content=new Map();this._all=[];this._timestamp=timestamp;}
PerformanceData.prototype={addChild:function(stat){if(!(stat instanceof PerformanceDataLeaf)){throw new TypeError();}
if(!stat.isChildProcess){throw new TypeError();}
this._content.set(stat.groupId,stat);this._all.push(stat);stat.owner=this;},setParent:function(stat){if(!(stat instanceof PerformanceDataLeaf)){throw new TypeError();}
if(stat.isChildProcess){throw new TypeError();}
this._parent=stat;this._all.push(stat);stat.owner=this;},equals:function(to){if(this._parent&&!to._parent){return false;}
if(!this._parent&&to._parent){return false;}
if(this._content.size!=to._content.size){return false;}
if(this._parent&&!this._parent.equals(to._parent)){return false;}
for(let[k,v]of this._content){let v2=to._content.get(k);if(!v2){return false;}
if(!v.equals(v2)){return false;}}
return true;},subtract:function(to=null){return(new PerformanceDiff(this,to));},get addonId(){return this._all[0].addonId;},get title(){return this._all[0].title;}};function PerformanceDiff(current,old=null){this.addonId=current.addonId;this.title=current.title;this.windowId=current.windowId;this.deltaT=old?current._timestamp-old._timestamp:Infinity;this._all=[];if(current._parent){this._parent=old?current._parent.subtract(old._parent):current._parent;this._all.push(this._parent);this._parent.owner=this;}else{this._parent=null;}
this._content=new Map();for(let[k,stat]of current._content){let diff=stat.subtract(old?old._content.get(k):null);this._content.set(k,diff);this._all.push(diff);diff.owner=this;} 
for(let k of Object.keys(Probes)){if(!(k in this._all[0])){continue;}
let data=this._all.map(item=>item[k]);let probe=Probes[k];this[k]=probe.compose(data);}}
PerformanceDiff.prototype={toString:function(){return`[PerformanceDiff] ${this.key}`;},get windowIds(){return this._all.map(item=>item.windowId).filter(x=>!!x);},get groupIds(){return this._all.map(item=>item.groupId);},get key(){if(this.addonId){return this.addonId;}
if(this._parent){return this._parent.windowId;}
return this._all[0].groupId;},get names(){return this._all.map(item=>item.name);},get processes(){return this._all.map(item=>({isChildProcess:item.isChildProcess,processId:item.processId}));}};function PerformanceDiffLeaf(current,old=null){for(let k of PROPERTIES_META){this[k]=current[k];}
for(let probeName of Object.keys(Probes)){let other=null;if(old&&probeName in old){other=old[probeName];}
if(probeName in current){this[probeName]=Probes[probeName].subtract(current[probeName],other);}}}
function ProcessSnapshot({xpcom,probes}){this.componentsData=[];let subgroups=new Map();let enumeration=xpcom.getComponentsData().enumerate();while(enumeration.hasMoreElements()){let xpcom=enumeration.getNext().QueryInterface(Ci.nsIPerformanceStats);let stat=(new PerformanceDataLeaf({xpcom,probes}));if(!xpcom.parentId){this.componentsData.push(stat);}else{let siblings=subgroups.get(xpcom.parentId);if(!siblings){subgroups.set(xpcom.parentId,(siblings=[]));}
siblings.push(stat);}}
for(let group of this.componentsData){for(let probe of probes){probe.importChildCompartments(group,subgroups.get(group.groupId)||[]);}}
this.processData=(new PerformanceDataLeaf({xpcom:xpcom.getProcessData(),probes}));}
function ApplicationSnapshot({xpcom,childProcesses,probes,date}){ProcessSnapshot.call(this,{xpcom,probes});this.addons=new Map();this.webpages=new Map();this.date=date; for(let{componentsData}of(childProcesses||[])){for(let json of componentsData){let leaf=(new PerformanceDataLeaf({json,probes}));this.componentsData.push(leaf);}}
for(let leaf of this.componentsData){let key,map;if(leaf.addonId){key=leaf.addonId;map=this.addons;}else if(leaf.windowId){key=leaf.windowId;map=this.webpages;}else{continue;}
let combined=map.get(key);if(!combined){combined=new PerformanceData(date);map.set(key,combined);}
if(leaf.isChildProcess){combined.addChild(leaf);}else{combined.setParent(leaf);}}}
var Process={ _idcounter:0,_loader:null,get loader(){if(isContent){return null;}
if(this._loader){return this._loader;}
Services.ppmm.loadProcessScript("resource://gre/modules/PerformanceStats-content.js",true );return this._loader=Services.ppmm;},broadcast:function(topic,payload){if(!this.loader){return;}
this.loader.broadcastAsyncMessage("performance-stats-service-"+topic,{payload});},broadcastAndCollect:Task.async(function*(topic,payload){if(!this.loader||this.loader.childCount==1){return undefined;}
const TOPIC="performance-stats-service-"+topic;let id=this._idcounter++;
let expecting=this.loader.childCount;let collected=[];let deferred=PromiseUtils.defer();let observer=function({data,target}){if(data.id!=id){return;}
if(data.data){collected.push(data.data)}
if(--expecting>0){return;}
deferred.resolve();};this.loader.addMessageListener(TOPIC,observer);this.loader.broadcastAsyncMessage(TOPIC,{id,payload});
let timeout=setTimeout(()=>{if(expecting==0){return;}
deferred.resolve();},MAX_WAIT_FOR_CHILD_PROCESS_MS);deferred.promise.then(()=>{clearTimeout(timeout);});yield deferred.promise;this.loader.removeMessageListener(TOPIC,observer);return collected;})};