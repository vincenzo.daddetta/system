'use strict';
const DEBUG = 1;
var debug = function(s) {
    if (DEBUG) {
        dump('<KAIOS_LOG> -*- engmodeEx_service -*-: ' + s + '\n');
    }
};
const Cu = Components.utils;
const Cc = Components.classes;
const Ci = Components.interfaces;
const Cr = Components.results;
this.EXPORTED_SYMBOLS = ['EngmodeService'];
Cu.import('resource://gre/modules/XPCOMUtils.jsm');
Cu.import('resource://gre/modules/Services.jsm');
Cu.import('resource://gre/modules/FileUtils.jsm');
Cu.import('resource://gre/modules/NetUtil.jsm');
var HEADSET_KEY = 'Insert detect insert = ';
XPCOMUtils.defineLazyGetter(this, 'libcutils', function() {
    Cu.import('resource://gre/modules/systemlibs.js');
    return libcutils;
});
XPCOMUtils.defineLazyServiceGetter(this, 'ppmm', '@mozilla.org/parentprocessmessagemanager;1', 'nsIMessageListenerManager');
XPCOMUtils.defineLazyServiceGetter(this, 'gRil', '@mozilla.org/ril;1', 'nsIRadioInterfaceLayer');
const nsIAudioManager = Ci.nsIAudioManager;
XPCOMUtils.defineLazyGetter(this, 'gAudioManager', function getAudioManager() {
    try {
        return Cc['@mozilla.org/telephony/audiomanager;1'].getService(nsIAudioManager);
    } catch (ex) {
        return null;
    }
});
this.EngmodeService = {
    _getfusest: null,
    init: function() {
        debug('EngmodeService Initialize ....');
        this._messages = ['EngmodeSrv:Common', 'EngmodeSrv:HookHeadsetStatusChangedEvt', 'EngmodeSrv:HookKeyEvt', 'EngmodeSrv:RunProcess', 'EngmodeSrv:GetFilesLastTime', 'EngmodeSrv:CopyFile', 'EngmodeSrv:CreateDirOrFile', 'EngmodeSrv:AudioLoop', 'EngmodeSrv:ForceInCall', 'EngmodeSrv:UniversalCommand', 'EngmodeSrv:GpsTest', 'EngmodeSrv:CheckIsCommandRunnig', 'EngmodeSrv:ReadNvitemRoot', 'EngmodeSrv:ReadNvitem', 'EngmodeSrv:WriteNvitem', 'EngmodeSrv:ReadNvitemEx', 'EngmodeSrv:WriteNvitemEx', 'EngmodeSrv:SetChargingEnabled', 'EngmodeSrv:SetDbClickUnlock', 'EngmodeSrv:SetChargerLED', 'EngmodeSrv:SetCameraLED', 'EngmodeSrv:setSubLCD', 'EngmodeSrv:setKeypadLED', 'EngmodeSrv:FileWrite', 'EngmodeSrv:SetDirProp', 'EngmodeSrv:setDataProfileByType', 'EngmodeSrv:setInitAttachProfile', 'EngmodeSrv:RemoveBandFromPriorityList', 'EngmodeSrv:AddBandToPriorityList', 'EngmodeSrv:EnableBand41TxASDiv', 'EngmodeSrv:IsBand41TxASDivEnabled', 'EngmodeSrv:ReadDebugInfo', 'EngmodeSrv:SetDdtmStatus', 'EngmodeSrv:GetDdtmStatus', 'EngmodeSrv:SetPropertyValue', 'EngmodeSrv:GetFusest'];
        this._messages.forEach((function(msgName) {
            ppmm.addMessageListener(msgName, this);
        }).bind(this));
        this._keyEvtTarget = null;
        this._headsetEvtTarget = null;
        this._audioLoopProcess = null;
        this._gpsTestProcess = null;
        this._universalCommandProcess = null;
    },
    get isKeyHooked() {
        debug('isKeyHooked = ' + (this._keyEvtTarget !== null));
        return (this._keyEvtTarget !== null);
    },
    processKeyEvt: function(evt) {
        debug('processKeyEvt');
        if (this._keyEvtTarget !== null) {
            debug('processKeyEvt: Send key event ' + JSON.stringify(evt));
            this._keyEvtTarget.sendAsyncMessage('EngmodeSrv:KeyEvent:Return', {
                event: evt
            });
        } else {
            debug('processKeyEvt: No responsibler for key evt processing');
        }
    },
    get isHeadsetStatusChangedHooked() {
        debug('isHeadsetStatusChangedHooked');
        return (this._headsetEvtTarget !== null);
    },
    processHeadsetStatusChangedEvt: function(evt) {
        debug('processHeadsetStatusChangedEvt');
        if (this._headsetEvtTarget !== null) {
            this._headsetEvtTarget.sendAsyncMessage('EngmodeSrv:HeadsetStatusChangedEvt:Return', {
                event: evt
            });
        }
    },
    _createGivenDirOrFile: function(path, typeStr, shouldCreate, callback) {
        debug('_createGivenDirOrFile--path:' + path);
        let file = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
        file.initWithPath(path);
        if (file.exists()) {
            callback('EXIST');
        } else if (shouldCreate) {
            let temp = 'CREATE_SUCCESS';
            try {
                if (typeStr === 'DIRECTORY') {
                    file.create(Ci.nsIFile.DIRECTORY_TYPE, FileUtils.PERMS_DIRECTORY);
                } else {
                    file.create(Ci.nsIFile.NORMAL_FILE_TYPE, FileUtils.PERMS_FILE);
                }
            } catch (e) {
                debug('create file fail: ' + e);
                temp = 'CREATE_FAILED';
            } finally {
                callback(temp);
            }
        } else {
            callback('NO_EXIST');
        }
    },
    _checkIsCommandRunning: function(commands, callback) {
        debug('_checkIsCommandRunning' + ' commands: ' + commands);
        let is_running = false;
        if (commands in this._sysRunningSrv) {
            is_running = true;
        }
        callback(is_running);
    },
    _strCompare: function(str1, str2) {
        let prop = /[^\*]\S+[^\*]/;
        let ret = prop.exec(str1);
        let tempStr = ret[0];
        let result = false;
        if (str1 && str2) {
            if (str1[0] === '*' && str1[str1.length - 1] === '*') {
                if (str2.indexof(tempStr) !== -1) {
                    result = true;
                }
            } else if (str1[str1.length - 1] === '*') {
                if (str2.length >= tempStr.length && tempStr === str2.substr(0, tempStr.length)) {
                    result = true;
                }
            } else if (str1[0] === '*') {
                if (str2.length >= tempStr.length && tempStr === str2.substr(str2.length - tempStr.length, tempStr.length)) {
                    result = true;
                }
            } else {
                if (tempStr === str2) {
                    result = true;
                }
            }
        }
        return result;
    },
    _readSysFile: function(filePath, callback) {
        let file = Cc['@mozilla.org/file/local;1'].createInstance(Ci.nsILocalFile);
        debug('_readSysFile entry.');
        file.initWithPath(filePath);
        NetUtil.asyncFetch(file, function(inputStream, status) {
            let temp = {};
            if (!Components.isSuccessCode(status)) {
                debug('_readSysFile:failed');
                temp.result = 'KO';
                temp.data = status;
            } else {
                debug('_readSysFile:success');
                let data = NetUtil.readInputStreamToString(inputStream, inputStream.available());
                temp.result = 'OK';
                temp.data = data;
            }
            callback(temp);
        });
    },
    _sysRunningSrv: {},
    _sysService: function(command, callback) {
        debug('sysService: ' + command);
        let pattern = /(start|stop)\s+(\S+)\s+([^>]*)(?:>\s*(\S+))?/;
        let result;
        let cbData = {};
        if (result = pattern.exec(command)) {
            let start = (result[1] === 'start') ? true : false;
            let program = result[2];
            let argument = result[3];
            let output = result[4];
            if (start === true) {
                if (program in this._sysRunningSrv && program !== '/system/bin/diag_mdlog') {
                    debug('The servcie ' + program + ' is still running');
                    return;
                }
                let file = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
                let process = Components.classes['@mozilla.org/process/util;1'].createInstance(Components.interfaces.nsIProcess);
                let args = [];
                if (output) {
                    debug('program, Arguments and output: ' + program + ' ' + argument + ' >' + output);
                    file.initWithPath('/system/bin/sh');
                    args.push('-c', program + ' ' + argument + ' >' + output);
                } else {
                    debug('program, Arguments: ' + program + ' ' + argument);
                    file.initWithPath(program);
                    args = argument.trim().split(/\s+/);
                }
                try {
                    process.init(file);
                    let that = this;
                    process.runAsync(args, args.length, {
                        observe: function(aSubject, aTopic, aData) {
                            debug('observer: ' + aSubject + ':' +
                                aTopic + ':' + aData + ':' + program);
                            for (let name in that._sysRunningSrv) {
                                if (that._sysRunningSrv[name] === process) {
                                    debug('!!!Remove ' + name + ' from running service');
                                    delete that._sysRunningSrv[name];
                                    break;
                                }
                            }
                            cbData['processState'] = aTopic;
                            callback(cbData);
                        }
                    });
                    this._sysRunningSrv[program] = process;
                    cbData['processState'] = 'process-running';
                    callback(cbData);
                } catch (e) {
                    cbData['processState'] = 'process-failed';
                    cbData['errorType'] = 'DefaultError';
                    callback(cbData);
                }
            } else {
                if (program in this._sysRunningSrv) {
                    debug('Stopping service ' + program);
                    cbData['processState'] = 'process-finished';
                    callback(cbData);
                    this._sysRunningSrv[program].kill();
                }
            }
        }
    },
    _copyFile: function(sourcePath, destPath, fileNewName, callback) {
        debug('copyFile: ' + sourcePath + ' destPath: ' + destPath + ' fileName:' + fileNewName);
        let sourcefile = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
        sourcefile.initWithPath(sourcePath);
        let destfile = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
        destfile.initWithPath(destPath);
        let cbData = {};
        cbData['processState'] = 'process-finished';
        try {
            sourcefile.copyTo(destfile, fileNewName);
        } catch (e) {
            debug('error: ' + e.result);
            cbData['processState'] = 'process-failed';
            if (e.result == Cr.NS_ERROR_FILE_TARGET_DOES_NOT_EXIST) {
                cbData['errorType'] = 'SourceFileNotExist';
            } else {
                cbData['errorType'] = 'DefaultError';
            }
        } finally {
            callback(cbData);
        }
    },
    _getFilesLastTime: function(aFilePathArray, callback) {
        debug('_getFilesLastTime aFilePathArray: ' + aFilePathArray.toString());
        let pattern = /(\/(\S+)\/)([^\/]+)/;
        let i = 0;
        let obj = {};
        let pathObj = {};
        for (i = 0; i < aFilePathArray.length; i++) {
            let result = pattern.exec(aFilePathArray[i]);
            if (result) {
                if (!(result[1] in pathObj)) {
                    pathObj[result[1]] = [];
                }
                pathObj[result[1]].push(result[3]);
                let prop = /[^\*]\S+[^\*]/;
                let ret = prop.exec(result[3]);
                obj[ret[0]] = {
                    time: 0
                };
            }
        }
        debug(JSON.stringify(obj));
        debug(JSON.stringify(pathObj));
        for (let directoryPath in pathObj) {
            let directory = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
            directory.initWithPath(directoryPath);
            if (directory.exists()) {
                let fileList = directory.directoryEntries;
                let fileNameAarry = pathObj[directoryPath];
                while (fileList.hasMoreElements()) {
                    let file = fileList.getNext().QueryInterface(Components.interfaces.nsILocalFile);
                    for (i = 0; i < fileNameAarry.length; i++) {
                        if (this._strCompare(fileNameAarry[i], file.leafName)) {
                            let prop = /[^\*]\S+[^\*]/;
                            let ret = prop.exec(fileNameAarry[i]);
                            if (obj[ret[0]].time < file.lastModifiedTime) {
                                obj[ret[0]].time = file.lastModifiedTime;
                                obj[ret[0]].location = directoryPath;
                            }
                        }
                    }
                }
            }
        }
        callback(obj);
    },
    _readNvitem: function(item, callback) {
        debug("engmode_service.jsm _readNvitem: enter");
        let obj = {};
        let engmodeNvAccess = Components.classes['@kaiostech.com/JrdNvAccess;1'].createInstance(Components.interfaces.nsIJrdNvAccess);
        try {
            obj.data = engmodeNvAccess.readNvitem(item);
            obj.result = 'OK';
            debug("engmode_service.jsm _readNvitem: size: " + obj.data.length + ", value: " + obj.data.toString());
        } catch (e) {
            obj.result = 'KO';
            debug("read nvram fail: " + e.result);
            obj.data = e.result;
        }
        if (callback !== null) {
            callback(obj);
        }
        return obj;
    },
    _readNvitemRoot: function(item, callback) {
        debug("engmode_service.jsm _readNvitem: enter");
        let obj = {};
        let engmodeNvAccess = Components.classes['@kaiostech.com/JrdNvAccess;1'].createInstance(Components.interfaces.nsIJrdNvAccess);
        try {
            obj.data = engmodeNvAccess.readNvitemRoot(item);
            obj.result = 'OK';
            debug("engmode_service.jsm _readNvitemRoot: size: " + obj.data.length + ", value: " + obj.data.toString());
        } catch (e) {
            obj.result = 'KO';
            debug("read nvram fail: " + e.result);
            obj.data = e.result;
        }
        if (callback !== null) {
            callback(obj);
        }
        return obj;
    },
    _eraseAPN: function(apnno, callback) {
        debug("engmode_service.jsm _eraseAPN: enter");
        let obj = {};
        let engmodeNvAccess = Components.classes['@kaiostech.com/JrdNvAccess;1'].createInstance(Components.interfaces.nsIJrdNvAccess);
        try {
            obj.data = engmodeNvAccess.eraseAPN(apnno);
            debug("_eraseAPN ok");
            obj.result = 'OK';
        } catch (e) {
            obj.result = 'KO';
            debug("erase APN fail: " + e.result);
            obj.data = e.result;
        }
        if (callback !== null) {
            callback(obj);
        }
        return obj;
    },
    _RemoveBandFromPriorityList: function(band, callback) {
        debug("engmode_service.jsm _RemoveBandFromPriorityList: enter");
        let obj = {};
        let engmodeNvAccess = Components.classes['@kaiostech.com/JrdNvAccess;1'].createInstance(Components.interfaces.nsIJrdNvAccess);
        try {
            obj.data = engmodeNvAccess.RemoveBandFromPriorityList(band);
            debug("_RemoveBandFromPriorityList ok");
            obj.result = 'OK';
        } catch (e) {
            obj.result = 'KO';
            debug("RemoveBandFromPriorityList fail: " + e.result);
            obj.data = e.result;
        }
        if (callback !== null) {
            callback(obj);
        }
        return obj;
    },
    _AddBandToPriorityList: function(band, callback) {
        debug("engmode_service.jsm _AddBandToPriorityList: enter");
        let obj = {};
        let engmodeNvAccess = Components.classes['@kaiostech.com/JrdNvAccess;1'].createInstance(Components.interfaces.nsIJrdNvAccess);
        try {
            obj.data = engmodeNvAccess.AddBandToPriorityList(band);
            debug("_AddBandToPriorityList ok");
            obj.result = 'OK';
        } catch (e) {
            obj.result = 'KO';
            debug("AddBandToPriorityList fail: " + e.result);
            obj.data = e.result;
        }
        if (callback !== null) {
            callback(obj);
        }
        return obj;
    },
    _isBand41TxASDivEnabled(callback) {
        debug("engmode_service.jsm _isBand41TxASDivEnabled: enter");
        let obj = {};
        let engmodeNvAccess = Components.classes['@kaiostech.com/JrdNvAccess;1'].createInstance(Components.interfaces.nsIJrdNvAccess);
        try {
            obj.data = engmodeNvAccess.isBand41TxASDivEnabled();
            debug("_eraseAPN ok");
            obj.result = 'OK';
        } catch (e) {
            obj.result = 'KO';
            debug("_isBand41TxASDivEnabled fail: " + e.result);
            obj.data = e.result;
        }
        if (callback !== null) {
            callback(obj);
        }
        return obj;
    },
    _enableBand41TxASDiv: function(enable, callback) {
        debug("engmode_service.jsm _enableBand41TxASDiv: enter");
        let obj = {};
        let engmodeNvAccess = Components.classes['@kaiostech.com/JrdNvAccess;1'].createInstance(Components.interfaces.nsIJrdNvAccess);
        try {
            obj.data = engmodeNvAccess.enableBand41TxASDiv(enable);
            debug("_enableBand41TxASDiv ok");
            obj.result = 'OK';
        } catch (e) {
            obj.result = 'KO';
            debug("_enableBand41TxASDiv fail: " + e.result);
            obj.data = e.result;
        }
        if (callback !== null) {
            callback(obj);
        }
        return obj;
    },
    _setInitAttachProfile: function(apn, protocol, authtype, user, password, callback) {
        debug('engmode_service.jsm cxk _setInitAttachProfile: enter');
        let obj = {};
        try {
            debug("[authtype:" + authtype + "][apn:" + apn + "][protocol:" + protocol + "][user:" + user + "][password:" + password + "]");
            let radioInterface = gRil.getRadioInterface(0);
            obj.data = radioInterface.SetInitAttachAPN(apn, protocol, authtype, user, password);
            debug("_setInitAttachProfile ok");
            obj.result = 'OK';
        } catch (e) {
            obj.result = 'KO';
            debug("_setInitAttachProfile fail: " + e.result);
            obj.data = e.result;
        }
        if (callback !== null) {
            callback(obj);
        }
        return obj;
    },
    _setDataProfileByType: function(networkType, profileId, apn, protocol, authtype, user, password, types, maxConnsTime, maxConns, waitTime, enabled, inactivityTimer, callback) {
        debug('engmodeEx_service.jsm _setDataProfileByType: enter');
        let obj = {};
        try {
            debug('[aNetworkType:' + networkType + '][aProfileId:' + profileId + '][authtype:' + authtype + '][types:' + types + '][maxConnsTime:' + maxConnsTime + '][maxConns:' + maxConns + '][waitTime:' + waitTime + '][enabled:' + enabled + '][apn:' + apn + '][protocol:' + protocol + '][user:' + user + '][password:' + password + ']');
            let radioInterface = gRil.getRadioInterface(0);
            obj.data = radioInterface.SetDataProfileByType(networkType, profileId, apn, protocol, authtype, user, password, types, maxConnsTime, maxConns, waitTime, enabled, inactivityTimer);
            debug('_setDataProfileByType ok');
            obj.result = 'OK';
        } catch (e) {
            obj.result = 'KO';
            debug('_setDataProfileByType fail: ' + e.result);
            obj.data = e.result;
        }
        if (callback !== null) {
            callback(obj);
        }
        return obj;
    },
    _setDirProp: function(path, status, callback) {
        debug("engmode_service.jsm _setDirProp: enter");
        let obj = {};
        let i = 0;
        let aLength = path.length;
        let array = new Array(aLength);
        let engmodeNvAccess = Components.classes['@kaiostech.com/JrdNvAccess;1'].createInstance(Components.interfaces.nsIJrdNvAccess);
        try {
            for (i = 0; i < aLength; i++) {
                let num = path.charCodeAt(i);
                array[i] = num;
            }
            obj.data = engmodeNvAccess.setDirProp(status, aLength, array);
            debug("_setDirProp ok");
            obj.result = 'OK';
        } catch (e) {
            obj.result = 'KO';
            debug("_setDirProp fail: " + e.result);
            obj.data = e.result;
        }
        if (callback !== null) {
            callback(obj);
        }
        return obj;
    },
    _readNvitemEx: function(item, callback) {
        debug("engmode_service.jsm _readNvitemEx: enter");
        let obj = {};
        let engmodeNvAccess = Components.classes['@kaiostech.com/JrdNvAccess;1'].createInstance(Components.interfaces.nsIJrdNvAccess);
        try {
            obj.data = engmodeNvAccess.readNvitemEx(item);
            obj.result = 'OK';
            debug("engmode_service.jsm _readNvitemEx: size: " + obj.data.length + ", value: " + obj.data.toString());
        } catch (e) {
            obj.result = 'KO';
            debug("read nvramEx fail: " + e.result);
            obj.data = e.result;
        }
        if (callback !== null) {
            callback(obj);
        }
        return obj;
    },
    _writeNvitem: function(item, length, value, callback) {
        let jrdNvAccess = Components.classes['@kaiostech.com/JrdNvAccess;1'].createInstance(Components.interfaces.nsIJrdNvAccess);
        debug("jrdnvram write item = " + item + "value = " + value);
        jrdNvAccess.writeNvitem(item, length, value);
        debug("jrdnvram: write finished");
        if (callback !== null) {
            callback();
        }
    },
    _writeNvitemEx: function(item, length, value, callback) {
        let jrdNvAccess = Components.classes['@kaiostech.com/JrdNvAccess;1'].createInstance(Components.interfaces.nsIJrdNvAccess);
        debug("jrdnvram writeEx item = " + item + "value = " + value);
        jrdNvAccess.writeNvitemEx(item, length, value);
        debug("jrdnvram: writeEx finished");
        if (callback !== null) {
            callback();
        }
    },
    _setchargerled: function(config) {
        try {
            let file = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
            file.initWithPath('/sys/class/ktd20xx/ktd2026/led');
            var foStream = Components.classes['@mozilla.org/network/file-output-stream;1'].createInstance(Components.interfaces.nsIFileOutputStream);
            foStream.init(file, 0x02 | 0x08 | 0x20, '0666', 0);
            var converter = Components.classes['@mozilla.org/intl/converter-output-stream;1'].createInstance(Components.interfaces.nsIConverterOutputStream);
            converter.init(foStream, 'UTF-8', 0, 0);
            converter.writeString(config);
            converter.close();
        } catch (ex) {
            debug('setchargerled io error! ex = ' + ex);
        }
    },
    _setcameraled: function(config) {
        try {
            let file = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
            file.initWithPath('/sys/class/leds/flashlight/brightness');
            var foStream = Components.classes['@mozilla.org/network/file-output-stream;1'].createInstance(Components.interfaces.nsIFileOutputStream);
            foStream.init(file, 0x02 | 0x08 | 0x20, '0666', 0);
            var converter = Components.classes['@mozilla.org/intl/converter-output-stream;1'].createInstance(Components.interfaces.nsIConverterOutputStream);
            converter.init(foStream, 'UTF-8', 0, 0);
            converter.writeString(config);
            converter.close();
        } catch (ex) {
            debug('setcaeraled io error! ex = ' + ex);
        }
    },
    _setsublcd: function(config) {
        try {
            let file = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
            file.initWithPath('/sys/class/ktd20xx/ktd2026/back_light_led');
            var foStream = Components.classes['@mozilla.org/network/file-output-stream;1'].createInstance(Components.interfaces.nsIFileOutputStream);
            foStream.init(file, 0x02 | 0x08 | 0x20, '0666', 0);
            var converter = Components.classes['@mozilla.org/intl/converter-output-stream;1'].createInstance(Components.interfaces.nsIConverterOutputStream);
            converter.init(foStream, 'UTF-8', 0, 0);
            converter.writeString(config);
            converter.close();
        } catch (ex) {
            debug('setcaeraled io error! ex = ' + ex);
        }
    },
    _setkeypadled: function(config) {
        try {
            let file = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
            file.initWithPath('/sys/class/leds/keyboard-backlight/brightness');
            var foStream = Components.classes['@mozilla.org/network/file-output-stream;1'].createInstance(Components.interfaces.nsIFileOutputStream);
            foStream.init(file, 0x02 | 0x08 | 0x20, '0666', 0);
            var converter = Components.classes['@mozilla.org/intl/converter-output-stream;1'].createInstance(Components.interfaces.nsIConverterOutputStream);
            converter.init(foStream, 'UTF-8', 0, 0);
            converter.writeString(config);
            converter.close();
        } catch (ex) {
            debug('setcaeraled io error! ex = ' + ex);
        }
    },
    _setchargingenabled: function(config) {
        try {
            let file = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
            file.initWithPath('/sys/class/power_supply/battery/charging_enabled');
            var foStream = Components.classes['@mozilla.org/network/file-output-stream;1'].createInstance(Components.interfaces.nsIFileOutputStream);
            foStream.init(file, 0x02 | 0x08 | 0x20, '0666', 0);
            var converter = Components.classes['@mozilla.org/intl/converter-output-stream;1'].createInstance(Components.interfaces.nsIConverterOutputStream);
            converter.init(foStream, 'UTF-8', 0, 0);
            converter.writeString(config);
            converter.close();
        } catch (ex) {
            debug('setchargingenabled io error! ex = ' + ex);
        }
    },
    _setdbclickunlock: function(config) {
        try {
            let file = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
            file.initWithPath('/sys/class/TP-UNLOCK/device/dbclick');
            var foStream = Components.classes['@mozilla.org/network/file-output-stream;1'].createInstance(Components.interfaces.nsIFileOutputStream);
            foStream.init(file, 0x02 | 0x08 | 0x20, '0666', 0);
            var converter = Components.classes['@mozilla.org/intl/converter-output-stream;1'].createInstance(Components.interfaces.nsIConverterOutputStream);
            converter.init(foStream, 'UTF-8', 0, 0);
            converter.writeString(config);
            converter.close();
        } catch (ex) {
            debug('setdbclickunlock io error! ex = ' + ex);
        }
    },
    _setPropertyValue: function(name, value) {
        debug('setPropertyValue name is ' + name + ', value is ' + value);
        try {
            libcutils.property_set(name, value);
        } catch (e) {
            debug('There are some errors : ' + e);
        }
    },
    _filewrite: function(str, path, par, callback) {
        debug('engmodeEx_service.jsm _filewrite: enter');
        let obj = {};
        try {
            let file = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
            file.initWithPath(path);
            var foStream = Components.classes['@mozilla.org/network/file-output-stream;1'].createInstance(Components.interfaces.nsIFileOutputStream);
            if ('a' == par) {
                debug('gaolu engmodeEx_service.jsm _filewrite: enter par = a');
                foStream.init(file, 0x02 | 0x08 | 0x10, '0644', 0);
            } else if ('f' == par) {
                debug('gaolu engmodeEx_service.jsm _filewrite: enter par = f');
                foStream.init(file, 0x02 | 0x08 | 0x20, '0644', 0);
            }
            var converter = Components.classes['@mozilla.org/intl/converter-output-stream;1'].createInstance(Components.interfaces.nsIConverterOutputStream);
            converter.init(foStream, 'UTF-8', 0, 0);
            converter.writeString(str);
            converter.close();
            obj.data = path + ': ' + str;
            obj.result = 'OK';
        } catch (ex) {
            debug('_filewrite io error! ex = ' + ex);
            obj.data = path + ': ' + str;
            obj.result = 'KO';
        }
        if (callback !== null) {
            callback(obj);
        }
        return obj;
    },
    _returnGetFuse: function(value) {
        debug('_returnOMADMCfge is ' + value);
        this._getfusest.sendAsyncMessage('EngmodeSrv:GetFusest:Return', {
            result: value
        });
    },
    _getFusestate: function() {
        debug("engmode_service.jsm _OpenFuse: enter");
        let engmodeGetFuse = Components.classes['@kaiostech.com/JrdNvAccess;1'].createInstance(Components.interfaces.nsIJrdNvAccess);
        try {
            engmodeGetFuse.getfusestate(this._returnGetFuse.bind(this));
            debug("engmode_service.jsm _OpenFuse, value: ");
        } catch (e) {
            debug("_OpenFuse fail: " + e.result);
        }
    },
    receiveMessage: function(aMessage) {
        debug('receiveMessage: ' + aMessage.name);
        let mm = aMessage.target;
        let msg = aMessage.data;
        switch (aMessage.name) {
            case 'EngmodeSrv:Common':
                switch (msg.type) {
                    case 'MAC_ADDR':
                        this._readSysFile('/system/etc/wifi/macaddr', function(data) {
                            mm.sendAsyncMessage('EngmodeSrv:Common:Return', {
                                data: data,
                                requestID: msg.requestID
                            });
                        });
                        break;
                    case 'BATTERY_TEMP':
                        this._readSysFile('/sys/class/power_supply/battery/temp', function(data) {
                            mm.sendAsyncMessage('EngmodeSrv:Common:Return', {
                                data: data,
                                requestID: msg.requestID
                            });
                        });
                        break;
                    case 'BATTERY_ONLINE':
                        this._readSysFile('/sys/class/power_supply/usb/online', function(data) {
                            mm.sendAsyncMessage('EngmodeSrv:Common:Return', {
                                data: data,
                                requestID: msg.requestID
                            });
                        });
                        break;
                    case 'USB_CONNECT':
                        this._readSysFile('/sys/devices/virtual/android_usb/android0/state', function(data) {
                            if (data.result === 'OK') {
                                let s = data.data.match('CONFIGURED');
                                if (s == 'CONFIGURED') {
                                    data.data = true;
                                } else {
                                    data.data = false;
                                }
                                mm.sendAsyncMessage('EngmodeSrv:Common:Return', {
                                    data: data,
                                    requestID: msg.requestID
                                });
                            } else {
                                mm.sendAsyncMessage('EngmodeSrv:Common:Return', {
                                    data: data,
                                    requestID: msg.requestID
                                });
                            }
                        });
                        break;
                    case 'HEADPHONE_STATE':
                        this._readSysFile('/sys/kernel/debug/wcd9xxx_mbhc', function(data) {
                            if (data.result === 'OK') {
                                var index = data.data.indexOf(HEADSET_KEY);
                                var result = data.data.substring(index + HEADSET_KEY.length);
                                if (result == 1) {
                                    data.data = 'on';
                                } else {
                                    data.data = 'off';
                                }
                                mm.sendAsyncMessage('EngmodeSrv:Common:Return', {
                                    data: data,
                                    requestID: msg.requestID
                                });
                            } else {
                                mm.sendAsyncMessage('EngmodeSrv:Common:Return', {
                                    data: data,
                                    requestID: msg.requestID
                                });
                            }
                        });
                        break;
                    case 'RAM':
                        this._readSysFile('/data/ram_memory', function(data) {
                            mm.sendAsyncMessage('EngmodeSrv:Common:Return', {
                                data: data,
                                requestID: msg.requestID
                            });
                        });
                        break;
                    case 'RECONDITIONED':
                        this._readSysFile('/dev/block/platform/soc.0/7824900.sdhci/by-name/traceability', function(data) {
                            mm.sendAsyncMessage('EngmodeSrv:Common:Return', {
                                data: data,
                                requestID: msg.requestID
                            });
                        });
                        break;
                    default:
                        debug('receiveMessage: <Engmode:Common> can not find type ' +
                            JSON.stringify(msg.type));
                        break;
                }
                break;
            case 'EngmodeSrv:HookKeyEvt':
                if (msg.enable === true) {
                    debug('enabling key hook');
                    this._keyEvtTarget = mm;
                } else {
                    debug('disabling key hook');
                    this._keyEvtTarget = null;
                }
                break;
            case 'EngmodeSrv:HookHeadsetStatusChangedEvt':
                if (msg.enable === true) {
                    this._headsetEvtTarget = mm;
                } else {
                    this._headsetEvtTarget = null;
                }
                break;
            case 'EngmodeSrv:RunProcess':
                this._sysService(msg.command, function(processState) {
                    mm.sendAsyncMessage('EngmodeSrv:RunProcess:Return', {
                        data: processState,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:GetFilesLastTime':
                this._getFilesLastTime(msg.aFilePathArray, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:GetFilesLastTime:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:CopyFile':
                this._copyFile(msg.sourcePath, msg.directoryPath, msg.fileNewName, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:CopyFile:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:CreateDirOrFile':
                this._createGivenDirOrFile(msg.path, msg.typeStr, msg.shouldCreate, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:CreateDirOrFile:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:CheckIsCommandRunnig':
                this._checkIsCommandRunning(msg.commands, function(is_running) {
                    mm.sendAsyncMessage('EngmodeSrv:CheckIsCommandRunnig:Return', {
                        status: is_running,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:AudioLoop':
                if (msg.operation === 'start') {
                    let file = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
                    let process = Components.classes['@mozilla.org/process/util;1'].createInstance(Components.interfaces.nsIProcess);
                    let args = [];
                    file.initWithPath('/system/bin/audio_test.sh');
                    if (msg.param === 'mic') {
                        args = ['mic'];
                    } else if (msg.param === 'stop-mic') {
                        args = ['stop-mic'];
                    } else if (msg.param === 'sub-mic') {
                        args = ['sub-mic'];
                    } else if (msg.param === 'stop-sub-mic') {
                        args = ['stop-sub-mic'];
                    } else if (msg.param === 'headset-left') {
                        args = ['headset-left'];
                    } else if (msg.param === 'headset-left-stop') {
                        args = ['headset-left-stop'];
                    } else if (msg.param === 'headset-right') {
                        args = ['headset-right'];
                    } else if (msg.param === 'headset-right-stop') {
                        args = ['headset-right-stop'];
                    } else if (msg.param === 'headset-mic') {
                        args = ['headset-mic'];
                    } else if (msg.param === 'stop-headset-mic') {
                        args = ['stop-headset-mic'];
                    } else if (msg.param === 'enable-headphone-manually') {
                        args = ['enable-headphone'];
                    } else if (msg.param === 'disable-headphone-manually') {
                        args = ['disable-headphone'];
                    } else {
                        break;
                    }
                    try {
                        let self = this;
                        process.init(file);
                        debug('mmitest--audio loop: init finished');
                        process.runAsync(args, args.length, {
                            observe: function() {
                                if (self._audioLoopProcess && self._audioLoopProcess.isRunning) {
                                    self._audioLoopProcess.kill();
                                    self._audioLoopProcess = null;
                                    debug('mmitest -- KILL by observe ******');
                                }
                            }
                        });
                        debug('mmitest: _audioLoopProcess: ' + this._audioLoopProcess);
                        this._audioLoopProcess = process;
                    } catch (e) {
                        debug('mmitest--audio loop: start process error:' + e);
                    }
                } else if (msg.operation === 'stop') {
                    debug('mmitest--audio loop: stop process');
                    if (this._audioLoopProcess && this._audioLoopProcess.isRunning) {
                        debug('mmitest: audio loop: do KILL ******');
                        this._audioLoopProcess.kill();
                    }
                    this._audioLoopProcess = null;
                }
                break;
            case 'EngmodeSrv:ForceInCall':
                if (msg.operation === 'start') {
                    if (gAudioManager) {
                        gAudioManager.phoneState = nsIAudioManager.PHONE_STATE_IN_CALL;
                        debug('mmitest--startForceInCall: set state finished');
                    }
                } else if (msg.operation === 'stop') {
                    if (gAudioManager) {
                        gAudioManager.phoneState = nsIAudioManager.PHONE_STATE_NORMAL;
                        debug('mmitest--stopForceInCall: reset state finished');
                    }
                }
                break;
            case 'EngmodeSrv:UniversalCommand':
                if (msg.operation === 'start') {
                    let file = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
                    let process = Components.classes['@mozilla.org/process/util;1'].createInstance(Components.interfaces.nsIProcess);
                    let args = [];
                    let s = null;
                    let cmd = null;
                    let cmds = [];
                    if (msg.useShell) {
                        file.initWithPath('/system/bin/sh');
                        args[0] = '-c';
                        args[1] = msg.param;
                        debug('UniversalCommand--args:' + args);
                    } else {
                        s = msg.param;
                        cmd = s.match(/\S+/);
                        file.initWithPath(cmd);
                        cmds = s.match(/\S+/g);
                        for (var i in cmds) {
                            if (i > 0) {
                                args[i - 1] = cmds[i];
                            }
                        }
                        debug('UniversalCommand--command:' + cmd + args);
                    }
                    try {
                        let self = this;
                        process.init(file);
                        debug('UniversalCommand: init finished ...');
                        process.runAsync(args, args.length, {
                            observe: function(aSubject, aTopic, aData) {
                                debug('UniversalCommand--*********observe********');
                                debug('UniversalCommand-- aSubject:' + aSubject);
                                debug('UniversalCommand-- aTopic:' + aTopic);
                                debug('UniversalCommand-- aData:' + aData);
                                if (self._universalCommandProcess && self._universalCommandProcess.isRunning) {
                                    self._universalCommandProcess.kill();
                                    debug('UniversalCommand--KILL by observe******');
                                }
                                self._universalCommandProcess = null;
                                mm.sendAsyncMessage('EngmodeSrv:UniversalCommand:Return', {
                                    data: aTopic,
                                    requestID: msg.requestID
                                });
                            }
                        });
                        debug('_universalCommandProcess: ' + this._universalCommandProcess);
                        this._universalCommandProcess = process;
                    } catch (e) {
                        debug('universalCommand: start process error:' + e);
                        mm.sendAsyncMessage('EngmodeSrv:UniversalCommand:Return', {
                            data: 'process-failed',
                            requestID: msg.requestID
                        });
                    }
                } else if (msg.operation === 'stop') {
                    debug('universalCommand: stop process');
                    if (this._universalCommandProcess && this._universalCommandProcess.isRunning) {
                        debug('universalCommand: do KILL******');
                        this._universalCommandProcess.kill();
                    }
                    this._universalCommandProcess = null;
                }
                break;
            case 'EngmodeSrv:GpsTest':
                if (msg.operation === 'start') {
                    let file = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
                    let process = Components.classes['@mozilla.org/process/util;1'].createInstance(Components.interfaces.nsIProcess);
                    let args = [];
                    file.initWithPath('/system/bin/gps_test');
                    args = ['0', '1'];
                    try {
                        let self = this;
                        process.init(file);
                        debug('mmitest--gps test: init finished');
                        process.runAsync(args, args.length, {
                            observe: function() {
                                if (self._gpsTestProcess && self._gpsTestProcess.isRunning) {
                                    self._gpsTestProcess.kill();
                                    self._gpsTestProcess = null;
                                    debug('mmitest--KILL by observe******');
                                }
                            }
                        });
                        debug('mmitest: _gpsTestProcess: ' + this._gpsTestProcess);
                        this._gpsTestProcess = process;
                    } catch (e) {
                        debug('mmitest--gps test: start process error:' + e);
                    }
                } else if (msg.operation === 'stop') {
                    debug('mmitest--gps test: stop process');
                    if (this._gpsTestProcess && this._gpsTestProcess.isRunning) {
                        debug('mmitest: gps test: do KILL ******');
                        this._gpsTestProcess.kill();
                    }
                    this._gpsTestProcess = null;
                }
                break;

            case 'EngmodeSrv:ReadNvitem':
                this._readNvitem(msg.item, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:ReadNvitem:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:GetFusest':
                this._getfusest = aMessage.target;
                this._getFusestate();
                break;
            case 'EngmodeSrv:ReadNvitemRoot':
                this._readNvitemRoot(msg.item, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:ReadNvitemRoot:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:ReadNvitemEx':
                this._readNvitemEx(msg.item, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:ReadNvitemEx:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;

            case 'EngmodeSrv:ReadDebugInfo':
                this._readDebugInfo(msg.item, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:ReadDebugInfo:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;

            case 'EngmodeSrv:SetDdtmStatus':
                this._setDdtmStatus(msg.enable, msg.appName, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:SetDdtmStatus:Return', {
                        data: '',
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:GetDdtmStatus':
                this._getDdtmStatus(function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:GetDdtmStatus:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:WriteNvitem':
                this._writeNvitem(msg.item, msg.length, msg.value, function() {
                    debug("engmodenvram write finished.");
                    mm.sendAsyncMessage('EngmodeSrv:WriteNvitem:Return', {
                        data: '',
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:WriteNvitemEx':
                this._writeNvitemEx(msg.item, msg.length, msg.value, function() {
                    debug("engmodenvram write finished.");
                    mm.sendAsyncMessage('EngmodeSrv:WriteNvitemEx:Return', {
                        data: '',
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:SetChargerLED':
                this._setchargerled(msg.config);
                break;
            case 'EngmodeSrv:SetCameraLED':
                this._setcameraled(msg.config);
                break;
            case 'EngmodeSrv:setSubLCD':
                this._setsublcd(msg.config);
                break;
            case 'EngmodeSrv:setKeypadLED':
                this._setkeypadled(msg.config);
                break;
            case 'EngmodeSrv:SetChargingEnabled':
                this._setchargingenabled(msg.config);
                break;
            case 'EngmodeSrv:SetDbClickUnlock':
                this._setdbclickunlock(msg.config);
                break;
            case 'EngmodeSrv:SetPropertyValue':
                this._setPropertyValue(msg.name, msg.value);
                break;
            case 'EngmodeSrv:FileWrite':
                this._filewrite(msg.str, msg.path, msg.par, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:FileWrite:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:SetDirProp':
                this._setDirProp(msg.path, msg.status, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:SetDirProp:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:EraseAPN':
                this._eraseAPN(msg.apnno, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:EraseAPN:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:EnableBand41TxASDiv':
                this._enableBand41TxASDiv(msg.enalbe, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:EnableBand41TxASDiv:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:IsBand41TxASDivEnabled':
                this._isBand41TxASDivEnabled(function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:IsBand41TxASDivEnabled:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:setInitAttachProfile':
                this._setInitAttachProfile(msg.apn, msg.protocol, msg.authtype, msg.user, msg.password, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:setInitAttachProfile:return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:setDataProfileByType':
                this._setDataProfileByType(msg.networkType, msg.profileId, msg.apn, msg.protocol, msg.authtype, msg.user, msg.password, msg.types, msg.maxConnsTime, msg.maxConns, msg.waitTime, msg.enabled, msg.inactivityTimer, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:setDataProfileByType:return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:RemoveBandFromPriorityList':
                this._RemoveBandFromPriorityList(msg.band, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:RemoveBandFromPriorityList:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            case 'EngmodeSrv:AddBandToPriorityList':
                this._AddBandToPriorityList(msg.band, function(data) {
                    mm.sendAsyncMessage('EngmodeSrv:AddBandToPriorityList:Return', {
                        data: data,
                        requestID: msg.requestID
                    });
                });
                break;
            default:
                debug('receiveMessage: Can not process the message ' + aMessage.name);
                break;
        }
    }
};
EngmodeService.init();