"use strict";const{Cc,Ci,Cu,components}=require("chrome");const{isWindowIncluded}=require("devtools/shared/layout/utils");const Services=require("Services");Cu.import("resource://gre/modules/XPCOMUtils.jsm");loader.lazyImporter(this,"VariablesView","resource://devtools/client/shared/widgets/VariablesView.jsm");XPCOMUtils.defineLazyServiceGetter(this,"swm","@mozilla.org/serviceworkers/manager;1","nsIServiceWorkerManager");const REGEX_MATCH_FUNCTION_NAME=/^\(?function\s+([^(\s]+)\s*\(/;const CONSOLE_ENTRY_THRESHOLD=5;const CONSOLE_WORKER_IDS=exports.CONSOLE_WORKER_IDS=["SharedWorker","ServiceWorker","Worker"];var WebConsoleUtils={supportsString:function(string){let str=Cc["@mozilla.org/supports-string;1"].createInstance(Ci.nsISupportsString);str.data=string;return str;},getWorkerType:function(message){let id=message?message.innerID:null;return CONSOLE_WORKER_IDS[CONSOLE_WORKER_IDS.indexOf(id)]||null;},cloneObject:function(object,recursive,filter){if(typeof object!="object"){return object;}
let temp;if(Array.isArray(object)){temp=[];Array.forEach(object,function(value,index){if(!filter||filter(index,value,object)){temp.push(recursive?WebConsoleUtils.cloneObject(value):value);}});}else{temp={};for(let key in object){let value=object[key];if(object.hasOwnProperty(key)&&(!filter||filter(key,value,object))){temp[key]=recursive?WebConsoleUtils.cloneObject(value):value;}}}
return temp;},copyTextStyles:function(from,to){let win=from.ownerDocument.defaultView;let style=win.getComputedStyle(from);to.style.fontFamily=style.getPropertyCSSValue("font-family").cssText;to.style.fontSize=style.getPropertyCSSValue("font-size").cssText;to.style.fontWeight=style.getPropertyCSSValue("font-weight").cssText;to.style.fontStyle=style.getPropertyCSSValue("font-style").cssText;},getInnerWindowId:function(window){return window.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindowUtils).currentInnerWindowID;},getInnerWindowIDsForFrames:function(window){let innerWindowID=this.getInnerWindowId(window);let ids=[innerWindowID];if(window.frames){for(let i=0;i<window.frames.length;i++){let frame=window.frames[i];ids=ids.concat(this.getInnerWindowIDsForFrames(frame));}}
return ids;},getOuterWindowId:function(window){return window.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindowUtils).outerWindowID;},isNativeFunction:function(func){return typeof func=="function"&&!("prototype"in func);},isNonNativeGetter:function(object,prop){if(typeof object!="object"){return false;}
let desc=this.getPropertyDescriptor(object,prop);return desc&&desc.get&&!this.isNativeFunction(desc.get);},getPropertyDescriptor:function(object,prop){let desc=null;while(object){try{if((desc=Object.getOwnPropertyDescriptor(object,prop))){break;}}catch(ex){if(ex.name!="NS_ERROR_XPC_BAD_CONVERT_JS"&&ex.name!="NS_ERROR_XPC_BAD_OP_ON_WN_PROTO"&&ex.name!="TypeError"){throw ex;}}
try{object=Object.getPrototypeOf(object);}catch(ex){if(ex.name=="TypeError"){return desc;}
throw ex;}}
return desc;},propertiesSort:function(a,b){let number=parseFloat(a.name);let bNumber=parseFloat(b.name);if(!isNaN(number)&&isNaN(bNumber)){return-1;}else if(isNaN(number)&&!isNaN(bNumber)){return 1;}else if(!isNaN(number)&&!isNaN(bNumber)){return number-bNumber;}else if(a.name<b.name){return-1;}else if(a.name>b.name){return 1;}
return 0;},createValueGrip:function(value,objectWrapper){switch(typeof value){case"boolean":return value;case"string":return objectWrapper(value);case"number":if(value===Infinity){return{type:"Infinity"};}else if(value===-Infinity){return{type:"-Infinity"};}else if(Number.isNaN(value)){return{type:"NaN"};}else if(!value&&1/value===-Infinity){return{type:"-0"};}
return value;case"undefined":return{type:"undefined"};case"object":if(value===null){return{type:"null"};}
case"function":return objectWrapper(value);default:Cu.reportError("Failed to provide a grip for value of "+typeof value
+": "+value);return null;}},isIteratorOrGenerator:function(object){if(object===null){return false;}
if(typeof object=="object"){if(typeof object.__iterator__=="function"||object.constructor&&object.constructor.name=="Iterator"){return true;}
try{let str=object.toString();if(typeof object.next=="function"&&str.indexOf("[object Generator")==0){return true;}}catch(ex){return false;}}
return false;},isMixedHTTPSRequest:function(request,location){try{let requestURI=Services.io.newURI(request,null,null);let contentURI=Services.io.newURI(location,null,null);return(contentURI.scheme=="https"&&requestURI.scheme!="https");}catch(ex){return false;}},getFunctionName:function(func){let name=null;if(func.name){name=func.name;}else{let desc;try{desc=func.getOwnPropertyDescriptor("displayName");}catch(ex){}
if(desc&&typeof desc.value=="string"){name=desc.value;}}
if(!name){try{let str=(func.toString()||func.toSource())+"";name=(str.match(REGEX_MATCH_FUNCTION_NAME)||[])[1];}catch(ex){}}
return name;},getObjectClassName:function(object){if(object===null){return"null";}
if(object===undefined){return"undefined";}
let type=typeof object;if(type!="object"){return type.charAt(0).toUpperCase()+type.substr(1);}
let className;try{className=((object+"").match(/^\[object (\S+)\]$/)||[])[1];if(!className){className=((object.constructor+"").match(/^\[object (\S+)\]$/)||[])[1];}
if(!className&&typeof object.constructor=="function"){className=this.getFunctionName(object.constructor);}}catch(ex){}
return className;},isActorGrip:function(grip){return grip&&typeof(grip)=="object"&&grip.actor;},_usageCount:0,get usageCount(){if(WebConsoleUtils._usageCount<CONSOLE_ENTRY_THRESHOLD){WebConsoleUtils._usageCount=Services.prefs.getIntPref("devtools.selfxss.count");if(Services.prefs.getBoolPref("devtools.chrome.enabled")){WebConsoleUtils.usageCount=CONSOLE_ENTRY_THRESHOLD;}}
return WebConsoleUtils._usageCount;},set usageCount(newUC){if(newUC<=CONSOLE_ENTRY_THRESHOLD){WebConsoleUtils._usageCount=newUC;Services.prefs.setIntPref("devtools.selfxss.count",newUC);}},pasteHandlerGen:function(inputField,notificationBox,msg,okstring){let handler=function(event){if(WebConsoleUtils.usageCount>=CONSOLE_ENTRY_THRESHOLD){inputField.removeEventListener("paste",handler);inputField.removeEventListener("drop",handler);return true;}
if(notificationBox.getNotificationWithValue("selfxss-notification")){event.preventDefault();event.stopPropagation();return false;}
let notification=notificationBox.appendNotification(msg,"selfxss-notification",null,notificationBox.PRIORITY_WARNING_HIGH,null,function(eventType){ if(eventType=="removed"){inputField.removeEventListener("keyup",pasteKeyUpHandler);}});function pasteKeyUpHandler(event2){let value=inputField.value||inputField.textContent;if(value.includes(okstring)){notificationBox.removeNotification(notification);inputField.removeEventListener("keyup",pasteKeyUpHandler);WebConsoleUtils.usageCount=CONSOLE_ENTRY_THRESHOLD;}}
inputField.addEventListener("keyup",pasteKeyUpHandler);event.preventDefault();event.stopPropagation();return false;};return handler;},};exports.Utils=WebConsoleUtils;
WebConsoleUtils.L10n=function(bundleURI){this._bundleUri=bundleURI;};WebConsoleUtils.L10n.prototype={_stringBundle:null,get stringBundle(){if(!this._stringBundle){this._stringBundle=Services.strings.createBundle(this._bundleUri);}
return this._stringBundle;},timestampString:function(milliseconds){let d=new Date(milliseconds?milliseconds:null);let hours=d.getHours(),minutes=d.getMinutes();let seconds=d.getSeconds();milliseconds=d.getMilliseconds();let parameters=[hours,minutes,seconds,milliseconds];return this.getFormatStr("timestampFormat",parameters);},getStr:function(name){let result;try{result=this.stringBundle.GetStringFromName(name);}catch(ex){Cu.reportError("Failed to get string: "+name);throw ex;}
return result;},getFormatStr:function(name,array){let result;try{result=this.stringBundle.formatStringFromName(name,array,array.length);}catch(ex){Cu.reportError("Failed to format string: "+name);throw ex;}
return result;},};
function ConsoleServiceListener(window,listener){this.window=window;this.listener=listener;}
exports.ConsoleServiceListener=ConsoleServiceListener;ConsoleServiceListener.prototype={QueryInterface:XPCOMUtils.generateQI([Ci.nsIConsoleListener]),window:null,listener:null,init:function(){Services.console.registerListener(this);},observe:function(message){if(!this.listener){return;}
if(this.window){if(!(message instanceof Ci.nsIScriptError)||!message.outerWindowID||!this.isCategoryAllowed(message.category)){return;}
let errorWindow=Services.wm.getOuterWindowWithId(message.outerWindowID);if(!errorWindow||!isWindowIncluded(this.window,errorWindow)){return;}}
this.listener.onConsoleServiceMessage(message);},isCategoryAllowed:function(category){if(!category){return false;}
switch(category){case"XPConnect JavaScript":case"component javascript":case"chrome javascript":case"chrome registration":case"XBL":case"XBL Prototype Handler":case"XBL Content Sink":case"xbl javascript":return false;}
return true;},getCachedMessages:function(includePrivate=false){let errors=Services.console.getMessageArray()||[];
if(!this.window){return errors.filter((error)=>{if(error instanceof Ci.nsIScriptError){if(!includePrivate&&error.isFromPrivateWindow){return false;}}
return true;});}
let ids=WebConsoleUtils.getInnerWindowIDsForFrames(this.window);return errors.filter((error)=>{if(error instanceof Ci.nsIScriptError){if(!includePrivate&&error.isFromPrivateWindow){return false;}
if(ids&&(ids.indexOf(error.innerWindowID)==-1||!this.isCategoryAllowed(error.category))){return false;}}else if(ids&&ids[0]){
return false;}
return true;});},destroy:function(){Services.console.unregisterListener(this);this.listener=this.window=null;},};
function ConsoleAPIListener(window,owner,{addonId}={}){this.window=window;this.owner=owner;this.addonId=addonId;}
exports.ConsoleAPIListener=ConsoleAPIListener;ConsoleAPIListener.prototype={QueryInterface:XPCOMUtils.generateQI([Ci.nsIObserver]),window:null,owner:null,addonId:null,init:function(){
Services.obs.addObserver(this,"console-api-log-event",false);},observe:function(message,topic){if(!this.owner){return;}


let apiMessage=message.wrappedJSObject;if(!this.isMessageRelevant(apiMessage)){return;}
this.owner.onConsoleAPICall(apiMessage);},isMessageRelevant:function(message){let workerType=WebConsoleUtils.getWorkerType(message);if(this.window&&workerType==="ServiceWorker"){

let scope=message.ID;if(!swm.shouldReportToWindow(this.window,scope)){return false;}}
if(this.window&&!workerType){let msgWindow=Services.wm.getCurrentInnerWindowWithId(message.innerID);if(!msgWindow||!isWindowIncluded(this.window,msgWindow)){return false;}}
if(this.addonId){



if(message.originAttributes&&message.originAttributes.addonId==this.addonId){return true;}

if(message.consoleID&&message.consoleID==`addon/${this.addonId}`){return true;}
return false;}
return true;},getCachedMessages:function(includePrivate=false){let messages=[];let ConsoleAPIStorage=Cc["@mozilla.org/consoleAPI-storage;1"].getService(Ci.nsIConsoleAPIStorage);
if(!this.window){messages=ConsoleAPIStorage.getEvents();}else{let ids=WebConsoleUtils.getInnerWindowIDsForFrames(this.window);ids.forEach((id)=>{messages=messages.concat(ConsoleAPIStorage.getEvents(id));});}
CONSOLE_WORKER_IDS.forEach((id)=>{messages=messages.concat(ConsoleAPIStorage.getEvents(id));});messages=messages.filter(msg=>{return this.isMessageRelevant(msg);});if(includePrivate){return messages;}
return messages.filter((m)=>!m.private);},destroy:function(){Services.obs.removeObserver(this,"console-api-log-event");this.window=this.owner=null;},};var WebConsoleCommands={_registeredCommands:new Map(),_originalCommands:new Map(),_registerOriginal:function(name,command){this.register(name,command);this._originalCommands.set(name,this.getCommand(name));},register:function(name,command){this._registeredCommands.set(name,command);},unregister:function(name){this._registeredCommands.delete(name);if(this._originalCommands.has(name)){this.register(name,this._originalCommands.get(name));}},getCommand:function(name){return this._registeredCommands.get(name);},hasCommand:function(name){return this._registeredCommands.has(name);},};exports.WebConsoleCommands=WebConsoleCommands;WebConsoleCommands._registerOriginal("$",function(owner,selector){return owner.window.document.querySelector(selector);});WebConsoleCommands._registerOriginal("$$",function(owner,selector){let nodes=owner.window.document.querySelectorAll(selector);
let result=new owner.window.Array();for(let i=0;i<nodes.length;i++){result.push(nodes[i]);}
return result;});WebConsoleCommands._registerOriginal("$_",{get:function(owner){return owner.consoleActor.getLastConsoleInputEvaluation();}});WebConsoleCommands._registerOriginal("$x",function(owner,xPath,context){let nodes=new owner.window.Array();let doc=owner.window.document;context=context||doc;let results=doc.evaluate(xPath,context,null,Ci.nsIDOMXPathResult.ANY_TYPE,null);let node;while((node=results.iterateNext())){nodes.push(node);}
return nodes;});WebConsoleCommands._registerOriginal("$0",{get:function(owner){return owner.makeDebuggeeValue(owner.selectedNode);}});WebConsoleCommands._registerOriginal("clear",function(owner){owner.helperResult={type:"clearOutput",};});WebConsoleCommands._registerOriginal("clearHistory",function(owner){owner.helperResult={type:"clearHistory",};});WebConsoleCommands._registerOriginal("keys",function(owner,object){ return Cu.cloneInto(Object.keys(Cu.waiveXrays(object)),owner.window);});WebConsoleCommands._registerOriginal("values",function(owner,object){let values=[]; let waived=Cu.waiveXrays(object);let names=Object.getOwnPropertyNames(waived);for(let name of names){values.push(waived[name]);}
return Cu.cloneInto(values,owner.window);});WebConsoleCommands._registerOriginal("help",function(owner){owner.helperResult={type:"help"};});WebConsoleCommands._registerOriginal("cd",function(owner,window){if(!window){owner.consoleActor.evalWindow=null;owner.helperResult={type:"cd"};return;}
if(typeof window=="string"){window=owner.window.document.querySelector(window);}
if(window instanceof Ci.nsIDOMElement&&window.contentWindow){window=window.contentWindow;}
if(!(window instanceof Ci.nsIDOMWindow)){owner.helperResult={type:"error",message:"cdFunctionInvalidArgument"};return;}
owner.consoleActor.evalWindow=window;owner.helperResult={type:"cd"};});WebConsoleCommands._registerOriginal("inspect",function(owner,object){let dbgObj=owner.makeDebuggeeValue(object);let grip=owner.createValueGrip(dbgObj);owner.helperResult={type:"inspectObject",input:owner.evalInput,object:grip,};});WebConsoleCommands._registerOriginal("pprint",function(owner,object){if(object===null||object===undefined||object===true||object===false){owner.helperResult={type:"error",message:"helperFuncUnsupportedTypeError",};return null;}
owner.helperResult={rawOutput:true};if(typeof object=="function"){return object+"\n";}
let output=[];let obj=object;for(let name in obj){let desc=WebConsoleUtils.getPropertyDescriptor(obj,name)||{};if(desc.get||desc.set){let getGrip=VariablesView.getGrip(desc.get);let setGrip=VariablesView.getGrip(desc.set);let getString=VariablesView.getString(getGrip);let setString=VariablesView.getString(setGrip);output.push(name+":","  get: "+getString,"  set: "+setString);}else{let valueGrip=VariablesView.getGrip(obj[name]);let valueString=VariablesView.getString(valueGrip);output.push(name+": "+valueString);}}
return"  "+output.join("\n  ");});WebConsoleCommands._registerOriginal("print",function(owner,value){owner.helperResult={rawOutput:true};if(typeof value==="symbol"){return Symbol.prototype.toString.call(value);}



return String(Cu.waiveXrays(value));});WebConsoleCommands._registerOriginal("copy",function(owner,value){let payload;try{if(value instanceof Ci.nsIDOMElement){payload=value.outerHTML;}else if(typeof value=="string"){payload=value;}else{payload=JSON.stringify(value,null,"  ");}}catch(ex){payload="/* "+ex+" */";}
owner.helperResult={type:"copyValueToClipboard",value:payload,};});function addWebConsoleCommands(owner){if(!owner){throw new Error("The owner is required");}
for(let[name,command]of WebConsoleCommands._registeredCommands){if(typeof command==="function"){owner.sandbox[name]=command.bind(undefined,owner);}else if(typeof command==="object"){let clone=Object.assign({},command,{
enumerable:true,configurable:true});if(typeof command.get==="function"){clone.get=command.get.bind(undefined,owner);}
if(typeof command.set==="function"){clone.set=command.set.bind(undefined,owner);}
Object.defineProperty(owner.sandbox,name,clone);}}}
exports.addWebConsoleCommands=addWebConsoleCommands;function ConsoleReflowListener(window,listener){this.docshell=window.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIWebNavigation).QueryInterface(Ci.nsIDocShell);this.listener=listener;this.docshell.addWeakReflowObserver(this);}
exports.ConsoleReflowListener=ConsoleReflowListener;ConsoleReflowListener.prototype={QueryInterface:XPCOMUtils.generateQI([Ci.nsIReflowObserver,Ci.nsISupportsWeakReference]),docshell:null,listener:null,sendReflow:function(start,end,interruptible){let frame=components.stack.caller.caller;let filename=frame?frame.filename:null;if(filename){filename=filename.split(" ").pop();}
this.listener.onReflowActivity({interruptible:interruptible,start:start,end:end,sourceURL:filename,sourceLine:frame?frame.lineNumber:null,functionName:frame?frame.name:null});},reflow:function(start,end){this.sendReflow(start,end,false);},reflowInterruptible:function(start,end){this.sendReflow(start,end,true);},destroy:function(){this.docshell.removeWeakReflowObserver(this);this.listener=this.docshell=null;},};function gSequenceId(){return gSequenceId.n++;}
gSequenceId.n=0;