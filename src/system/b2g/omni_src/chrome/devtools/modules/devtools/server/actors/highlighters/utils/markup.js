"use strict";const{Cc,Ci,Cu}=require("chrome");const{getCurrentZoom,getRootBindingParent}=require("devtools/shared/layout/utils");const{on,emit}=require("sdk/event/core");const lazyContainer={};loader.lazyRequireGetter(lazyContainer,"CssLogic","devtools/shared/inspector/css-logic",true);exports.getComputedStyle=(node)=>lazyContainer.CssLogic.getComputedStyle(node);exports.getBindingElementAndPseudo=(node)=>lazyContainer.CssLogic.getBindingElementAndPseudo(node);loader.lazyGetter(lazyContainer,"DOMUtils",()=>Cc["@mozilla.org/inspector/dom-utils;1"].getService(Ci.inIDOMUtils));exports.hasPseudoClassLock=(...args)=>lazyContainer.DOMUtils.hasPseudoClassLock(...args);exports.addPseudoClassLock=(...args)=>lazyContainer.DOMUtils.addPseudoClassLock(...args);exports.removePseudoClassLock=(...args)=>lazyContainer.DOMUtils.removePseudoClassLock(...args);exports.getCSSStyleRules=(...args)=>lazyContainer.DOMUtils.getCSSStyleRules(...args);const SVG_NS="http://www.w3.org/2000/svg";const XUL_NS="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";const STYLESHEET_URI="resource://devtools/server/actors/"+"highlighters.css";const _tokens=Symbol("classList/tokens");function ClassList(className){let trimmed=(className||"").trim();this[_tokens]=trimmed?trimmed.split(/\s+/):[];}
ClassList.prototype={item(index){return this[_tokens][index];},contains(token){return this[_tokens].includes(token);},add(token){if(!this.contains(token)){this[_tokens].push(token);}
emit(this,"update");},remove(token){let index=this[_tokens].indexOf(token);if(index>-1){this[_tokens].splice(index,1);}
emit(this,"update");},toggle(token){if(this.contains(token)){this.remove(token);}else{this.add(token);}},get length(){return this[_tokens].length;},[Symbol.iterator]:function*(){for(let i=0;i<this.tokens.length;i++){yield this[_tokens][i];}},toString(){return this[_tokens].join(" ");}};function isXUL(window){return window.document.documentElement.namespaceURI===XUL_NS;}
exports.isXUL=isXUL;var installedHelperSheets=new WeakMap();function installHelperSheet(win,source,type="agent"){if(installedHelperSheets.has(win.document)){return;}
let{Style}=require("sdk/stylesheet/style");let{attach}=require("sdk/content/mod");let style=Style({source,type});attach(style,win);installedHelperSheets.set(win.document,style);}
exports.installHelperSheet=installHelperSheet;function isNodeValid(node){if(!node||Cu.isDeadWrapper(node)){return false;} 
if(node.nodeType!==node.ELEMENT_NODE){return false;}
let doc=node.ownerDocument;if(!doc||!doc.defaultView){return false;}

let bindingParent=getRootBindingParent(node);if(!doc.documentElement.contains(bindingParent)){return false;}
return true;}
exports.isNodeValid=isNodeValid;function createSVGNode(win,options){if(!options.nodeType){options.nodeType="box";}
options.namespace=SVG_NS;return createNode(win,options);}
exports.createSVGNode=createSVGNode;function createNode(win,options){let type=options.nodeType||"div";let node;if(options.namespace){node=win.document.createElementNS(options.namespace,type);}else{node=win.document.createElement(type);}
for(let name in options.attributes||{}){let value=options.attributes[name];if(options.prefix&&(name==="class"||name==="id")){value=options.prefix+value;}
node.setAttribute(name,value);}
if(options.parent){options.parent.appendChild(node);}
return node;}
exports.createNode=createNode;function CanvasFrameAnonymousContentHelper(highlighterEnv,nodeBuilder){this.highlighterEnv=highlighterEnv;this.nodeBuilder=nodeBuilder;this.anonymousContentDocument=this.highlighterEnv.document;this.anonymousContentGlobal=Cu.getGlobalForObject(this.anonymousContentDocument);this._insert();this._onNavigate=this._onNavigate.bind(this);this.highlighterEnv.on("navigate",this._onNavigate);this.listeners=new Map();}
CanvasFrameAnonymousContentHelper.prototype={destroy:function(){try{let doc=this.anonymousContentDocument;doc.removeAnonymousContent(this._content);}catch(e){
}
this.highlighterEnv.off("navigate",this._onNavigate);this.highlighterEnv=this.nodeBuilder=this._content=null;this.anonymousContentDocument=null;this.anonymousContentGlobal=null;this._removeAllListeners();},_insert:function(){
if(!this.highlighterEnv.document.documentElement||isXUL(this.highlighterEnv.window)){return;}
let doc=this.highlighterEnv.document;
if(doc.hidden){
let onVisibilityChange=()=>{doc.removeEventListener("visibilitychange",onVisibilityChange);this._insert();};doc.addEventListener("visibilitychange",onVisibilityChange);return;}


installHelperSheet(this.highlighterEnv.window,"@import url('"+STYLESHEET_URI+"');");let node=this.nodeBuilder();this._content=doc.insertAnonymousContent(node);},_onNavigate:function(e,{isTopLevel}){if(isTopLevel){this._removeAllListeners();this._insert();this.anonymousContentDocument=this.highlighterEnv.document;}},getTextContentForElement:function(id){if(!this.content){return null;}
return this.content.getTextContentForElement(id);},setTextContentForElement:function(id,text){if(this.content){this.content.setTextContentForElement(id,text);}},setAttributeForElement:function(id,name,value){if(this.content){this.content.setAttributeForElement(id,name,value);}},getAttributeForElement:function(id,name){if(!this.content){return null;}
return this.content.getAttributeForElement(id,name);},removeAttributeForElement:function(id,name){if(this.content){this.content.removeAttributeForElement(id,name);}},hasAttributeForElement:function(id,name){return typeof this.getAttributeForElement(id,name)==="string";},addEventListenerForElement:function(id,type,handler){if(typeof id!=="string"){throw new Error("Expected a string ID in addEventListenerForElement but"+" got: "+id);}
if(!this.listeners.has(type)){let target=this.highlighterEnv.pageListenerTarget;target.addEventListener(type,this,true);this.listeners.set(type,new Map());}
let listeners=this.listeners.get(type);listeners.set(id,handler);},removeEventListenerForElement:function(id,type){let listeners=this.listeners.get(type);if(!listeners){return;}
listeners.delete(id);if(!this.listeners.has(type)){let target=this.highlighterEnv.pageListenerTarget;target.removeEventListener(type,this,true);}},handleEvent:function(event){let listeners=this.listeners.get(event.type);if(!listeners){return;}

let isPropagationStopped=false;let eventProxy=new Proxy(event,{get:(obj,name)=>{if(name==="originalTarget"){return null;}else if(name==="stopPropagation"){return()=>{isPropagationStopped=true;};}
return obj[name];}});
let node=event.originalTarget;while(node){let handler=listeners.get(node.id);if(handler){handler(eventProxy,node.id);if(isPropagationStopped){break;}}
node=node.parentNode;}},_removeAllListeners:function(){if(this.highlighterEnv){let target=this.highlighterEnv.pageListenerTarget;for(let[type]of this.listeners){target.removeEventListener(type,this,true);}}
this.listeners.clear();},getElement:function(id){let classList=new ClassList(this.getAttributeForElement(id,"class"));on(classList,"update",()=>{this.setAttributeForElement(id,"class",classList.toString());});return{getTextContent:()=>this.getTextContentForElement(id),setTextContent:text=>this.setTextContentForElement(id,text),setAttribute:(name,val)=>this.setAttributeForElement(id,name,val),getAttribute:name=>this.getAttributeForElement(id,name),removeAttribute:name=>this.removeAttributeForElement(id,name),hasAttribute:name=>this.hasAttributeForElement(id,name),addEventListener:(type,handler)=>{return this.addEventListenerForElement(id,type,handler);},removeEventListener:(type,handler)=>{return this.removeEventListenerForElement(id,type,handler);},classList};},get content(){if(!this._content||Cu.isDeadWrapper(this._content)){return null;}
return this._content;},scaleRootElement:function(node,id){let zoom=getCurrentZoom(node);let value="position:absolute;width:100%;height:100%;";if(zoom!==1){value="position:absolute;";value+="transform-origin:top left;transform:scale("+(1/zoom)+");";value+="width:"+(100*zoom)+"%;height:"+(100*zoom)+"%;";}
this.setAttributeForElement(id,"style",value);}};exports.CanvasFrameAnonymousContentHelper=CanvasFrameAnonymousContentHelper;