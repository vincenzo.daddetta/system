
/**
 * Hotspot is a singleton that you can easily use it to fetch
 * some shared data across different panels
 *
 * @module Hotspot
 */
define('panels/hotspot/hotspot',['require','shared/toaster','shared/settings_listener'],function(require) {
  

  var Toaster = require('shared/toaster');
  // modules / helpers
  var SettingsListener = require('shared/settings_listener');

  var Hotspot = function() {};

  /**
   * @alias module:hotspot/hotspot
   * @requires module:hotspot/hotspot_settings
   * @returns {Hotspot}
   */
  Hotspot.prototype = {
    /**
     * Wifi hotspot setting
     *
     * @memberOf Hotspot
     * @type {Boolean}
     */
    _hotspotSetting: null,
    /**
     * Usb hotspot setting
     *
     * @memberOf Hotspot
     * @type {Boolean}
     */
    _usbHotspotSetting: null,
    /**
     * Usb storage setting
     *
     * @memberOf Hotspot
     * @type {Boolean}
     */
    _usbStorageSetting: null,
    /**
     * These listeners would be called when hotspot setting is changed
     *
     * @memberOf Hotspot
     * @type {Array}
     */
    _hotspotChangeListeners: [],

    /**
     * These listeners would be called when usb hotspot setting is changed
     *
     * @memberOf Hotspot
     * @type {Array}
     */
    _usbHotspotChangeListeners: [],

    /**
     * These listeners would be called when usb storage setting is changed
     *
     * @memberOf Hotspot
     * @type {Array}
     */
    _usbStorageChangeListeners: [],

    /**
     * These listeners would be called when incompatibles settings are
     * enabled at the same time
     *
     * @memberOf Hotspot
     * @type {Array}
     */
    _incompatibleSettingsListeners: [],

    /**
     * Wifi tethering setting key
     *
     * @access public
     * @memberOf Hotspot
     * @type {String}
     */
    tetheringWifiKey: 'tethering.wifi.enabled',

    /**
     * Usb tethering setting key
     *
     * @access public
     * @memberOf Hotspot
     * @type {String}
     */
    tetheringUsbKey: 'tethering.usb.enabled',

    /**
     * Usb storage setting key
     *
     * @access public
     * @memberOf Hotspot
     * @type {String}
     */
    usbStorageKey: 'ums.enabled',

    /**
     * Init is used to initialize some basic stuffs
     *
     * @memberOf Hotspot
     */
    init: function h_init() {
      this._bindEvents();
    },

    /**
     * We will bind some default listeners here
     *
     * @memberOf Hotspot
     */
    _bindEvents: function() {
      // Wifi tethering enabled
      SettingsListener.observe(this.tetheringWifiKey, false,
        this._hotspotSettingChange.bind(this));

      // USB tethering enabled
      SettingsListener.observe(this.tetheringUsbKey, false,
        this._usbHotspotSettingChange.bind(this));

      // USB storage enabled
      SettingsListener.observe(this.usbStorageKey, false,
        this._usbStorageSettingChange.bind(this));
    },

    /**
     * When wifi hotspot is changed, we will call all registered listeners
     *
     * @memberOf Hotspot
     */
    _hotspotSettingChange: function(enabled) {
      this._hotspotSetting = enabled;
      this._hotspotChangeListeners.forEach(function(listener) {
        listener(enabled);
      });
    },

    /**
     * When usb hotspot is changed, we will call all registered listeners
     *
     * @memberOf Hotspot
     */
    _usbHotspotSettingChange: function(enabled) {
      this._usbHotspotSetting = enabled;
      this._usbHotspotChangeListeners.forEach(function(listener) {
        listener(enabled);
      });
    },

    /**
     * When usb storage is changed, we will call all registered listeners
     *
     * @memberOf Hotspot
     */
    _usbStorageSettingChange: function(enabled) {
      this._usbStorageSetting = enabled;
      this._usbStorageChangeListeners.forEach(function(listener) {
        listener(enabled);
      });
    },

    /**
     * When two incompatible settings are enabled we will call all
     * registered listeners.
     *
     * @param bothConflicts Indicates that usb hotspot has the two
     * possible conflicts (wifi hotspot and usb storage)
     *
     * @memberOf Hotspot
     */
    _incompatibleSettings: function(newSetting, oldSetting, bothConflicts) {
      this._incompatibleSettingsListeners.forEach(function(listener) {
        listener(newSetting, oldSetting, bothConflicts);
      });
    },

    /**
     * Check if two incompatible settings are enabled
     *
     * @memberOf Hotspot
     */
    checkIncompatibleSettings: function(newSetting, value) {
      switch(newSetting) {
        case this.tetheringWifiKey:
          // Early return if the user has disabled the setting
          if (!value) {
            this._setWifiTetheringSetting(value);
            return;
          }

          if (value && this._usbHotspotSetting) {
            this._incompatibleSettings(this.tetheringWifiKey,
              this.tetheringUsbKey, false);
          } else {
            this._setWifiTetheringSetting(value);
          }
          break;
        case this.tetheringUsbKey:
          // Early return if the user has disabled the setting or the
          // incompatible settings are disabled
          if (!value || (!this._hotspotSetting && !this._usbStorageSetting)) {
            this._setUsbTetheringSetting(value);
            return;
          }
          if (this._usbStorageSetting && this._hotspotSetting) {
            this._incompatibleSettings(this.tetheringUsbKey, null, true);
          } else {
            var oldSetting = this._usbStorageSetting ? this.usbStorageKey :
              this.tetheringWifiKey;
            this._incompatibleSettings(this.tetheringUsbKey, oldSetting, false);
          }
          break;
      }
    },

    /**
     * This is an internal function that can help us find out the matched
     * callback from catched listeners and remove it
     *
     * @memberOf Hotspot
     * @param {Array} listeners
     * @param {Function} callback
     */
    _removeEventListener: function(listeners, callback) {
      var index = listeners.indexOf(callback);
      if (index >= 0) {
        listeners.splice(index, 1);
      }
    },

    /**
     * This is an internal function that set a value to the
     * Wifi tethering setting
     *
     * @memberOf Hotspot
     * @param {Boolean} Setting value
     */
    _setWifiTetheringSetting: function(value) {
      var cset = {};
      cset[this.tetheringWifiKey] = value;
      SettingsListener.getSettingsLock().set(cset);
      var toast = {
        messageL10nId: 'changessaved',
        latency: 3000,
        useTransition: true
      };
      Toaster.showToast(toast);
    },

    /**
     * This is an internal function that set a value to the
     * Usb tethering setting
     *
     * @memberOf Hotspot
     * @param {Boolean} Setting value
     */
    _setUsbTetheringSetting: function(value) {
      var cset = {};
      cset[this.tetheringUsbKey] = value;
      SettingsListener.getSettingsLock().set(cset);
      var toast = {
        messageL10nId: 'changessaved',
        latency: 3000,
        useTransition: true
      };
      Toaster.showToast(toast);
    },

    addEventListener: function(eventName, callback) {
      if (eventName === 'incompatibleSettings') {
        this._incompatibleSettingsListeners.push(callback);
      } else if (eventName === 'wifiHotspotChange') {
        this._hotspotChangeListeners.push(callback);
      } else if (eventName === 'usbHotspotChange') {
        this._usbHotspotChangeListeners.push(callback);
      } else if (eventName === 'usbStorageChange') {
        this._usbStorageChangeListeners.push(callback);
      }
    },

    removeEventListener: function(eventName, callback) {
      if (eventName === 'incompatibleSettings') {
        this._removeEventListener(
          this._incompatibleSettingsListeners, callback);
      } else if (eventName === 'wifiHotspotChange') {
        this._removeEventListener(
          this._hotspotChangeListeners, callback);
      } else if (eventName === 'usbHotspotChange') {
        this._removeEventListener(
          this._usbHotspotChangeListeners, callback);
      } else if (eventName === 'usbStorageChange') {
        this._removeEventListener(
          this._usbStorageChangeListeners, callback);
      }
    },

    get wifiHotspotSetting() {
      return this._hotspotSetting;
    },

    get usbHotspotSetting() {
      return this._usbHotspotSetting;
    },

    get usbStorageSetting() {
      return this._usbStorageSetting;
    },

    set hotspotSetting(value) {
      this._setWifiTetheringSetting(value);
    },

    set usbHotspotSetting(value) {
      this._setUsbTetheringSetting(value);
    }
  };

  return function ctor_hotspot() {
    return new Hotspot();
  };
});

/**
 * Hotspot Settings:
 *   - Update Hotspot Settings
 * @module HotspotSettings
 */
define('panels/hotspot/hotspot_settings',['require','shared/settings_listener','modules/settings_cache','modules/mvvm/observable','shared/toaster'],function(require) {
  

  var SettingsListener = require('shared/settings_listener');
  var SettingsCache = require('modules/settings_cache');
  var Observable = require('modules/mvvm/observable');
  var Toaster = require('shared/toaster');

  /**
   * @alias module:hotspot/hotspot_settings
   * @requires module:modules/mvvm/observable
   * @requires module:modules/settings_cache
   * @returns {hotspotSettingsPrototype}
   */
  var hotspotSettingsPrototype = {
    /**
     * USB insert status
     */
    isUSBInserted: false,

    /**
     * Hotspot SSID.
     *
     * @access private
     * @memberOf hotspotSettingsPrototype
     * @type {String}
     */
    hotspotSSID: '',

    /**
     * Hotspot security type
     *
     * @access private
     * @memberOf hotspotSettingsPrototype
     * @type {String}
     */
    hotspotSecurity: '',

    /**
     * Hotspot Password
     *
     * @access private
     * @memberOf hotspotSettingsPrototype
     * @type {String}
     */
    hotspotPassword: '',

    /**
     * Hotspot SSID setting key
     *
     * @access public
     * @memberOf hotspotSettingsPrototype
     * @type {String}
     */
    tetheringSSIDKey: 'tethering.wifi.ssid',

    /**
     * Hotspot security type setting key
     *
     * @access public
     * @memberOf hotspotSettingsPrototype
     * @type {String}
     */
    tetheringSecurityKey: 'tethering.wifi.security.type',

    /**
     * Hotspot password setting key
     *
     * @access public
     * @memberOf hotspotSettingsPrototype
     * @type {String}
     */
    tetheringPasswordKey: 'tethering.wifi.security.password',

    /**
     * Init module.
     *
     * @access private
     * @memberOf hotspotSettingsPrototype
     */
    _init: function hs_init() {
      this._usbManager = navigator.usb;
      this._initUSBStatus();
      this._bindEvents();
      this._updatePasswordIfNeeded();
    },

    /**
     * Init USB connection status
     *
     */
    _initUSBStatus: function hs_init_USB_status() {
      this.isUSBInserted = this._usbManager.deviceAttached;
    },

    /**
     * We will generate a random password for the hotspot
     *
     * @access private
     * @memberOf hotspotSettingsPrototype
     */
    _generateHotspotPassword: function hs_generateHotspotPassword() {
      var words = ['amsterdam', 'ankara', 'auckland',
                 'belfast', 'berlin', 'boston',
                 'calgary', 'caracas', 'chicago',
                 'dakar', 'delhi', 'dubai',
                 'dublin', 'houston', 'jakarta',
                 'lagos', 'lima', 'madrid',
                 'newyork', 'osaka', 'oslo',
                 'porto', 'santiago', 'saopaulo',
                 'seattle', 'stockholm', 'sydney',
                 'taipei', 'tokyo', 'toronto'];
      var password = words[Math.floor(Math.random() * words.length)];
      for (var i = 0; i < 4; i++) {
        password += Math.floor(Math.random() * 10);
      }
      return password;
    },

    /**
     * We will update hotspot password if needed
     *
     * @access private
     * @memberOf hotspotSettingsPrototype
    */
    _updatePasswordIfNeeded: function hs_updatePasswordIfNeeded() {
      var self = this;
      SettingsCache.getSettings(function(results) {
        if (!results[self.tetheringPasswordKey]) {
          var pwd = self._generateHotspotPassword();
          var cset = {};
          cset[self.tetheringPasswordKey] = pwd;
          SettingsListener.getSettingsLock().set(cset);
        }
      });
    },

    _showToaster: function hs_showToaster(msgId) {
      var toast = {
        messageL10nId: msgId,
        useTransition: true
      };
      Toaster.showToast(toast);
    },

    /**
     * Sets the value to the tethering SSID setting
     *
     * @access public
     * @memberOf hotspotSettingsPrototype
     * @param {String} value
     */
    setHotspotSSID: function hs_setHotspotSSID(value) {
      var cset = {};
      cset[this.tetheringSSIDKey] = value;
      var result = SettingsListener.getSettingsLock().set(cset);
      result.onsuccess = () => {
        this._showToaster('changessaved');
      };
    },

    /**
     * Sets the value to the tethering security type setting
     *
     * @access public
     * @memberOf hotspotSettingsPrototype
     * @param {String} value
     */
    setHotspotSecurity: function hs_setHotspotSecurity(value) {
      var cset = {};
      cset[this.tetheringSecurityKey] = value;
      var result = SettingsListener.getSettingsLock().set(cset);
      result.onsuccess = () => {
        this._showToaster('changessaved');
      };
    },

    /**
     * Sets the value to the tethering password setting
     *
     * @access private
     * @memberOf hotspotSettingsPrototype
     * @param {String} value
     */
    setHotspotPassword: function hs_setHotspotPassword(value) {
      var cset = {};
      cset[this.tetheringPasswordKey] = value;
      var result = SettingsListener.getSettingsLock().set(cset);
      result.onsuccess = () => {
        this._showToaster('changessaved');
      };
    },

    /**
     * Updates the current value of hotspot SSID
     *
     * @access private
     * @memberOf hotspotSettingsPrototype
     * @param {String} value
     */
    _onSSIDChange: function hs_onSSIDChange(value) {
      this.hotspotSSID = value;
    },

    /**
     * Updates the current value of hotspot security type
     *
     * @access private
     * @memberOf hotspotSettingsPrototype
     * @param {String} value
     */
    _onSecurityChange: function hs_onSecurityChange(value) {
      this.hotspotSecurity = value;
    },

    /**
     * Updates the current value of hotspot password
     *
     * @access private
     * @memberOf hotspotSettingsPrototype
     * @param {String} value
     */
    _onPasswordChange: function hs_onPasswordChange(value) {
      this.hotspotPassword = value;
    },

    /**
     * Updates the current status of USB
     *
     * @access private
     * @memberOf hotspotSettingsPrototype
     * @param {EventHandler} evt
     */
    _onUSBInsertChange: function hs__onUsbStatusChange(evt) {
      this.isUSBInserted = evt.deviceAttached;
    },

    /**
     * Listen to hotspot settings changes
     *
     * @access private
     * @memberOf hotspotSettingsPrototype
     */
    _bindEvents: function hs_bindEvents() {
      SettingsListener.observe(this.tetheringSSIDKey,
        '', this._onSSIDChange.bind(this));

      SettingsListener.observe(this.tetheringSecurityKey,
        'wpa-psk', this._onSecurityChange.bind(this));

      SettingsListener.observe(this.tetheringPasswordKey,
        '', this._onPasswordChange.bind(this));

      this._usbManager.onusbstatuschange =
        this._onUSBInsertChange.bind(this);
    }
  };

  return function ctor_hotspotSettings() {
    // Create the observable object using the prototype.
    var hotspotSettings = Observable(hotspotSettingsPrototype);
    hotspotSettings._init();
    return hotspotSettings;
  };
});

/* global openIncompatibleSettingsDialog, DsdsSettings, SettingsSoftkey */

define('panels/hotspot/panel',['require','modules/settings_panel','modules/settings_service','shared/settings_listener','panels/hotspot/hotspot','shared/simslot_manager','panels/hotspot/hotspot_settings'],function(require) {
  

  var SettingsPanel = require('modules/settings_panel');
  var SettingsService = require('modules/settings_service');
  var SettingsListener = require('shared/settings_listener');
  var Hotspot = require('panels/hotspot/hotspot');
  var SIMSlotManager = require('shared/simslot_manager');
  var HotspotSettings = require('panels/hotspot/hotspot_settings');

  return function ctor_hotspot() {
    var elements = {};
    // SIM Card available flag
    var hasSIMCard = false;
    // Current active SIM Card Object
    var activeSIMCard = null;
    // Data Connection status flag
    var isDataEnabled = false;
    var isWifiEnabled = false;
    var isFTUHotspot = false;
    var isFTUTethering = false;
    var recoverWifiState = false;
    var hotspot = Hotspot();
    var hotspotSettings = HotspotSettings();
    const FTU_HOTSPOT_KEY = 'isFirstTimeUseHotspot';
    const FTU_TETHERING_KEY = 'isFirstTimeUseTethering';

    function _updateSoftkey(evt) {
      var params = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }]
      };

      var position = evt.target.className.indexOf('none-select');
      if (position === -1) {
        SettingsSoftkey.init(params);
        SettingsSoftkey.show();
        //Bug-517 Add-BEGIN by ganghongliu@t2mobile.com 2017.11.15
        _updateHotspotPassword(hotspotSettings.hotspotPassword);
        //Bug-517 Add-END 2017.11.15
      } else {
        SettingsSoftkey.hide();
      }
    }

    // When the read-only element is focused, we should hide the soft-key.
    function _initAllFocusEvent() {
      var i = elements.panelLiItems.length - 1;
      for (i; i >= 0; i--) {
        elements.panelLiItems[i].addEventListener('focus', _updateSoftkey);
      }
    }

    function _removeAllFocusEvent() {
      var i = elements.panelLiItems.length - 1;
      for (i; i >= 0; i--) {
        elements.panelLiItems[i].removeEventListener('focus', _updateSoftkey);
      }
    }

    function _showSelect() {
      var select = document.querySelector('li:not([aria-disabled="true"]).focus select');
      select && select.focus();
    }

    function _handleDialogEvent(menuId, isFTU, isConflict) {
      if (isWifiEnabled) {
        if (isFTU) {
          _openWaringDialog(menuId).then(() => {
            _openFirstTimeUseDialog(menuId, false);
          });
        } else {
          _openWaringDialog(menuId);
        }
      } else {
        if (isConflict) {
          if (isFTU) {
            _setHotspotStatus(menuId);
            _openFirstTimeUseDialog(menuId, false);
          }
        } else {
          if (isFTU) {
            _openFirstTimeUseDialog(menuId, true);
          }
        }
      }
    }

    function _handleHotspotEvent(evt) {
      if (evt.key === 'Enter') {
        if (!hasSIMCard) {
          _showSIMCardDialog();
          return;
        }

        if (!isDataEnabled) {
          _showNetworkDialog();
          evt.preventDefault();
          evt.stopPropagation();
          return;
        }

        if ((!isWifiEnabled &&!isFTUHotspot) ||
            hotspot.wifiHotspotSetting) {
          _showSelect();
          return;
        }

        var isConflict = hotspot.wifiHotspotSetting ||
          hotspot.usbHotspotSetting;
        var menuId = 'wifi-hotspot';
        _handleDialogEvent(menuId, isFTUHotspot, isConflict);
      }
    }

    function _handleTetheringEvent(evt) {
      if (evt.key === 'Enter') {
        if (!hotspotSettings.isUSBInserted) {
          return;
        }

        if (!hasSIMCard) {
          _showSIMCardDialog();
          return;
        }

        if (!isDataEnabled) {
          _showNetworkDialog();
          evt.preventDefault();
          evt.stopPropagation();
          return;
        }

        if ((!isWifiEnabled && !isFTUTethering) ||
            hotspot.usbHotspotSetting) {
          _showSelect();
          return;
        }

        var isConflict = hotspot.wifiHotspotSetting ||
          hotspot.usbHotspotSetting;
        var menuId = 'usb-tethering';
        _handleDialogEvent(menuId, isFTUTethering, isConflict);
      }
    }

    function _openWaringDialog(menuId) {
      var header = '',
        message = '';
      if (menuId === 'wifi-hotspot') {
        header = 'is-warning-wifi-header';
        message = 'is-warning-hotspot-wifi-message';
      } else {
        header = 'is-warning-tethering-header';
        message = 'is-warning-hotspot-tethering-message';
      }

      return new Promise((resolve, reject) => {
        var dialogConfig = {
          title: {id: header, args: {}},
          body: {id: message, args: {}},
          cancel: {
            name: 'Cancel',
            l10nId: 'cancel',
            priority: 1,
            callback: function() {
              reject();
            }
          },
          confirm: {
            name: 'Turn On',
            l10nId: 'turnOn',
            priority: 3,
            callback: function() {
              _setHotspotStatus(menuId);
              resolve();
            }
          }
        };

        var dialog = new ConfirmDialogHelper(dialogConfig);
        dialog.show(document.getElementById('app-confirmation-dialog'));
      });
    }

    function _openFirstTimeUseDialog(menuId, enableDoneKey) {
      // If the Hotspot has switched on, we should show the "OK" soft-key.
      // Otherwise, we need to show the "Turn On" and "Cancel" soft-key.
      var key = '',
        header = '',
        message = '';
      if (menuId === 'wifi-hotspot') {
        key = FTU_HOTSPOT_KEY;
        header = 'is-warning-wifi-header';
        message = 'is-ftu-hotspot-wifi';
      } else {
        key = FTU_TETHERING_KEY;
        header = 'is-warning-tethering-header';
        message = 'is-ftu-hotspot-tethering';
      }

      var dialogConfig = {
        title: {id: header, args: {}},
        body: {id: message, args: {}},
      };

      if (enableDoneKey) {
        dialogConfig.cancel = {
          name: 'Cancel',
          l10nId: 'cancel',
          priority: 1,
          callback: function() {}
        };
        dialogConfig.confirm = {
          name: 'Turn On',
          l10nId: 'turnOn',
          priority: 3,
          callback: function() {
            _setHotspotStatus(menuId);
            _setStorageItem(key, 'false');
            _updateFirstTimeUseConfig();
          }
        };
      } else {
        dialogConfig.accept = {
          name: 'OK',
          l10nId: 'ok',
          priority: 2,
          callback: function() {
            _setStorageItem(key, 'false');
            _updateFirstTimeUseConfig();
          }
        };
      }

      var dialog = new ConfirmDialogHelper(dialogConfig);
      dialog.show(document.getElementById('app-confirmation-dialog'));
    }

    function _showSIMCardDialog() {
      _showConfrimDialog('no-sim-card-message-1', 'no-sim-card-message-2');
    }

    function _showNetworkDialog() {
      _showConfrimDialog('trun-on-network-message', '');
    }

    function _showNoNetworkDialog() {
      _showConfrimDialog('no-network-message', '');
    }

    function _showConfrimDialog(message, desc) {
      var dialogConfig = {
        title: {id: 'internetSharing', args: {}},
        body: {id: message, args: {}},
        desc: {id: desc, args: {}},
        accept: {
          name: 'OK',
          l10nId: 'ok',
          priority: 2,
          callback: function() {},
        }
      };

      var dialog = new ConfirmDialogHelper(dialogConfig);
      dialog.show(document.getElementById('app-confirmation-dialog'));
    }

    /**
     * At Gecko side, when you want to switch on Hotspot,
     *   it would disable the Wi-Fi first and enable Hotspot then.
     * Turning off Hotspot, Gecko would delay 10s to recover the Wi-Fi status.
     * So, we don't need to handle it at Gaia side except USB Tethering.
     */
    function _setHotspotStatus(menuId) {
      if (menuId === 'wifi-hotspot') {
        hotspot.checkIncompatibleSettings(
          hotspot.tetheringWifiKey, true);
      } else {
        // If we want to switch on USB Tethering,
        //   we should turn off Wi-Fi first.
        if (isWifiEnabled) {
          recoverWifiState = true;
          var request = SettingsListener.getSettingsLock().set({
            'wifi.enabled': false
          });

          request.onsuccess = () => {
            hotspot.checkIncompatibleSettings(
              hotspot.tetheringUsbKey, true);
          };
        } else {
          recoverWifiState = false;
          hotspot.checkIncompatibleSettings(
            hotspot.tetheringUsbKey, true);
        }
      }
    }

    function _updateWifiStatus(enabled) {
      isWifiEnabled = enabled;
    }

    function _updateHotspotSecurity(newValue) {
      elements.hotspotSecurityType.setAttribute('data-l10n-id',
        'hotspot-' + newValue);
      var password = elements.panel.querySelector('.wifi-password');
      password.hidden = (newValue === 'open') ? true : false;
    }

    function _updateHotspotSSID(newValue) {
      elements.hotspotSSID.textContent = newValue;
    }

    function _updateHotspotPassword(newValue) {
      //Bug-517 Modified-BEGIN by ganghongliu@t2mobile.com 2017.11.15
      var passwordType = localStorage.getItem('hotspot_wifi_setting_pwdtype');
      if ( passwordType === 'password' ) {
          elements.hotspotPassword.textContent = '**********';
      }else {
          elements.hotspotPassword.textContent = newValue;
      }
      //Bug-517 Modified-END 2017.11.15
    }

    function _setHotspotSettingsEnabled(enabled) {
      // To disable the "Hotspot Settings" button when internet sharing is ON
      elements.hotspotSettingBtn.setAttribute('aria-disabled', enabled);
      elements.hotspotSelectElement.value = enabled;

      // @HACK To remove the SettingBtn envent listener
      if (enabled) {
        elements.hotspotSettingBtn.removeEventListener('click',
          _onHotspotSettingsClick);
        elements.hotspotSettingBtn.classList.add('none-select');
      } else {
        elements.hotspotSettingBtn.addEventListener('click',
          _onHotspotSettingsClick);
        elements.hotspotSettingBtn.classList.remove('none-select');
      }
    }

    /**
     * There is no USB connected, we should disable the USB Tethering menu item.
     */
    function _updateUSBTetheringMenu(enabled) {
      elements.usbTethering.setAttribute('aria-disabled', !enabled);
      // When USB connection is lost, we need to turn off the USB Tethering
      if (hotspot.usbHotspotSetting && !enabled) {
        hotspot.checkIncompatibleSettings(
          hotspot.tetheringUsbKey, false);
      }
    }

    function _updateUSBTetheringSelect(enabled) {
      elements.usbTetheringSelectElement.value = enabled;
    }

    function _onWifiHotspotChange(evt) {
      var enabled = (evt.target.value === 'true' || false);
      hotspot.checkIncompatibleSettings(
        hotspot.tetheringWifiKey, enabled);
    }

    function _onUSBTetheringChange(evt) {
      var enabled = (evt.target.value === 'true' || false);
      // We need to recover the Wi-Fi status after turn off the USB Tethering
      if (recoverWifiState && !enabled) {
        var request = SettingsListener.getSettingsLock().set({
          'wifi.enabled': true
        });

        request.onsuccess = () => {
          hotspot.checkIncompatibleSettings(
            hotspot.tetheringUsbKey, false);
        };
      } else {
        hotspot.checkIncompatibleSettings(
          hotspot.tetheringUsbKey, enabled);
      }
    }

    function _onHotspotSettingsClick() {
      SettingsService.navigate('hotspot-wifiSettings', {
        settings: hotspotSettings
      });
    }

    function _updateUI() {
      _updateHotspotSSID(hotspotSettings.hotspotSSID);
      _updateHotspotSecurity(hotspotSettings.hotspotSecurity);
      _updateHotspotPassword(hotspotSettings.hotspotPassword);
      _updateUSBTetheringMenu(hotspotSettings.isUSBInserted);
    }

    function _initActiveSIMSlot() {
      SIMSlotManager.getSlots().forEach((SIMSlot) => {
        if (!SIMSlot.isAbsent()) {
          activeSIMCard = SIMSlot;
        }
      });

      if (activeSIMCard !== null) {
        activeSIMCard.conn.addEventListener('cardstatechange',
          _updateActiveSIMSlot);
        _updateActiveSIMSlot();
      } else {
        hasSIMCard = false;
      }
    }

    function _updateActiveSIMSlot(evt) {
      var cardState = activeSIMCard.simCard.cardState;
      switch (cardState) {
        case null:
        case 'unknown':
          hasSIMCard = false;
          break;
        default:
          hasSIMCard = true;
          break;
      }
    }

    function _updateDataConnectionStatus(enabled) {
      isDataEnabled = enabled;
    }

    function _getStorageBoolean(key) {
      var boolValue = _getStorageItem(key);
      var enabled = (boolValue === 'true' || false);
      return enabled;
    }

    function _getStorageItem(key) {
      return localStorage.getItem(key);
    }

    function _setStorageItem(key, value) {
      localStorage.setItem(key, value);
    }

    function _initFirstTimeUseConfig() {
      if (_getStorageItem(FTU_HOTSPOT_KEY) === null) {
        _setStorageItem(FTU_HOTSPOT_KEY, 'true');
        isFTUHotspot = _getStorageBoolean(FTU_HOTSPOT_KEY);
      }

      if (_getStorageItem(FTU_TETHERING_KEY) === null) {
        _setStorageItem(FTU_TETHERING_KEY, 'true');
        isFTUTethering = _getStorageBoolean(FTU_TETHERING_KEY);
      }
    }

    function _updateFirstTimeUseConfig() {
      isFTUHotspot = _getStorageBoolean(FTU_HOTSPOT_KEY);
      isFTUTethering = _getStorageBoolean(FTU_TETHERING_KEY);
    }

    //[task-5277231/5277233] modify by shangpeng.su 2017.09.12 begin
    function _checkIfNeedTetheringMenuOptions () {
      var wfhotspotNeed = true;
      var usbTeteringNeed = true;
      navigator.customization.getValue(
        'wifi.hotspot.support').then((result) => {
        wfhotspotNeed = JSON.stringify(result) == '1' ? false : true;
        if (wfhotspotNeed) {
          elements.wifiHotspot.removeAttribute('hidden');
          elements.wifiHotspotHeader.removeAttribute('hidden');
          elements.hotspotProp.removeAttribute('hidden');
        } else {
          elements.wifiHotspot.setAttribute('hidden', true);
          elements.wifiHotspotHeader.setAttribute('hidden', true);
          elements.hotspotProp.setAttribute('hidden', true);
        }
      });
      navigator.customization.getValue(
        'usb.tethering.support').then((result) => {
        usbTeteringNeed = JSON.stringify(result) == '1' ? false : true;
        if (usbTeteringNeed) {
          elements.usbTethering.removeAttribute('hidden');
          elements.usbTetheringHeader.removeAttribute('hidden');
        } else {
          elements.usbTethering.setAttribute('hidden', true);
          elements.usbTetheringHeader.setAttribute('hidden', true);
        }
      });
    }
    //[task-5277231/5277233] modify end

    return SettingsPanel({
      onInit: function(panel) {
        this._incompatibleSettingsDialog = 'incompatible-settings-dialog';
        //[task-5277231/5277233] modify by shangpeng.su 2017.09.12 begin
        elements = {
          panel: panel,
          panelLiItems: panel.querySelectorAll('li'),
          wifiHotspot: document.getElementById('wifi-hotspot'),
          hotspotSSID: document.getElementById('network-name-desc'),
          hotspotPassword: document.getElementById('wifi-password-desc'),
          showPasswordHotspot: document.getElementById('showPassword-hotspot'),
          hotspotSecurityType: document.getElementById('wifi-security-desc'),
          hotspotSettingBtn: document.getElementById('hotspot-settings-section'),
          hotspotSelectElement: panel.querySelector('#wifi-hotspot select'),
          wifiHotspotHeader: document.getElementById('wifi-hotspot-header'),
          hotspotProp: document.getElementById('wifi-hotspot-prop'),
          usbTethering: document.getElementById('usb-tethering'),
          usbTetheringHeader: document.getElementById('usb-tethering-header'),
          usbTetheringSelectElement: panel.querySelector('#usb-tethering select')
        };
        //[task-5277231/5277233] modify end

        this.incompatibleSettingsHandler =
          this._openIncompatibleSettingsDialog.bind(this);

        hotspot.init();

        //add for task5344156 by shangpeng.su 2017.09.20
        this._setCustomSSID();

        if (localStorage.getItem('isSupportWifiDevice') !== 'true') {
          var el = document.getElementById('hotspot-wifi');
          el && el.classList.add('hidden');
        }
        _initFirstTimeUseConfig();
      },

      onBeforeShow: function(panel, options) {
        _initActiveSIMSlot();

        // Incompatible settings
        hotspot.addEventListener('incompatibleSettings',
          this.incompatibleSettingsHandler);

        // Wi-fi hotspot event listener
        elements.hotspotSelectElement.addEventListener('change',
          _onWifiHotspotChange);

        // USB tethering event listener
        elements.usbTetheringSelectElement.addEventListener('change',
          _onUSBTetheringChange);

        hotspotSettings.observe('hotspotSSID', _updateHotspotSSID);

        hotspotSettings.observe('hotspotPassword', _updateHotspotPassword);

        // Localize WiFi security type string when setting changes
        hotspotSettings.observe('hotspotSecurity', _updateHotspotSecurity);

        // Update the USB Tethering status
        hotspotSettings.observe('isUSBInserted', _updateUSBTetheringMenu);

        // Update current wifi status
        SettingsListener.observe('wifi.enabled', false,
          _updateWifiStatus);

        // Update current USB rethering status and "Select" item value
        SettingsListener.observe('tethering.usb.enabled', false,
          _updateUSBTetheringSelect);

        // Update current Hotspot status and
        //   enable/disable "Hotspot Settings" option menu
        SettingsListener.observe('tethering.wifi.enabled', true,
          _setHotspotSettingsEnabled);

        SettingsListener.observe('ril.data.enabled', false,
          _updateDataConnectionStatus);

        elements.wifiHotspot.addEventListener('keydown', _handleHotspotEvent);

        elements.usbTethering.addEventListener('keydown', _handleTetheringEvent);

        _updateUI();

        _initAllFocusEvent();

        _updateFirstTimeUseConfig();

        //[task-5277231/5277233] modify by shangpeng.su 2017.09.12 begin
        _checkIfNeedTetheringMenuOptions();
        //[task-5277231/5277233] modify end
      },

      onBeforeHide: function(panel, options) {
        // Incompatible settings
        hotspot.removeEventListener('incompatibleSettings',
          this.incompatibleSettingsHandler);

        // Wi-fi hotspot event listener
        elements.hotspotSelectElement.removeEventListener('change',
          _onWifiHotspotChange);

        // USB tethering event listener
        elements.usbTetheringSelectElement.removeEventListener('change',
          _onUSBTetheringChange);

        elements.hotspotSettingBtn.removeEventListener('click',
          _onHotspotSettingsClick);

        hotspotSettings.unobserve('hotspotSSID');

        hotspotSettings.unobserve('hotspotPassword');

        hotspotSettings.unobserve('hotspotSecurity');

        hotspotSettings.unobserve('isUSBInserted');

        SettingsListener.unobserve('wifi.enabled',
          _updateWifiStatus);

        SettingsListener.unobserve('tethering.usb.enabled',
          _updateUSBTetheringSelect);

        SettingsListener.unobserve('tethering.wifi.enabled',
          _setHotspotSettingsEnabled);

        SettingsListener.unobserve('ril.data.enabled',
          _updateDataConnectionStatus);

        elements.wifiHotspot.removeEventListener('keydown', _handleHotspotEvent);

        elements.usbTethering.removeEventListener('keydown', _handleTetheringEvent);

        if (activeSIMCard !== null) {
          activeSIMCard.conn.removeEventListener('cardstatechange',
            _updateActiveSIMSlot);
        }

        _removeAllFocusEvent();

        SettingsSoftkey.hide();
      },

      _openIncompatibleSettingsDialog: function(newSetting, oldSetting, bothConflicts) {
        // We must check if there is two incompatibilities
        // (usb hotspot case) or just one
        if (bothConflicts) {
          openIncompatibleSettingsDialog(this._incompatibleSettingsDialog,
            hotspot.tetheringUsbKey, hotspot.tetheringWifiKey,
            this._openSecondWarning.bind(this));
        } else {
          openIncompatibleSettingsDialog(this._incompatibleSettingsDialog,
            newSetting, oldSetting, null);
        }
      },

      _openSecondWarning: function() {
        openIncompatibleSettingsDialog(this._incompatibleSettingsDialog,
          hotspot.tetheringUsbKey, hotspot.usbStorageKey,
          null);
      },

      //add for task5344156 by shangpeng.su 2017.09.20
      _setCustomSSID: function() {
        var modelName = navigator.kaiosExtension.getPropertyValue('ro.product.name');

        try {
          var req = SettingsListener.getSettingsLock().get('tethering.wifi.ssid');
          req.addEventListener('success', (function onsuccess() {
            var value = req.result['tethering.wifi.ssid'];

            if (value == '') {
              console.log('set default hotspot ssid as modelName ' + modelName);
              _updateHotspotSSID(modelName);
              hotspotSettings.setHotspotSSID(modelName);
            } else {
              _updateHotspotSSID(value);
            }
          }));
        } catch(e) {
          _updateHotspotSSID(modelName);
          hotspotSettings.setHotspotSSID(modelName);
        }
      }
      //add end
    });
  };
});
