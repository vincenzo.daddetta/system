
/**
  * This SettingsList module copy from shared/js/action_list.
  * It mainly display the action menu in Settings app.
  * It will be customized and made it suitable for Settings.
  */
/* global SoftkeyPanel */

define('modules/settings_list',['require','shared/mobile_operator'],function(require) {
  
  var MobileOperator = require('shared/mobile_operator');
  var _config = null;
  var _container = null;
  var _visibility = false;
  var _wrapper = document.getElementById('settings-list');

  // Set the default of softkey bar
  var _softkey = new SoftkeyPanel({
    menuClassName: 'menu-button',
    items: [{
      name: '',
      priority: 1,
      method: function() {}
    }]
  });

  /**
    * Init the current action list container
    */
  var _init = function(config) {
    _config = config;
    if (_container !== null) {
      _clearItems();
    }
    _initSoftKey(config);
    _wrapper.innerHTML = '<div class="visible" tabindex="0">' +
      '<h1 class="header"></h1><div>'+
      '<ul class="container" role="menu"></ul>' +
      '</div></div>';
    _container = _wrapper.firstElementChild;
    _createList(config, _container);
    return new Promise((resolve) => {
      resolve();
    });
  };

  /**
    * Init the settings list softkey
    */
  var _initSoftKey = function(config) {
    // Hide the default softkey first
    _softkey.hide();

    var softkeyParams = {
      menuClassName: 'menu-button',
      header: {},
      items: []
    };

    if (config.accept) {
      softkeyParams.items.push({
        name: '',
        priority: 2,
        l10nId: config.accept.l10nId,
        method: function() {}
      });
    }

    _softkey.initSoftKeyPanel(softkeyParams);
  };

  /**
    * Init the settings list focus and keydown event
    */
  var _initNavigation = function() {
    window.dispatchEvent(new CustomEvent('panelready', {
      detail: {
        previous : '#root',
        current: '#settings-list'
      }
    }));

    // Dispatch the settings list show event
    window.dispatchEvent(new CustomEvent('settings-list-event', {
      detail: {
        isVisible: _visibility
      }
    }));

    _wrapper.addEventListener('keydown', _handleEvent);
  };

  /**
    * Restore the focus to previous panel and remove the keydown event
    */
  var _restoreNavigation = function() {
    // After settings list hidden, we should focus previous panel item
    window.dispatchEvent(new CustomEvent('panelready', {
      detail: {
        previous : '#settings-list',
        current: '#root',
      }
    }));

    window.dispatchEvent(new CustomEvent('settings-list-event', {
      detail: {
        isVisible: _visibility
      }
    }));

    _wrapper.removeEventListener('keydown', _handleEvent);
  };

  // Get the current item value via focused element
  var _getCurrentValue = function(focusedElement) {
    var value = null;
    if (focusedElement) {
      var index = focusedElement.dataset.index;
      value = _config.items[index].value;
      if (value === undefined) {
        value = null;
      }
    }
    return value;
  };

  /**
    * Handle all keydown event and hide settings list event
    */
  var _handleEvent = function(evt) {
    // handle keydown event
    switch (evt.key) {
      case 'Backspace':
        _hideList();
        evt.preventDefault();
        break;
      case 'Enter':
        _hideList();
        if (_config.accept && !evt.target.disabled) {
          var value = _getCurrentValue(evt.target);
          var accept = _config.accept.callback;
          accept && accept(value);
        }
        evt.preventDefault();
        break;
    }
  };

  /**
    * Pop up the settings list
    */
  var _showList = function() {
    if (_wrapper.hidden) {
      _wrapper.hidden = false;
      _visibility = true;
      _softkey.show();
      _initNavigation();
    }
  };

  /**
    * Hide the current settings list
    */
  var _hideList = function() {
    if (!_wrapper.hidden) {
      _wrapper.hidden = true;
      _visibility = false;
      _softkey.hide();
      _restoreNavigation();
    }
  };

  /**
    * Remove current items and panel
    */
  var _clearItems = function() {
    _container.parentNode.removeChild(_container);
    _container = null;
  };

  /**
    * Create the settings list header and content container
    */
  var _createList = function(context, container) {
    var header = container.querySelector('.header');
    if (context.title) {
      navigator.mozL10n.setAttributes(header,
        context.title.id,
        context.title.args);
    } else {
      header.setAttribute('data-l10n-id', 'select');
    }

    var contentList = container.querySelector('.container');
    context.items.forEach((item, index) => {
      _createItems(contentList, item, index);
    });
  };

  /**
    * Create the settings list item
    */
  var _createItems = function(container, item, index) {
    var li = document.createElement('li');
    li.setAttribute('role', 'menuitem');
    li.dataset.index = index;
    if (item.disabled !== undefined) {
      li.setAttribute('aria-disabled', item.disabled);
      li.disabled = item.disabled;
    }

    var iccManager = navigator.mozIccManager;
    var conn = navigator.mozMobileConnections[index];
    var iccId = conn.iccId;
    var icc = iccManager.getIccById(iccId);
    if (icc) {
      var operatorInfo = MobileOperator.userFacingInfo(conn);
      if (operatorInfo.operator) {
        item.id = 'sim-with-index-and-carrier';
        item.args.carrier = operatorInfo.operator;
      } else {
        item.id = 'noOperator-with-index';
      }
    } else {
      item.id = 'noSim-with-index';
      item.disabled = true;
    }

    var span = document.createElement('span');
    navigator.mozL10n.setAttributes(span, item.id, item.args);
    // Modified by yingsen.zhang@t2mbole.com for sync SIM card name 2018.01.02 begin
    updateSIMCardName(index, function(cardIndex, cardName) {
      if (cardName && cardName.length) {
        if (item.args.carrier) {
          span.textContent = cardName + ' - ' + item.args.carrier;
        } else {
          span.textContent = cardName + ' - ' + navigator.mozL10n.get('no-operator');
        }
      }
    });
    // Modified by yingsen.zhang@t2mbole.com for sync SIM card name 2018.01.02 end
    li.appendChild(span);
    container.appendChild(li);
  };

  return {
    show: function _showSettingsList(config) {
      return new Promise((resolve) => {
        var promise = _init(config);
        promise && promise.then(() => {
          _showList();
          resolve();
        });
      });
    }
  };
});

/* global TelephonySettingHelper */
/**
 * The module loads scripts used by the root panel. In the future these scripts
 * must be converted to AMD modules. Implementation details please refer to
 * {@link Root}.
 *
 * @module root/root
 */
define('panels/root/root',['require','shared/lazy_loader','shared/toaster'],function(require) {
  

  var LazyLoader = require('shared/lazy_loader');
  var Toaster = require('shared/toaster');
  /**
   * @alias module:root/root
   * @class Root
   * @requires module:shared/lazy_loader
   * @returns {Root}
   */
  function Root() {}

  Root.prototype = {
    _loadScripts: function root_loadScripts() {
      /**
       * Enable or disable the menu items related to the ICC card
       * relying on the card and radio state.
       */
      LazyLoader.load([
        'js/firefox_accounts/menu_loader.js',
        'js/telephony_settings.js',
        'js/telephony_items_handler.js'
      ], function() {
        TelephonySettingHelper
          .init()
          .then(function telephonySettingInitDone() {
            window.dispatchEvent(new CustomEvent('telephony-settings-loaded'));
          });
      });
    },

    init: function root_init() {
      // Load the necessary scripts after the UI update.
      setTimeout(this._loadScripts);
      RootManager.init();
    }
  };

  return function ctor_root() {
    return new Root();
  };
});

/**
 * This module is used to control the background stuff when users
 * toggle on/off airplane mode checkbox.
 *
 * @module panels/root/airplane_mode_item
 */
/* global DsdsSettings */
define('panels/root/airplane_mode_item',['require','shared/toaster','shared/airplane_mode_helper','shared/settings_listener'],function(require) {
  
  var Toaster = require('shared/toaster');
  var AirplaneModeHelper = require('shared/airplane_mode_helper');
  var SettingsListener = require('shared/settings_listener');

  /**
   * @alias module:panels/root/airplane_mode_item
   * @class AirplaneModeItem
   * @param {HTMLElement} element the checkbox for airplane mode
   * @returns {AirplaneModeItem}
   */
  function AirplaneModeItem(element) {
    this._itemEnabled = false;
    this._element = element;
    this.init();
    this._boundAPMStateChange = this._onAPMStateChange.bind(this);
  }

  AirplaneModeItem.prototype = {
    /**
     * The value indicates whether the module is responding.
     *
     * @access public
     * @memberOf AirplaneModeItem.prototype
     * @type {Boolean}
     */
    set enabled(value) {
      if (this._itemEnabled === value) {
        return;
      } else {
        this._itemEnabled = value;
        if (this._itemEnabled) {
          AirplaneModeHelper.addEventListener('statechange',
            this._boundAPMStateChange);
        } else {
          AirplaneModeHelper.removeEventListener('statechange',
            this._boundAPMStateChange);
        }
      }
    },

    /**
     * The value indicates whether the module is responding.
     *
     * @access public
     * @memberOf AirplaneModeItem.prototype
     * @type {Boolean}
     */
    get enabled() {
      return this._itemEnabled;
    },

    /**
     * This function is used to reflect current status of APM to checkbox
     *
     * @access private
     * @memberOf AirplaneModeItem.prototype
     * @param {String} status current status of APM
     * @type {Function}
     */
    _onAPMStateChange: function ami_onAPMStateChange(status) {
      if (status === 'enabled' || status === 'disabled') {
        this._element.classList.remove('disabled');
      } else {
        this._element.classList.add('disabled');
      }
    },

    _onChangeAirplaneModeMenu: function ami_onChangeAirplaneModeMenu(status) {
      var enabled =
        (status === 'enabled' || status === 'enabling') ? true : false;
      this.airplaneModeDesc.setAttribute('data-l10n-id', enabled ? 'on' : 'off');
      if (status === 'enabled' || status === 'disabled') {
        this._element.parentNode.removeAttribute('aria-disabled');
        this._element.setAttribute('href', '#airplane-mode');
      } else {
        this._element.parentNode.setAttribute('aria-disabled', true);
        this._element.removeAttribute('href');
      }
    },


    /**
     * Initialize function
     *
     * @access public
     * @memberOf AirplaneModeItem.prototype
     * @type {Function}
     */
    init: function ami_init() {
      var self = this;
      var toast = {
        messageL10nId: 'changessaved',
        latency: 2000,
        useTransition: true
      };
      AirplaneModeHelper.ready(function() {
        // initial status
        var status = AirplaneModeHelper.getStatus();
        var enabled = (status === 'enabled') ? true : false;
        var airplaneModeDesc = document.getElementById('airplaneMode-desc');
        airplaneModeDesc.setAttribute('data-l10n-id', enabled ? 'on' : 'off');
        this._element.classList.remove('disabled');
      }.bind(this));

      // initial Geolocation settings observer
      SettingsListener.observe('geolocation.enabled', false, value => {
        var geoDesc = document.getElementById('geolocation-desc');
        geoDesc.setAttribute('data-l10n-id', value ? 'on' : 'off');
      });

      // initial Data Connection settings observer
      SettingsListener.observe('ril.data.enabled', false, value => {
        var dataConnDesc = document.getElementById('mobilenetworkanddata-desc');
        dataConnDesc.setAttribute('data-l10n-id', value ? 'on' : 'off');
      });

      this.airplaneModeDesc = document.getElementById('airplaneMode-desc');
      SettingsListener.observe('airplaneMode.status', false,
        this._onChangeAirplaneModeMenu.bind(this));
      window.addEventListener('airplaneModeChange', e => {
        this._onChangeAirplaneModeMenu(e.detail.status);
      });
    }
  };

  return function ctor_airplane_mode_item(element) {
    return new AirplaneModeItem(element);
  };
});

/**
 * The moudle supports displaying nfc toggle on an element.
 *
 * @module panels/root/nfc_item
 */
define('panels/root/nfc_item',['require','shared/toaster','shared/settings_listener'],function(require) {
  

  var Toaster = require('shared/toaster');
  var SettingsListener = require('shared/settings_listener');

  /**
   * @alias module:panels/root/nfc_item
   * @class NFCItem
   * @param {Object} elements
   * @param {HTMLElement} elements.nfcMenuItem
   * @param {HTMLElement} elements.nfcSelect
   * @returns {NFCItem}
   */
  function NFCItem(elements) {
    if (!navigator.mozNfc) {
      return;
    }

    elements.parentNode.hidden = false;
    this._menuItem = elements;
    SettingsListener.observe('nfc.enabled', false, value => {
      var nfcDesc = document.getElementById('nfc-desc');
      nfcDesc.setAttribute('data-l10n-id', value ? 'on' : 'off');
    });

    SettingsListener.observe('nfc.status', undefined,
                             (status) => this._onNfcStatusChanged(status));
  }

  NFCItem.prototype = {
    // When the NFC is changing, we should disable the item.
    _onNfcStatusChanged: function ni_onNfcStatusChanged(status) {
      if (status === 'enabling' || status === 'disabling') {
        this._menuItem.parentNode.setAttribute('aria-disabled', true);
        this._menuItem.removeAttribute('href');
      } else if (status === 'enabled' || status === 'disabled') {
        this._menuItem.parentNode.setAttribute('aria-disabled', false);
        this._menuItem.setAttribute('href', '#nfc');
      }
    }
  };

  return function ctor_nfcItem(elements) {
    return new NFCItem(elements);
  };
});

define('panels/root/stk_item',['require','shared/stk_helper'],function(require) {
  
  var STKHelper = require('shared/stk_helper');

  function STKItem(elements) {
    this._elements = elements;
    this.init();
  }

  STKItem.prototype.init = function() {
    var iccLoaded = false;
    var self = this;
    function loadIccPage(callback) {
      callback = (typeof callback === 'function') ? callback : function() {};
      if (iccLoaded) {
        return callback();
      }
      Settings.currentPanel = '#icc';
      window.addEventListener('iccPageLoaded',
        function oniccPageLoaded(event) {
          iccLoaded = true;
          callback();
        });
    }

    function executeIccCmd(iccMessage) {
      if (!iccMessage) {
        return;
      }

      // Clear cache
      var reqIccData = window.navigator.mozSettings.createLock().set({
        'icc.data': null
      });
      reqIccData.onsuccess = function icc_getIccData() {
        window.DUMP('ICC Cache cleared');
      };

      // Open ICC section
      window.DUMP('ICC message to execute: ', iccMessage);
      loadIccPage(function() {
        var event = new CustomEvent('stkasynccommand', {
          detail: { 'message': iccMessage }
        });
        window.dispatchEvent(event);
      });
    }

    setTimeout(function updateStkMenu() {
      window.DUMP('Showing STK main menu');
      // XXX https://bugzilla.mozilla.org/show_bug.cgi?id=844727
      // We should use Settings.settingsCache first
      var settings = Settings.mozSettings;
      var lock = settings.createLock();

      function showStkEntries(menu) {
        window.DUMP('STK cached menu: ', menu);
        if (!menu || typeof menu !== 'object' ||
          Object.keys(menu).length === 0) {
            window.DUMP('No STK available - exit');
            self._elements.iccMainHeader.hidden = true;
            self._elements.iccEntries.hidden = true;
            return;
        }

        // Clean current entries
        self._elements.iccEntries.innerHTML = '';
        self._elements.iccMainHeader.hidden = true;
        self._elements.iccEntries.hidden = true;
        // update and show the entry in settings
        Object.keys(menu).forEach(function(SIMNumber) {
          window.DUMP('STK Menu for SIM ' + SIMNumber +
            ' (' + menu[SIMNumber].iccId + ') - ', menu[SIMNumber].entries);

          var li = document.createElement('li');
          var a = document.createElement('a');
          var menuItem = menu[SIMNumber].entries;
          var icon = STKHelper.getFirstIconRawData(menuItem);

          a.id = 'menuItem-icc-' + menu[SIMNumber].iccId;
          a.className = 'menu-item menuItem-icc';
          a.href = '#icc';
          a.onclick = function menu_icc_onclick() {
            window.DUMP('Touched ' + menu[SIMNumber].iccId);
            loadIccPage(function() {
              var event = new CustomEvent('stkmenuselection', {
                detail: { 'menu': menu[SIMNumber] }
              });
              window.dispatchEvent(event);
            });
          };

          var span = document.createElement('span');
          span.textContent = menu[SIMNumber].entries.title;
          a.appendChild(span);

          if (Object.keys(menu).length > 1) {
            var small = document.createElement('small');
            small.setAttribute('data-l10n-id', 'sim' + SIMNumber);
            small.classList.add('menu-item-desc');
            a.appendChild(small);
          }

          li.appendChild(a);
          self._elements.iccEntries.appendChild(li);

          self._elements.iccMainHeader.hidden = false;
          self._elements.iccEntries.hidden = false;
        });
      }

      // Check if SIM card sends an Applications menu
      var reqApplications = lock.get('icc.applications');
      reqApplications.onsuccess = function icc_getApplications() {
        var json = reqApplications.result['icc.applications'];
        var menu = json && JSON.parse(json);
        showStkEntries(menu);
      };

      settings.addObserver('icc.applications',
        function icc_getApplications(event) {
          var json = event.settingValue;
          var menu = json && JSON.parse(json);
          window.DUMP('STK Settings.currentPanel ' +  Settings.currentPanel);
          if (Settings && Settings.currentPanel === '#icc') {
             Settings.currentPanel = '#root';
          }
          showStkEntries(menu);
        });

      // Check if there are pending STK commands
      var reqIccData = lock.get('icc.data');
      reqIccData.onsuccess = function icc_getIccData() {
        var cmd = reqIccData.result['icc.data'];
        if (cmd) {
          window.DUMP('ICC async command (launcher)');
          executeIccCmd(JSON.parse(cmd));
        }
      };

      settings.addObserver('icc.data', function(event) {
        var value = event.settingValue;
        if (value) {
          window.DUMP('ICC async command while settings running: ', value);
          executeIccCmd(JSON.parse(value));
        }
      });
    }.bind(this));
  };

  return function ctor_stk_item(elements) {
    return new STKItem(elements);
  };
});

/* global SettingsSoftkey */

define('panels/root/panel',['require','modules/settings_list','shared/mobile_operator','modules/settings_service','modules/settings_panel','panels/root/root','panels/root/airplane_mode_item','panels/root/nfc_item','panels/root/stk_item','modules/bluetooth/version_detector','dsds_settings'],function(require) {
  

  var SettingsList = require('modules/settings_list');
  var MobileOperator = require('shared/mobile_operator');
  var SettingsService = require('modules/settings_service');
  var SettingsPanel = require('modules/settings_panel');
  var Root = require('panels/root/root');
  var AirplaneModeItem = require('panels/root/airplane_mode_item');
  var NFCItem = require('panels/root/nfc_item');
  var STKItem = require('panels/root/stk_item');
  var BTAPIVersionDetector = require('modules/bluetooth/version_detector');
  var DsdsSettings = require('dsds_settings');

  const TELEPHONY_ITEMS = [
    'call-settings'
  ];

  const PANEL_MAPPING = {
    'call-settings': 'call'
  };

  const TETHERING_SUPPORT_KEY = 'tethering.support';

  var queryRootForLowPriorityItems = function(panel) {
    // This is a map from the module name to the object taken by the constructor
    // of the module.
    var storageDialog = document.querySelector('.turn-on-ums-dialog');
    return {
      'BluetoothItem': panel.querySelector('.bluetooth-desc'),
      'LanguageItem': panel.querySelector('.language-desc'),
      'BatteryItem': panel.querySelector('.battery-desc'),
      'StorageUSBItem': {
        mediaStorageDesc: panel.querySelector('.media-storage-desc'),
        usbEnabledCheckBox: panel.querySelector('.usb-switch'),
        usbStorage: panel.querySelector('#menuItem-enableStorage'),
        usbEnabledInfoBlock: panel.querySelector('.usb-desc'),
        umsWarningDialog: storageDialog,
        umsConfirmButton: storageDialog.querySelector('.ums-confirm-option'),
        umsCancelButton: storageDialog.querySelector('.ums-cancel-option'),
        mediaStorageSection: panel.querySelector('.media-storage-section')
      },
      'StorageAppItem': panel.querySelector('.application-storage-desc'),
      'WifiItem': panel.querySelector('#wifi-desc'),
      'MobileNetworkAndDataItem': panel.querySelector('#mobilenetworkanddata-desc'),
      'AntitheftItem': panel.querySelector('#antitheft-desc')
    };
  };

  var HWKhandler = function(e) {
    if (e.key === 'Enter') {
      switch (Settings.currentPanel) {
        case '#hotspot':
          break;
        default:
          var select =
            document.querySelector('li:not([aria-disabled="true"]).focus select');
          select && select.focus();
          break;
      }
    }
  };

  return function ctor_root_panel() {
    var root;
    var airplaneModeItem;
    var stkItem;
    var nfcItem;

    var lowPriorityRoots = null;
    var initLowPriorityItemsPromise = null;
    var simPickerConfig = {};
    var _mobileConnections = window.navigator.mozMobileConnections;
    var _currentCapability = null;

    var initLowPriorityItems = function(rootElements) {
      if (!initLowPriorityItemsPromise) {
        initLowPriorityItemsPromise = new Promise(function(resolve) {
          require(['panels/root/low_priority_items'], resolve);
        }).then(function(itemCtors) {
          var result = {};
          Object.keys(rootElements).forEach(function(name) {
            var itemCtor = itemCtors[name];
            if (itemCtor) {
              result[name] = itemCtor(rootElements[name]);
            }
          });
          return result;
        });
      }
      return initLowPriorityItemsPromise;
    };

    function _handleClickEvent(evt) {
      if (evt.key === 'Enter') {
        var disabled = evt.target.getAttribute('aria-disabled');
        var id = evt.target.id;
        if (!disabled) {
          AirplaneModeHelper.ready(function() {
            var imsCapability = _getImsHandlerCapability();
            var status = AirplaneModeHelper.getStatus();
            if ((imsCapability === 'voice-over-wifi' ||
              imsCapability === 'video-over-wifi') &&
              status === 'enabled') {
              var request =
                navigator.mozSettings.createLock().get('ril.data.defaultServiceId');
              request.onsuccess = function onSuccessHandler() {
                var serviceId = request.result['ril.data.defaultServiceId'];
                DsdsSettings.setIccCardIndexForCallSettings(serviceId);
                DsdsSettings.setIccCardIndexForCellAndDataSettings(serviceId);
                SettingsService.navigate(PANEL_MAPPING[id]);
              };
            } else {
              _showDualCardsMenu(PANEL_MAPPING[id]);
            }
          });
        }
      }
    }

    function _addShowMenuEvent() {
      if (DsdsSettings.getNumberOfIccSlots() > 1) {
        TELEPHONY_ITEMS.forEach((id) => {
          var item = document.getElementById(id);
          item.addEventListener('keydown', _handleClickEvent);
        });
      }
    }

    function _removeShowMenuEvent() {
      if (DsdsSettings.getNumberOfIccSlots() > 1) {
        TELEPHONY_ITEMS.forEach((id) => {
          var item = document.getElementById(id);
          item.removeEventListener('keydown', _handleClickEvent);
        });
      }
    }

    function _getDSDSMenuConfig() {
      simPickerConfig = {
        title: {id: 'select', args: {}},
        items: []
      };

      var iccManager = navigator.mozIccManager;
      var conns = navigator.mozMobileConnections;

      return new Promise((resolve) => {
        if (conns) {
          [].forEach.call(conns, (simcard, cardIndex) => {
            var iccId = simcard.iccId;
            var icc = iccManager.getIccById(iccId);
            var item = {
              id: 'sim-with-index-and-carrier',
              args: {
                index: cardIndex + 1
              },
              value: cardIndex
            };

            if (icc) {
              var operatorInfo = MobileOperator.userFacingInfo(simcard);
              if (operatorInfo.operator) {
                item.args.carrier = operatorInfo.operator;
              } else {
                item.id = 'noOperator-with-index';
              }
            } else {
              item.id = 'noSim-with-index';
              item.disabled = true;
            }
            simPickerConfig.items.push(item);
          });
        }
        resolve();
      });
    }

    function _showDualCardsMenu(panel) {
      simPickerConfig.accept = {
        l10nId: 'select',
        callback: function(index) {
          // To prevent the dual cards menu displaying twice
          _removeShowMenuEvent();
          DsdsSettings.setIccCardIndexForCallSettings(index);
          SettingsService.navigate(panel);
        }
      };
      SettingsList.show(simPickerConfig);
    }

    function _updateHotspotMenuItem(panel) {
      var request =
        SettingsListener.getSettingsLock().get(TETHERING_SUPPORT_KEY);

      request.onsuccess = () => {
        // If the tethering support settings key is true,
        //   we would show the Internet Sharing menu item.
        var item = panel.querySelector('#internet-sharing');
        var support = request.result[TETHERING_SUPPORT_KEY];
        if (item.hidden === support) {
          item.hidden = !support;
          window.dispatchEvent(new CustomEvent('refresh'));
        }
      };

      request.onerror = () => {
        console.error('ERROR: Get the tethering support flag failure.');
      };
    }


    function _updateAccessibilityMenu() {
      if (localStorage.getItem('isLowMemoryDevice') === 'true') {
        var el = document.getElementById('menuItem-accessibility');
        el && el.parentNode.classList.add('hidden');
      }
    }

    function _updateVolteDisplay(panel) {
      var volteDisplay = panel.querySelector('#volte-settings');
      var volteWifiHeader = document.getElementById('volte-vowifi-header');

      var reqVolte =
        navigator.mozSettings.createLock().get('volte_vowifi_settings.show');
      reqVolte.onsuccess = () => {
        var val = reqVolte.result['volte_vowifi_settings.show'];
        if (volteDisplay.hidden === val) {
          volteDisplay.hidden = !val;
          var mobileConnection = navigator.mozMobileConnections[0];
          var supportedBearers =
            mobileConnection.imsHandler.deviceConfig.supportedBearers;
          if (!supportedBearers || ((supportedBearers.indexOf('wifi') < 0 ||
            localStorage.getItem('isSupportWifiDevice') !== 'true') &&
            supportedBearers.indexOf('cellular') < 0)) {
            volteDisplay.hidden = true;
          }
          if (supportedBearers && supportedBearers.indexOf('cellular') >= 0
            && (supportedBearers.indexOf('wifi') < 0 ||
            localStorage.getItem('isSupportWifiDevice') !== 'true')) {
            volteWifiHeader && volteWifiHeader.setAttribute('data-l10n-id', 'volte');
          } else if (supportedBearers && supportedBearers.indexOf('cellular') < 0
            && (supportedBearers.indexOf('wifi') >= 0 ||
            localStorage.getItem('isSupportWifiDevice') === 'true')) {
            volteWifiHeader && volteWifiHeader.setAttribute('data-l10n-id', 'vowifi');
          }

          NavigationMap.reset();
        }
      };
      reqVolte.onerror = () => {
        console.error('ERROR: Can not get the volte setting.');
      };
    }


    //[task-5277231/5277233] modify by shangpeng.su 2017.09.12 begin
    function _checkIfNeedInternetSharingMenu() {
      var isWfHspotNeed = true;
      var isUsbTetheringNeed = true;

      navigator.customization.getValue(
        'wifi.hotspot.support').then((result) => {
        console.log('wifi hotspot support =' + JSON.stringify(result));
        isWfHspotNeed = JSON.stringify(result) == '1' ? false : true;
      });
      navigator.customization.getValue(
        'usb.tethering.support').then((result) => {
        console.log('usb tethering support =' + JSON.stringify(result));
        isUsbTetheringNeed = JSON.stringify(result) == '1' ? false : true;
      }).then(() => {
        var display = (!isWfHspotNeed && !isUsbTetheringNeed) ? false : true;
        var internetSharing = document.getElementById('internet-sharing');
        if (display) {
            internetSharing.removeAttribute('hidden');
        } else {
           internetSharing.setAttribute('hidden', true);
        }
        window.dispatchEvent(new CustomEvent('refresh'));
      });
    }
    //[task-5277231/5277233] modify end

    function _getImsHandlerCapability() {
      var imsCapability = null;
      var _imsHandler = null;
      for (var i = 0; i < _mobileConnections.length; i++) {
        _imsHandler = _mobileConnections[i].imsHandler;
        if (_imsHandler) {
          imsCapability = _imsHandler.capability || imsCapability;
        }
      }
      return imsCapability;
    }

    function _updateCallSettingsMenu() {
      var _imsHandler = null;
      for (var i = 0; i < _mobileConnections.length; i++) {
        _imsHandler = _mobileConnections[i].imsHandler;
        if (_imsHandler) {
          _imsHandler.addEventListener('capabilitychange',
            _onCapabilityChange);
        }
      }
    }

    function _updateCallSettings(imsCapability) {
      if (imsCapability === 'voice-over-wifi' ||
        imsCapability === 'video-over-wifi') {
        var item = document.getElementById('call-settings');
        var desc = document.getElementById('call-desc');
        var hrefItem = item.querySelector('a');

        item.removeAttribute('aria-disabled');
        desc.removeAttribute('data-l10n-id');
        desc.textContent = '';
      } else {
        AirplaneModeHelper.ready(function() {
          var status = AirplaneModeHelper.getStatus();
          if (status === 'enabled') {
            var item = document.getElementById('call-settings');
            var hrefItem = item.querySelector('a');
            hrefItem.removeAttribute('href');
            item.setAttribute('aria-disabled', true);
          }
        });
      }
    }

    function _onCapabilityChange() {
      var imsCapability = _getImsHandlerCapability();
      if (imsCapability === 'voice-over-wifi' ||
        imsCapability === 'video-over-wifi') {
        if (imsCapability === _currentCapability) {
          return;
        } else {
          _currentCapability = imsCapability;
        }
        var item = document.getElementById('call-settings');
        var desc = document.getElementById('call-desc');
        var hrefItem = item.querySelector('a');

        item.removeAttribute('aria-disabled');
        desc.removeAttribute('data-l10n-id');
        desc.textContent = '';
      } else {
        _currentCapability = imsCapability;
        AirplaneModeHelper.ready(function() {
          var status = AirplaneModeHelper.getStatus();
          if (status === 'enabled') {
            var item = document.getElementById('call-settings');
            var hrefItem = item.querySelector('a');
            hrefItem.removeAttribute('href');
            item.setAttribute('aria-disabled', true);
          }
        });
      }
    }

    return SettingsPanel({
      onInit: function rp_onInit(panel) {
        root = Root();
        root.init();

        nfcItem = NFCItem(panel.querySelector('#menuItem-nfc'));

        airplaneModeItem =
          AirplaneModeItem(panel.querySelector('#menuItem-airplaneMode'));

        stkItem = STKItem({
          iccMainHeader: panel.querySelector('#icc-mainheader'),
          iccEntries: panel.querySelector('#icc-entries')
        });

        // The decision of navigation panel will be removed while we are no
        // longer to use Bluetooth API v1.
        var bluetoothListItem = panel.querySelector('.menuItem-bluetooth');
        var BTAPIVersion = BTAPIVersionDetector.getVersion();
        bluetoothListItem.addEventListener('click', function() {
          SettingsService.navigate('bluetooth');
        });

        // Task5108059-chengyanzhang@t2mobile.com-for disable cb in settings-begin

	var simmcc;
	var connections = window.navigator.mozMobileConnections || [navigator.mozMobileConnection];
        navigator.customization.getValue('def.enable.cellbroadcast').then((isEnableCellBroadcast) => {
          console.log('isEnableCellBroadcast===>' +isEnableCellBroadcast);
	//add mcc judge for show 4G Calling in UK
	for (var i = 0; i < connections.length; ++i) {
				var conn = connections[i];
				if (conn && conn.voice && conn.voice.network && conn.voice.connected) {
					// we have connection available, so we use it
					simmcc = conn.voice.network.mcc;
					
					if(simmcc=="234"||simmcc=="235"){			
						document.getElementById('4gcalling').innerHTML="4G Calling";
						document.getElementById('next_4gcalling').innerHTML="4G Calling";
					}
				}
			}
          if (isEnableCellBroadcast!== undefined && !isEnableCellBroadcast) {
            var emergencyAlert = panel.querySelector('#emergency-alert-menu');
            if (emergencyAlert != undefined && emergencyAlert != null) {
              // bug1825-chengyan.zhang@tcl.com-begin
              // emergencyAlert.setAttribute('hidden', true);
              emergencyAlert.classList.add('hidden');
              emergencyAlert.classList.remove('navigable')
              // bug1825-chengyan.zhang@tcl.com-end
            }
          }
       });
       // Task5108059-chengyanzhang@t2mobile.com-for disable cb in settings-end

        var idleObserver = {
          time: 3,
          onidle: function() {
            navigator.removeIdleObserver(idleObserver);
            lowPriorityRoots = queryRootForLowPriorityItems(panel);
            initLowPriorityItems(lowPriorityRoots).then(function(items) {
              Object.keys(items).forEach((key) => items[key].enabled = true);
            });
          }
        };
        navigator.addIdleObserver(idleObserver);
        this.softkeyParams = {
          menuClassName: 'menu-button',
          header: { l10nId:'message' },
          items: [{
            name: 'Select',
            l10nId: 'select',
            priority: 2,
            method: function() {}
          }]
        };

        window.addEventListener('keydown', HWKhandler);

        //[task-5277231/5277233] modify by shangpeng.su 2017.09.12 begin
        _checkIfNeedInternetSharingMenu();
        //_updateHotspotMenuItem(panel);
        //[task-5277231/5277233] modify end

        _updateAccessibilityMenu();

        _updateVolteDisplay(panel);
        _updateCallSettingsMenu();
      },
      onShow: function rp_onShow(panel) {
        LazyLoader.load(['shared/js/airplane_mode_helper.js'], () => {
          // This event listener will be removed
          //   after the click event callback is processing on.
          _getDSDSMenuConfig().then(() => {
            _addShowMenuEvent();
          });
        });

        if (initLowPriorityItemsPromise) {
          initLowPriorityItemsPromise.then(function(items) {
            Object.keys(items).forEach((key) => {
              items[key].enabled = true;
            });
          });
        }

        var focusedLi =
          document.querySelector('#root div.content:not(.hidden) li.focus');
        if (focusedLi !== null) {
          focusedLi.focus();
        }
        SettingsSoftkey.init(this.softkeyParams);
        SettingsSoftkey.show();
      },
      onHide: function rp_onHide() {
        if (initLowPriorityItemsPromise) {
          initLowPriorityItemsPromise.then(function(items) {
            Object.keys(items).forEach((key) => items[key].enabled = false);
          });
        }
        SettingsSoftkey.hide();
      },
      onBeforeShow: function rp_onBeforeShow() {
        _currentCapability = _getImsHandlerCapability();
        _updateCallSettings(_currentCapability);
      }
    });
  };
});
