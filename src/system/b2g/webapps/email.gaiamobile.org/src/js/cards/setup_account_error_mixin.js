define([ "require", "./setup_l10n_map", "l10n!" ], function(e) {
    var t = e("./setup_l10n_map"), n = e("l10n!");
    return {
        showError: function(e, o) {
            this.errorRegionNode.classList.remove("collapsed");
            var r = t.hasOwnProperty(e) ? t[e] : t.unknown;
            n.setAttributes(this.errorMessageNode, r, o);
            var i = document.querySelector(".sup-error-region");
            i.classList.add("collapsed"), setTimeout(function() {
                Toaster.showToast({
                    message: this.errorMessageNode.textContent,
                    latency: 3e3
                });
            }.bind(this), 0), this.scrollBelowNode.scrollTop = 0;
        }
    };
});