define([ "require", "app_self", "cards", "./base", "template!./setup_oauth2.html" ], function(e) {
    var t = e("app_self"), n = e("cards");
    return [ e("./base")(e("template!./setup_oauth2.html")), {
        onArgs: function(e) {
            this.onBrowserComplete = e.onBrowserComplete;
            var t = document.createElement("iframe");
            t.classList.add("sup-oauth2-browser"), t.setAttribute("mozbrowser", !0), t.setAttribute("src", e.url), 
            t.addEventListener("mozbrowserlocationchange", this.onLocationChange.bind(this)), 
            this.scrollRegion.appendChild(t);
        },
        die: function() {
            t.latest("self", function(e) {
                console.log("clearing browser data: " + e), e && e.clearBrowserData(), console.log("browser data cleared");
            });
        },
        close: function() {
            n.removeCardAndSuccessors(this, "animate", 1);
        },
        onBack: function(e) {
            e && (e.stopPropagation(), e.preventDefault()), this.onBrowserComplete({
                type: "oauth2Cancel"
            }), this.close();
        },
        onLocationChange: function(e) {
            var t = e.detail;
            if (0 !== t.indexOf("app:") || -1 === t.indexOf("cards/oauth2/redirect.html?")) return this.headerLabel.textContent = t, 
            void 0;
            this.close();
            var n = t.split("?")[1] || "", o = {
                type: "oauth2Complete",
                data: {}
            }, r = n.split("&");
            r.forEach(function(e) {
                var t = e.split("=");
                o.data[decodeURIComponent(t[0])] = decodeURIComponent(t[1]);
            }), this.onBrowserComplete(o);
        }
    } ];
});