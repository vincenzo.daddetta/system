define([ "require", "cards", "./base", "template!./cmp-attachments.html" ], function(e) {
    var t, n, o = e("cards"), r = -1, i = !1, s = !1;
    return [ e("./base")(e("template!./cmp-attachments.html")), {
        onArgs: function(o) {
            this.composer = o.composer, t = this, e([ "shared/js/mime_mapper" ], function(e) {
                n || (n = e);
            });
        },
        onBack: function() {
            o.removeCardAndSuccessors(this, "animate");
        },
        die: function() {
            window.removeEventListener("keydown", this.handleKeydown), window.removeEventListener("cmp-attachments-update", this.upgradeData), 
            window.removeEventListener("email-endkey", this.endkeyHandler);
        },
        onCardVisible: function() {
            window.addEventListener("keydown", this.handleKeydown), window.addEventListener("cmp-attachments-update", this.upgradeData), 
            window.addEventListener("email-endkey", this.endkeyHandler), this._refresh(), NavigationMap.setFocus("first"), 
            this.setSoftKey();
        },
        navSetup: function() {
            var e = this.localName, t = ".cmp-attachment-focusable", n = e + " " + t;
            NavigationMap.navSetup(e, t), NavigationMap.setCurrentControl(n);
        },
        handleKeydown: function(e) {
            switch (e.key) {
              case "Backspace":
                e.preventDefault(), 0 == option.menuVisible && (i ? (i = !1, o._endKeyClicked = !1, 
                option.show(), t.composer.hideDialog()) : t.onBack());
            }
        },
        updateHeader: function() {
            this.attachmentsSize.textContent = this.composer.attachmentsSize.textContent, this.attachmentLabel.textContent = this.composer.attachmentLabel.textContent;
        },
        _refresh: function(e) {
            e && (this.composer = e.composer), this.attachmentsContainer.innerHTML = this.composer.attachmentsContainer.innerHTML, 
            this.checkAttachments(), this.updateHeader(), this.navSetup(), this.makeAttachIndex();
        },
        checkAttachments: function() {
            var e = this.composer.composer.attachments.length;
            e > 0 || this.onBack();
        },
        getAttachmentBlob: function(e, t) {
            try {
                var n = e.pathName, o = navigator.getDeviceStorage("sdcard"), r = o.get(n);
                r.onerror = function() {
                    console.warn("Could not open attachment file: ", n, r.error.name);
                }, r.onsuccess = function() {
                    var e = r.result;
                    t(e);
                };
            } catch (i) {
                console.warn("Exception getting attachment from device storage:", e, "\n", i, "\n", i.stack);
            }
        },
        viewAttachment: function(e) {
            var t = n.guessTypeFromFileProperties(e.name, e.blob.type.toLowerCase()), o = n.ensureFilenameMatchesType(e.name, t), r = new MozActivity({
                name: "open",
                data: {
                    type: t,
                    filename: o,
                    blob: e.blob,
                    url: o
                }
            });
            r.onerror = function() {
                if (console.warn('Problem with "open" activity', r.error.name), "NO_PROVIDER" === r.error.name) {
                    s = !0;
                    var e = {
                        title: {
                            id: "message-attachment-did-not-open-label",
                            args: {}
                        },
                        body: {
                            id: "message-attachment-did-not-open-body",
                            args: {}
                        },
                        desc: {
                            id: "",
                            args: {}
                        },
                        accept: {
                            l10nId: "confirm-dialog-ok",
                            priority: 2,
                            callback: function() {
                                s = !1;
                            }
                        }
                    }, t = new ConfirmDialogHelper(e);
                    t.show(document.getElementById("confirm-dialog-container"));
                }
            }, r.onsuccess = function() {
                console.log('"open" activity allegedly succeeded');
            };
        },
        getBaseName: function(e) {
            if (!e) throw new Error("Filepath is not defined!");
            return e.substring(e.lastIndexOf("/") + 1);
        },
        onViewAttachment: function(e) {
            if (console.log("trying to open", e.blob, "known type:", e.blob.type), e.blob && "File" === e.blob.constructor.name) t.viewAttachment(e); else {
                if (e.blob.type.indexOf("vcard") > 0) return e.data && (e.blob = e.data), t.viewAttachment(e), 
                void 0;
                t.getAttachmentBlob(e, function(n) {
                    try {
                        if (!e.blob) throw new Error("Blob does not exist");
                        e.blob = n, t.viewAttachment(e);
                    } catch (o) {
                        console.warn('Problem creating "open" activity:', o, "\n", o.stack);
                    }
                });
            }
        },
        setSoftKey: function() {
            var e = [];
            e.push({
                name: "Select",
                l10nId: "select",
                priority: 2,
                method: function() {
                    if (!s) {
                        var e = t.composer.composer.attachments[r];
                        t.onViewAttachment(e);
                    }
                }
            }), e.push({
                name: "Remove Attachment",
                l10nId: "remove-attachemnt",
                priority: 5,
                method: function() {
                    var e = NavigationMap.getCurrentItem().getAttribute("data-attachIndex");
                    t.composer.onClickRemoveAttachment(e);
                }
            }), e.push({
                name: "Replace Attachment",
                l10nId: "replace-attachemnt",
                priority: 5,
                method: function() {
                    t.composer.onAttachmentReplace();
                }
            }), e.push({
                name: "Add Attachment",
                l10nId: "add-attachemnt",
                priority: 5,
                method: function() {
                    t.composer.onAttachmentAdd();
                }
            }), NavigationMap.setSoftKeyBar(e);
        },
        recoverFocus: function() {
            if (-1 != r) {
                var e = document.querySelector('[data-attachindex="' + r + '"]');
                e ? NavigationMap.setFocus(r) : NavigationMap.setFocus(r - 1);
            }
        },
        upgradeData: function(e) {
            t._refresh(e.detail), t.recoverFocus();
        },
        onFocusChanged: function(e, t, n) {
            r = n.getAttribute("data-attachindex");
        },
        makeAttachIndex: function() {
            for (var e = ".cmp-attachment-focusable", n = this.attachmentsContainer.querySelectorAll(e), o = 0; o < n.length; o++) n[o].setAttribute("data-attachIndex", o);
            for (var r = t.composer.attachmentsContainer.querySelectorAll(e), o = 0; o < r.length; o++) r[o].setAttribute("data-attachIndex", o);
        },
        endkeyHandler: function(e) {
            var n = e.detail.callback;
            i = !0, t.composer.onBack(n);
        }
    } ];
});