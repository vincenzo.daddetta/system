define([ "require", "l10n!", "cards" ], function(e) {
    var t, n = e("l10n!"), o = e("cards");
    return {
        extraClasses: [ "anim-fade", "anim-overlay" ],
        onArgs: function(e) {
            this.account = e.account, this.whichSide = e.whichSide, this.restoreCard = e.restoreCard;
            var o = this.account.type;
            if ("imap+smtp" === o || "pop3+smtp" === o) {
                var r = null;
                r = "incoming" === this.whichSide ? "imap+smtp" === o ? "settings-account-clarify-imap" : "settings-account-clarify-pop3" : "settings-account-clarify-smtp";
            } else r = "settings-account-clarify";
            n.setAttributes(this.accountNode, r, {
                "account-name": this.account.name
            }), t = this;
        },
        handleKeyDown: function(e) {
            switch (e.key) {
              case "Backspace":
                e.preventDefault(), t.proceed();
                break;

              case "Enter":
                e.preventDefault(), e.stopPropagation();
            }
        },
        onCardVisible: function(e) {
            const n = this.localName, o = ".sup-fix-password-li", r = n + " " + o;
            var i = [ {
                name: "Cancel",
                l10nId: "cancel",
                priority: 1,
                method: function() {
                    t.proceed();
                }
            }, {
                name: "Retry",
                l10nId: "retry",
                priority: 3,
                method: function() {
                    t.onUsePassword();
                }
            } ];
            console.log(this.localName + ".onCardVisible, navDirection=" + e), NavigationMap.navSetup(n, o), 
            NavigationMap.setCurrentControl(r), NavigationMap.setFocus("first"), NavigationMap.setSoftKeyBar(i), 
            document.querySelector(".sup-fix-password-input").focus(), this.addEventListener("keydown", t.handleKeyDown);
        },
        onUsePassword: function() {
            var e = this.fixPasswordNode.value;
            e ? this.account.modifyAccount("incoming" === this.whichSide ? {
                password: e
            } : {
                outgoingPassword: e
            }, this.proceed.bind(this)) : this.proceed();
        },
        proceed: function() {
            this.account.clearProblems(), o.removeCardAndSuccessors(this, "animate", 1, this.restoreCard);
        },
        die: function() {
            this.removeEventListener("keydown", t.handleKeyDown);
        }
    };
});