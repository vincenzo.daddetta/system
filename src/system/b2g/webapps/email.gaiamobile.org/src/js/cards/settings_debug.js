var _secretDebug;

define([ "require", "api", "cards", "html_cache", "./base", "template!./settings_debug.html" ], function(e) {
    var t = e("api"), n = e("cards"), o = e("html_cache");
    return _secretDebug || (_secretDebug = {}), [ e("./base")(e("template!./settings_debug.html")), {
        createdCallback: function() {
            this.loggingSelect.value = t.config.debugLogging || "";
        },
        onClose: function() {
            n.removeCardAndSuccessors(this, "animate", 1);
        },
        handleKeyDown: function(e) {
            switch (e.key) {
              case "Enter":
                var t = document.querySelector(".scrollregion-below-header .focus select");
                t ? t.focus() : e.preventDefault();
                break;

              case "Backspace":
                e.preventDefault(), this.onClose();
            }
        },
        menuOptions: [ {
            name: "Select",
            l10nId: "select",
            priority: 2
        } ],
        onCardVisible: function(e) {
            const t = this.localName, n = t + " " + ".debug", o = t + " " + n;
            "forward" == e ? (NavigationMap.navSetup(t, n), NavigationMap.setCurrentControl(o), 
            NavigationMap.setFocus("first"), NavigationMap.setSoftKeyBar(this.menuOptions)) : "back" == e && (NavigationMap.navSetup(t, n), 
            NavigationMap.setCurrentControl(o), NavigationMap.setFocus("restore"), NavigationMap.setSoftKeyBar(this.menuOptions)), 
            this.addEventListener("keydown", this.handleKeyDown);
        },
        resetApp: function() {
            window.location.reload();
        },
        dumpLog: function() {
            t.debugSupport("dumpLog", "storage");
        },
        onChangeLogging: function() {
            var e = this.loggingSelect.value || !1;
            t.debugSupport("setLogging", e);
        },
        fastSync: function() {
            _secretDebug.fastSync = [ 2e4, 6e4 ];
        },
        resetStartupCache: function() {
            o.reset(), console.log("htmlCache.reset done");
        },
        die: function() {
            this.removeEventListener("keydown", this.handleKeyDown);
        }
    } ];
});