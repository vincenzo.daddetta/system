'use strict';
(function(exports) {
    var HardwareButtonsBaseState;
    var HardwareButtonsHomeState;
    var HardwareButtonsSleepState;
    var HardwareButtonsVolumeState;
    var HardwareButtonsWakeState;
    var HardwareButtonsScreenshotState;
    var HardwareButtonsKeyPadState;
    var HardwareButtonsLaunchState;
    var HardwareButtonsEndcallState;
    var HardwareButtons = function HardwareButtons() {
        this._started = false;
        this._softwareHome = false;
        this._timer = undefined;
        this._duringPoweroff = false;
        this._initStatus = false;
        this._daScreenOff = false;
        this._daScreenOn = false;
        this._haveKeydown = false;
        this._haveBeforeKeydown = false;
        this._emergencyTriggered = false;
        this._emergencyTriggerTimes = 1;
        this._emergencyTriggerEnable = false;
        this._emergencyTriggerTimer = undefined;
    };
    HardwareButtons.STATES = {};
    HardwareButtons.prototype.HOLD_INTERVAL = 1500;
    HardwareButtons.prototype.LONG_HOLD_INTERVAL = 3000;
    HardwareButtons.prototype.REPEAT_DELAY = 700;
    HardwareButtons.prototype.REPEAT_INTERVAL = 100;
    HardwareButtons.prototype.name = 'HardwareButtons';
    HardwareButtons.prototype._bKeypad = false;
    HardwareButtons.prototype.start = function hb_start() {
        if (this._started) {
            throw 'Instance should not be start()\'ed twice.';
        }
        this._started = true;
        this.state = new HardwareButtonsBaseState(this);
        this.browserKeyEventManager = new BrowserKeyEventManager();
        window.addEventListener('mozbrowserbeforekeydown', this);
        window.addEventListener('mozbrowserbeforekeyup', this);
        window.addEventListener('mozbrowserafterkeydown', this);
        window.addEventListener('mozbrowserafterkeyup', this);
        window.addEventListener('keydown', this);
        window.addEventListener('keyup', this);
        window.addEventListener('evt_type_poweroff', this);
        window.addEventListener('da_screenoff', this);
        window.addEventListener('da_screenon', this);
        SettingsListener.observe('software-button.enabled', false, function(value) {
            this._softwareHome = value;
        }.bind(this));
        Service.request('SettingsCore:addObserver', 'keyboard.vibration', (value) => {
            this._vibrationEnabled = value;
        });
        Service.register('handleEvent', this);
        this.observeEmergencyTriggerCustomizationValue();
    };
    HardwareButtons.prototype.observeEmergencyTriggerCustomizationValue = function() {
        var self = this;
        SettingsListener.observe('emergency.thric_trigger.enable', 0, function(value) {
            var result = Number(value);
            console.log('[HardwareButtons] observeEmergencyTriggerCustomizationValue, observed value: ' + result);
            self._emergencyTriggerEnable = result;
        });
    };
    HardwareButtons.prototype.stop = function hb_stop() {
        if (!this._started) {
            throw 'Instance was never start()\'ed but stop() is called.';
        }
        this._started = false;
        if (this.state && this.state.exit) {
            this.state.exit();
        }
        this.state = null;
        window.removeEventListener('softwareButtonEvent', this);
        window.removeEventListener('mozbrowserbeforekeydown', this);
        window.removeEventListener('mozbrowserbeforekeyup', this);
        window.removeEventListener('mozbrowserafterkeydown', this);
        window.removeEventListener('mozbrowserafterkeyup', this);
        window.removeEventListener('keydown', this);
        window.removeEventListener('keyup', this);
        window.removeEventListener('evt_type_poweroff', this);
        window.removeEventListener('da_screenoff', this);
        window.removeEventListener('da_screenon', this);
    };
    HardwareButtons.prototype.publish = function hb_publish(type, detail) {
        window.dispatchEvent(new CustomEvent(type, {
            bubbles: type === 'home',
            detail: detail
        }));
    };
    HardwareButtons.prototype.setState = function hb_setState(s, type) {
        if (this.state && this.state.exit) {
            this.state.exit(type);
        }
        this.state = new HardwareButtons.STATES[s](this);
        if (this.state && this.state.enter) {
            this.state.enter(type);
        }
    };
    HardwareButtons.prototype.triggerEmergencyCall = function hb_triggerEmergencyCall() {
        findCardIndexToTrigger(function(cardIndex) {
            console.log('[HardwareButtons] triggerEmergencyCall, triggerred card index: ' + cardIndex);
            var telephony = navigator.mozTelephony;
            if (telephony) {
                if (cardIndex === -1) {
                    telephony.dialEmergency('112');
                } else {
                    telephony.dialEmergency('112', cardIndex);
                }
            }
        });

        function findCardIndexToTrigger(callback) {
            if (!SIMSlotManager.isMultiSIM()) {
                console.log('[HardwareButtons] triggerEmergencyCall, single SIM card!');
                callback(-1);
            } else {
                if (SIMSlotManager.noSIMCardOnDevice()) {
                    console.log('[HardwareButtons] triggerEmergencyCall, no SIM card!');
                    callback(-1);
                } else {
                    lazyLoadSimSettingsHelperIfNeed(function() {
                        console.log('[HardwareButtons] triggerEmergencyCall, read card index!');
                        SimSettingsHelper.getCardIndexFrom('outgoingCall', function(cardIndex) {
                            callback(cardIndex);
                        });
                    });
                }
            }
        }

        function lazyLoadSimSettingsHelperIfNeed(callback) {
            if (typeof SimSettingsHelper === 'undefined') {
                console.log('[HardwareButtons] triggerEmergencyCall, load helper');
                LazyLoader.load('shared/js/sim_settings_helper.js', () => {
                    console.log('[HardwareButtons] triggerEmergencyCall, load helper success');
                    callback();
                });
            } else {
                console.log('[HardwareButtons] triggerEmergencyCall, no need to load helper');
                callback();
            }
        }
    };
    HardwareButtons.prototype.handleEvent = function hb_handleEvent(evt) {
        console.log('kaioseiffel:hardware_button evt.key:%s, evt.type:%s', evt.key, evt.type);
        if (evt.type === 'da_screenoff') {
            this._daScreenOff = true;
            return;
        }
        if (evt.type === 'da_screenon') {
            this._daScreenOn = true;
            return;
        }
        if (evt.type === 'evt_type_poweroff' || this._duringPoweroff === true) {
            if (this._duringPoweroff === false) {
                this._duringPoweroff = true;
            }
            console.log('kaioseiffel:hardware_button durring poweroff');
            return;
        }
        if (evt.key === 'Power') {
            this._bKeypad = false;
            if (evt.type === 'keydown') {
                this._haveKeydown = true;
                if(ScreenManager.screenEnabled && navigator.SlideStatus > 0) { /* Home button is back from the dead */
                  if(navigator.SlideStatus === 2) { /* Home logic when slider is open to the end */
                    this.publish('home');
                    this.setState('base', 'home-button-release');
                    if (!this._timer) {
                        this._timer = setTimeout(function() {
                          this.publish('holdhome');
                          this.setState('base', 'sleep-button-release');
                        }.bind(this), this.HOLD_INTERVAL);
                    }
                  }
                  else { /* Screenshot logic when slider is slightly closed */
                    this.publish('volumedown+sleep');
                    this.setState('base', 'sleep-button-release');
                  }
                } else {
                  this._initStatus = ScreenManager.screenEnabled;
                  console.log('kaioseiffel:hardware_button Screen _initStatus : ' +
                      this._initStatus + ', _haveBeforeKeydown:' + this._haveBeforeKeydown);
                  if (false === this._haveBeforeKeydown) {
                      this._daScreenOff = false;
                      this._daScreenOn = false;
                  }
                  if (this._emergencyTriggerEnable) {
                      if (!this._emergencyTriggerTimer) {
                          console.log('[HardwareButtons] emergency call timer is not exist, reset times!');
                          this._emergencyTriggerTimes = 1;
                      } else {
                          console.log('[HardwareButtons] emergency call timer is running, reset timer!');
                          clearTimeout(this._emergencyTriggerTimer);
                          this._emergencyTriggerTimer = undefined;
                          this._emergencyTriggerTimes++;
                          console.log('[HardwareButtons] emergency call try to be triggered ' +
                              this._emergencyTriggerTimes + ' times');
                          if (this._emergencyTriggerTimes === 3) {
                              this._emergencyTriggered = true;
                          }
                      }
                      this._emergencyTriggerTimer = setTimeout(function() {
                          console.log('[HardwareButtons] emergency call timer is fired!');
                          clearTimeout(this._emergencyTriggerTimer);
                          this._emergencyTriggerTimer = undefined;
                          this._emergencyTriggered = false;
                      }.bind(this), 500);
                  } else {
                      console.log('[HardwareButtons] emergency call trigger is disabled!');
                  }
                  if (!ScreenManager.screenEnabled) {
                      this.publish('wake');
                      this.setState('wake', 'sleep-button-press');
                  }
                  if (!this._timer) {
                      this._timer = setTimeout(function() {
                          this.publish('holdsleep');
                          navigator.vibrate(50);
                          this.setState('sleep', 'sleep-button-release');
                          this.publish('custompower');
                          console.log('kaioseiffel:hardware_button to long press');
                      }.bind(this), this.LONG_HOLD_INTERVAL);
                  }
                }
            } else if (evt.type === 'keyup') {
                if(ScreenManager.screenEnabled && navigator.SlideStatus > 0) { /* Home button is back from the dead*/
                  this.hardwareButtons.publish('home-button-release');
                } else {
                  if (this._timer) {
                      clearTimeout(this._timer);
                      this._timer = undefined;
                      console.log('kaioseiffel:hardware_button clear long press timeout');
                  }
                  if (this._emergencyTriggered && this._emergencyTriggerTimer) {
                      console.log('[HardwareButtons] emergency call is triggered!');
                      clearTimeout(this._emergencyTriggerTimer);
                      this._emergencyTriggerTimer = undefined;
                      this._emergencyTriggered = false;
                      this.triggerEmergencyCall();
                  }
                  console.log('kaioseiffel:hardware_button status return %s,%s, %s', this._initStatus, ScreenManager.screenEnabled, this._haveKeydown);
                  if (this._initStatus !== ScreenManager.screenEnabled || this._haveKeydown === false) {
                      return;
                  }
                }
                this._haveKeydown = false;
            } else {
                if (evt.type.indexOf('keydown') !== -1) {
                    this.publish('stop-alert');
                    if (evt.type === 'mozbrowserbeforekeydown') {
                        this._daScreenOff = false;
                        this._daScreenOn = false;
                        this._haveBeforeKeydown = true;
                    }
                } else if (evt.type.indexOf('keyup') !== -1) {
                    this._haveBeforeKeydown = false;
                    if (this._timer) {
                        clearTimeout(this._timer);
                        this._timer = undefined;
                        console.log('kaioseiffel:hardware_button before/after type clear long press timeout');
                    }
                }
                return;
            }
        } else {
            this._bKeypad = true;
        }
        if (evt.type === 'keydown' && evt.key !== 'flip' && this._vibrationEnabled) {
            navigator.vibrate(50);
        }
        if (evt.type === 'mozbrowserbeforekeydown' && evt.key !== 'flip' && ScreenManager.lidOpened) {
            Service.request('turnKeypadBacklightOn');
        }
        var buttonEventType = this.browserKeyEventManager.getButtonEventType(evt);
        if (!buttonEventType) {
            if (this.browserKeyEventManager.screenOff()) {
                evt.preventDefault();
                Service.request('turnScreenOn', 'power');
            }
            return;
        }
        this.state.process(buttonEventType);
    };
    HardwareButtonsBaseState = HardwareButtons.STATES.base = function HardwareButtonsBaseState(hb) {
        this.hardwareButtons = hb;
    };
    HardwareButtonsBaseState.prototype.process = function(type) {
        switch (type) {
            case 'home-button-press':
                this.hardwareButtons.setState('home', type);
                return;
            case 'endcall-button-press':
                this.hardwareButtons.setState('endcall', type);
                return;
            case 'sleep-button-press':
                return;
            case 'sleep-button-release':
                console.log('kaioseiffel:hardware_button sleep-button-release');
                if (!navigator.mozPower.screenEnabled) {
                    console.log('kaioseiffel:hardware_button :: ' + this.hardwareButtons._bKeypad);
                    if (this.hardwareButtons._bKeypad) {
                        console.log('kaioseiffel:hardware_button to wake');
                        this.hardwareButtons.publish('wake');
                        this.hardwareButtons.setState('wake', type);
                    }
                } else {
                    if (Service && Service.currentApp && Service.currentApp.origin === 'app://kaios-plus.kaiostech.com') {
                        return;
                    }
                    if (true === this.hardwareButtons._daScreenOff || true === this.hardwareButtons._daScreenOn) {
                        console.log('kaioseiffel:hardware_button to _daScreenOff/_daScreenOn true');
                        return;
                    }
                    this.hardwareButtons.publish('sleep');
                    this.hardwareButtons.setState('sleep', type);
                    console.log('kaioseiffel:hardware_button to sleep');
                }
                return;
            case 'volume-up-button-press':
            case 'volume-down-button-press':
                if (Service.query('DialerAgent.isAlerting')) {
                    this.hardwareButtons.publish('stop-alert');
                } else {
                    this.hardwareButtons.setState('volume', type);
                }
                return;
            case 'hash-button-press':
                this.hardwareButtons.setState('keypad', type);
                if (!Service.query('hasVolumeKey') && Service.query('DialerAgent.isAlerting')) {
                    this.hardwareButtons.publish('stop-alert');
                }
                return;
            case 'flip-button-release':
            case 'flip-button-press':
            case 'browserback-button-press':
            case 'backspace-button-press':
            case 'softleft-button-press':
            case 'softright-button-press':
            case 'enter-button-press':
            case 'arrowup-button-press':
            case 'arrowdown-button-press':
            case '1-button-press':
            case '2-button-press':
            case '3-button-press':
            case 'e-button-press':
            case 'i-button-press':
            case 'o-button-press':
            case 'r-button-press':
            case 'w-button-press':
            case 'a-button-press':
            case 'star-button-press':
                this.hardwareButtons.setState('keypad', type);
                return;
            case 'sms-button-press':
            case 'camera-button-press':
                this.hardwareButtons.setState('launch', type);
                return;
            case 'home-button-release':
            case 'volume-up-button-release':
            case 'volume-down-button-release':
                return;
            case 'led-button-press':
                this.hardwareButtons.publish('led-press');
                return;
            case 'a-button-release':
            case 'star-button-release':
            case 'hash-button-release':
            case 'endcall-button-release':
            case 'softleft-button-release':
            case 'softright-button-release':
            case 'enter-button-release':
            case 'arrowup-button-release':
            case 'arrowdown-button-release':
            case '1-button-release':
            case '2-button-release':
            case '3-button-release':
                return;
        }
        console.error('Unexpected hardware key: ', type);
    };
    HardwareButtonsHomeState = HardwareButtons.STATES.home = function HardwareButtonsHomeState(hb) {
        this.hardwareButtons = hb;
        this.timer = undefined;
    };
    HardwareButtonsHomeState.prototype.enter = function() {
        this.timer = setTimeout(function() {
            this.hardwareButtons.publish('holdhome');
            navigator.vibrate(50);
            this.hardwareButtons.setState('base');
        }.bind(this), this.hardwareButtons.HOLD_INTERVAL);
    };
    HardwareButtonsHomeState.prototype.exit = function() {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = undefined;
        }
    };
    HardwareButtonsHomeState.prototype.process = function(type) {
        switch (type) {
            case 'home-button-release':
                this.hardwareButtons.publish('home');
                navigator.vibrate(50);
                this.hardwareButtons.setState('base', type);
                return;
            case 'volume-up-button-press':
            case 'volume-down-button-press':
            case 'sleep-button-release':
                this.hardwareButtons.publish('wake');
                this.hardwareButtons.setState('wake', type);
                return;
        }
        console.error('Unexpected hardware key: ', type);
        this.hardwareButtons.setState('base', type);
    };
    HardwareButtonsSleepState = HardwareButtons.STATES.sleep = function HardwareButtonsSleepState(hb) {
        this.hardwareButtons = hb;
        this.timer = undefined;
    };
    HardwareButtonsSleepState.prototype.process = function(type) {
        switch (type) {
            case 'a-button-press':
            case 'star-button-press':
                this.hardwareButtons.setState('keypad', type);
                return;
            case 'sleep-button-release':
                return;
            case 'volume-down-button-press':
                this.hardwareButtons.setState('screenshot', type);
                return;
            case 'volume-up-button-press':
                this.hardwareButtons.setState('volume', type);
                this.hardwareButtons.setState('base', type);
                return;
            case 'home-button-press':
                this.hardwareButtons.publish('wake');
                this.hardwareButtons.setState('wake', type);
                return;
            case 'led-button-press':
                this.hardwareButtons.publish('led-press');
                return;
        }
        console.error('Unexpected hardware key: ', type);
        this.hardwareButtons.setState('base', type);
    };
    HardwareButtonsVolumeState = HardwareButtons.STATES.volume = function HardwareButtonsVolumeState(hb) {
        this.hardwareButtons = hb;
        this.timer = undefined;
        this.repeating = false;
    };
    HardwareButtonsVolumeState.prototype.repeat = function() {
        this.repeating = true;
        if (this.direction === 'volume-up-button-press') {
            this.hardwareButtons.publish('volumeup');
        } else {
            this.hardwareButtons.publish('volumedown');
        }
        this.timer = setTimeout(this.repeat.bind(this), this.hardwareButtons.REPEAT_INTERVAL);
    };
    HardwareButtonsVolumeState.prototype.enter = function(type) {
        this.direction = type;
        this.repeating = false;
        this.timer = setTimeout(this.repeat.bind(this), this.hardwareButtons.REPEAT_DELAY);
    };
    HardwareButtonsVolumeState.prototype.exit = function() {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = undefined;
        }
    };
    HardwareButtonsVolumeState.prototype.process = function(type) {
        switch (type) {
            case 'home-button-press':
                this.hardwareButtons.setState('base', type);
                return;
                break;
            case 'camera-button-press':
                if (this.direction === 'volume-down-button-press') {
                    this.hardwareButtons.setState('screenshot', type);
                    return;
                }
                break;
            case 'sleep-button-press':
                if (this.direction === 'volume-down-button-press') {
                    this.hardwareButtons.setState('screenshot', type);
                    return;
                }
                this.hardwareButtons.setState('sleep', type);
                return;
            case 'volume-up-button-release':
                if (this.direction === 'volume-up-button-press') {
                    if (!this.repeating) {
                        this.hardwareButtons.publish('volumeup');
                    }
                    this.hardwareButtons.setState('base', type);
                    return;
                }
                break;
            case 'volume-down-button-release':
                if (this.direction === 'volume-down-button-press') {
                    if (!this.repeating) {
                        this.hardwareButtons.publish('volumedown');
                    }
                    this.hardwareButtons.setState('base', type);
                    return;
                }
                break;
            default:
                return;
        }
        console.error('Unexpected hardware key: ', type);
        this.hardwareButtons.setState('base', type);
    };
    HardwareButtonsWakeState = HardwareButtons.STATES.wake = function HardwareButtonsWakeState(hb) {
        this.hardwareButtons = hb;
        this.timer = undefined;
        this.delegateState = null;
    };
    HardwareButtonsWakeState.prototype.enter = function(type) {
        if (type === 'home-button-press') {
            this.delegateState = new HardwareButtonsHomeState(this.hardwareButtons);
        } else {
            this.delegateState = new HardwareButtonsSleepState(this.hardwareButtons);
        }
        this.timer = setTimeout(function() {
            if (type === 'home-button-press') {
                this.hardwareButtons.publish('holdsleep');
            }
            this.hardwareButtons.setState('base', type);
        }.bind(this), this.hardwareButtons.HOLD_INTERVAL);
    };
    HardwareButtonsWakeState.prototype.exit = function() {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }
    };
    HardwareButtonsWakeState.prototype.process = function(type) {
        switch (type) {
            case 'home-button-release':
            case 'sleep-button-release':
                this.hardwareButtons.setState('base', type);
                return;
            default:
                this.delegateState.process(type);
                return;
        }
    };
    HardwareButtonsScreenshotState = HardwareButtons.STATES.screenshot = function HardwareButtonsScreenshotState(hb) {
        this.hardwareButtons = hb;
        this.timer = undefined;
    };
    HardwareButtonsScreenshotState.prototype.enter = function() {
        this.timer = setTimeout(function() {
            this.hardwareButtons.publish('volumedown+sleep');
            this.hardwareButtons.setState('base');
        }.bind(this), this.hardwareButtons.HOLD_INTERVAL);
    };
    HardwareButtonsScreenshotState.prototype.exit = function() {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = undefined;
        }
    };
    HardwareButtonsScreenshotState.prototype.process = function(type) {
        this.hardwareButtons.setState('base', type);
    };
    HardwareButtonsKeyPadState = HardwareButtons.STATES.keypad = function HardwareButtonsKeyPadState(hb) {
        this.hardwareButtons = hb;
    };
    HardwareButtonsKeyPadState.prototype.enter = function(type) {
        this.direction = type;
        this.hardwareButtons.publish(type);
        if ('a-button-press' === type || 'hash-button-press' === type || 'star-button-press' === type) {
            const eventName = 'hold' + type.split('-')[0];
            this.timer = setTimeout(function() {
                this.hardwareButtons.publish(eventName);
                this.hardwareButtons.setState('base');
            }.bind(this), this.hardwareButtons.HOLD_INTERVAL);
        } else if (type === 'backspace-button-press' && !Service.query('isFlip')) {}
    };
    HardwareButtonsKeyPadState.prototype.process = function(type) {
        var eventType;
        switch (type) {
            case 'a-button-release':
            case 'star-button-release':
            case 'hash-button-release':
                break;
            case 'flip-button-release':
            case 'flip-button-press':
                this.hardwareButtons.publish(type);
                break;
            case 'browserback-button-release':
            case 'backspace-button-release':
            case 'softleft-button-release':
            case 'softright-button-release':
            case 'enter-button-release':
            case 'arrowup-button-release':
            case 'arrowdown-button-release':
            case '1-button-release':
            case '2-button-release':
            case '3-button-release':
            case 'e-button-release':
            case 'i-button-release':
            case 'o-button-release':
            case 'r-button-release':
            case 'w-button-release':
                if (this.direction === 'backspace-button-press' && type === 'backspace-button-release') {
                    this.hardwareButtons.publish('home', {
                        kill: true,
                        back: true
                    });
                }
                if (type === 'softright-button-release') {
                    if (this.direction !== 'softright-button-press') {
                        break;
                    }
                }
                if (type === 'softleft-button-release') {
                    if (this.direction !== 'softleft-button-press') {
                        break;
                    }
                }
                eventType = type.replace('-button-release', '');
                this.hardwareButtons.publish(eventType);
                break;
            default:
                if (!type || type.indexOf('-press') < 0) {
                    console.error('Unexpected hardware key: ', type);
                }
        }
        this.hardwareButtons.setState('base', type);
    };
    HardwareButtonsKeyPadState.prototype.exit = function() {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = undefined;
        }
    };
    HardwareButtonsEndcallState = HardwareButtons.STATES.endcall = function(hb) {
        this.hardwareButtons = hb;
    };
    HardwareButtonsEndcallState.prototype.enter = function(type) {
        this.hardwareButtons.publish(type);
        this.timer = setTimeout(function() {
            this.hardwareButtons.publish('holdsleep');
            navigator.vibrate(50);
            this.hardwareButtons.setState('base');
        }.bind(this), this.hardwareButtons.HOLD_INTERVAL);
    };
    HardwareButtonsEndcallState.prototype.process = function(type) {
        switch (type) {
            case 'endcall-button-release':
                this.hardwareButtons.publish('home', {
                    kill: true
                });
                break;
        }
        this.hardwareButtons.setState('base', type);
    };
    HardwareButtonsEndcallState.prototype.exit = function() {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = undefined;
        }
    };
    HardwareButtonsLaunchState = HardwareButtons.STATES.launch = function HardwareButtonsLaunchState(hb) {
        this.hardwareButtons = hb;
    };
    HardwareButtonsLaunchState.prototype.enter = function(type) {
        this.direction = type;
        this.hardwareButtons.publish(type);
    };
    HardwareButtonsLaunchState.prototype.process = function(type) {
        var name, url;
        switch (type) {
            case 'volume-down-button-press':
                if (this.direction === 'camera-button-press') {
                    this.hardwareButtons.setState('screenshot', type);
                    return;
                }
                break;
            case 'sms-button-release':
            case 'camera-button-release':
                name = type.replace('-button-release', '');
                url = 'app://' + name + '.gaiamobile.org';
                this.hardwareButtons.publish(type);
                break;
            default:
                if (!type || type.indexOf('-press') < 0) {
                    console.error('Unexpected hardware key: ', type);
                }
        }
        if (url && (!window.FtuLauncher || !FtuLauncher.isFtuRunning()) && Service.query('ScreenManager.lidOpened') !== false && (!Service.query('AttentionWindowManager.hasActiveWindow')) && !Service.query('locked')) {
            navigator.mozApps.mgmt.getAll().onsuccess = function(event) {
                var apps = event.target.result;
                apps.some((app) => app.origin === url && app.launch());
            };
        }
        this.hardwareButtons.setState('base', type);
    };
    exports.hardwareButtons = new HardwareButtons();
    exports.hardwareButtons.start();
    exports.HardwareButtons = HardwareButtons;
}(window));
