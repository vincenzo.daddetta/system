/* exported SelectItemUI */

(function(exports) {
  'use strict';

  var skSelect = {
    name: 'Select',
    l10nId: 'select',
    priority: 2,
    method: function() {
    }
  };

  var skCancel = {
    name: 'Cancel',
    l10nId: 'cancel',
    priority: 1,
    method: function() {
      SelectItemUI._back();
    }
  };

  var smsRequestStatusReport =
      'ril.sms.requestStatusReport.enabled';
  var mmsRequestStatusReport =
      'ril.mms.requestStatusReport.enabled';
  var mmsRetrievalMode =
      'ril.mms.retrieval_mode';
  // Task5243959-tingtingchao@t2mobile.com-add
  var smsEncodingMode =
        'ril.sms.encoding_mode';
  var wapPush=
      'wap.push.enabled';
  var wapPushSim1 =
      'wap.push.enabled.sim1';
  var wapPushSim2 =
      'wap.push.enabled.sim2';
  var elements = {},
      settings = window.navigator.mozSettings;
  var params = {
        header: { l10nId:'options' },
        items: [skCancel, skSelect]
      };
  var SelectItemUI = {
    initialized: false,
    _storeFocused: null,
    _currentPanel:'',
    _setItem:'',
    init: function sui_init() {
    },

    _updateSKs:function(params) {
      if (exports.option) {
        exports.option.initSoftKeyPanel(params);
      } else {
        exports.option = new SoftkeyPanel(params);
      }
      exports.option.show();
    },
    /*
      afterEnter checks li items and bind the value from settings has set
     */


    afterEnter: function (args) {
      ['delivery-report-view', 'sms-encoding-view','auto-Retrieve-view',
       'wap-push-view-sim1', 'wap-push-view-sim2'].forEach(function(viewName) {
        if (Navigation.isCurrentPanel(viewName) == true) {
          SelectItemUI._bindValue(viewName);
        }
      });
      console.log('selectItem _updateSKs');
      SelectItemUI._updateSKs(params);
      window.addEventListener('keydown', SelectItemUI._handleKeyDownEvent);
    },

    beforeLeave:function(args) {
      window.removeEventListener('keydown', SelectItemUI._handleKeyDownEvent);
    },

    afterLeave: function sui_afterLeave(args) {
      if (exports.option) {
        exports.option.hide();
      }
    },

    _handleKeyDownEvent: function (event) {
      switch (event.key) {
        case 'BrowserBack':
        case 'Backspace':
          event.preventDefault();
          SelectItemUI._back();
          break;
        case 'Accept':
        case 'Enter':
          SelectItemUI._saveSelected();
          break;
        }
    },

    _back:function() {
      Navigation.toPanel('settings-view');
    },

    _bindValue:function(panelName) {
      switch(panelName) {
        case 'delivery-report-view':
          var deliveryReportItemView =
            document.getElementById('deliveryReportItem');
          if (deliveryReportItemView.innerHTML.length === 0) {
            var template = Template('deliveryReportItemViewTmpl');
            deliveryReportItemView.innerHTML = template.toString();
          }
          SelectItemUI._bindElement('deliveryReportItem', smsRequestStatusReport);
        break;
        case 'auto-Retrieve-view':
          var autoRetrieveMessageItemView =
            document.getElementById('autoRetrieveMessageItem');
          if (autoRetrieveMessageItemView.innerHTML.length === 0) {
            var template = Template('autoRetrieveMessageItemViewTmpl');
            autoRetrieveMessageItemView.innerHTML = template.toString();
          }
          SelectItemUI._bindElement('autoRetrieveMessageItem', mmsRetrievalMode);
        break;
        case 'wap-push-view-sim1':
        case 'wap-push-view-sim2':
          var wapPushMessageItemView =
            document.getElementById('wapPushMessageItem');
          if (wapPushMessageItemView.innerHTML.length === 0) {
            var template = Template('wapPushMessageItemViewTmpl');
            wapPushMessageItemView.innerHTML = template.toString();
          }
          SelectItemUI.updateHeaderText(wapPushMessageItemView, panelName);
          if (panelName === 'wap-push-view-sim1') {
            SelectItemUI._bindElement('wapPushMessageItem', wapPushSim1);
          } else {
            SelectItemUI._bindElement('wapPushMessageItem', wapPushSim2);
          }
        break;
        // Task5243959-tingtingchao@t2mobile.com-begin
        case 'sms-encoding-view':
              var smsEncodingItemView = document.getElementById('smsEncodingMessageItem');
              if (smsEncodingItemView.innerHTML.length === 0) {
                  var template = Template('smsEncodingMessageItemViewTmpl');
                  smsEncodingItemView.innerHTML = template.toString();
                  console.log("smsEncodingItemView.innerHTML=  " + smsEncodingItemView.innerHTML);
              }
              SelectItemUI._bindElement("smsEncodingMessageItem",smsEncodingMode);
        break;
        // Task5243959-tingtingchao@t2mobile.com-end
      }
    },

    updateHeaderText: function (el, panel) {
      var elHeader = el.getElementsByTagName('h1')[0];
      var isDualSim = Settings.isDualSimDevice();
      if (isDualSim) {
        // Bug876-gang-chen@t2mobile.com-FR DSDS sim card name begin
        elHeader.setAttribute('data-l10n-args',
          JSON.stringify({ simname: (panel === 'wap-push-view-sim1') ? (Settings.sim1Name ? Settings.sim1Name : 'SIM 1') : (Settings.sim2Name ? Settings.sim2Name : 'SIM 2') }));
        // Bug876-gang-chen@t2mobile.com-FR DSDS sim card name end
        elHeader.setAttribute('data-l10n-id', (panel === 'wap-push-view-sim1') ?
          'wappushSIM1-ds' : 'wappushSIM2-ds');
      } else {
        elHeader.setAttribute('data-l10n-id', 'wappushNormal-title');
      }
    },

    _bindElement:function(el, name) {
      SelectItemUI._setItem=name;
      SelectItemUI._currentPanel=el;
      console.log("_bindElement","name:"+name+"el:"+el);
      NavigationMap.reset(el);
      var request = settings.createLock().get(name);
      request.onsuccess = function onSuccessCb() {
        var value =  request.result[name];
        var radioItems = document.querySelectorAll('#'+el+' Input');
        if (typeof(value) === 'undefined') {
          radioItems[0].checked = true;
          NavigationMap.setFocus(0);
        } else {
          if (radioItems.length > 0) {
            for (var i=0; i < radioItems.length; i++) {
              if (radioItems[i].value === value ||
                  radioItems[i].value === value.toString()) {
                radioItems[i].checked = true;
                NavigationMap.setFocus(i);
              } else {
                radioItems[i].checked = false;
              }
            }
          }
        }
      }
    },

    _saveSelected:function(evt) {
      var liItems =
        document.querySelectorAll('#'+SelectItemUI._currentPanel+' LI');
      for (var i=0; i < liItems.length; i++) {
          var radioBtn = liItems[i].querySelector("input");
        if (radioBtn&&radioBtn.checked == true) {
          var radio_value = null;
          switch(radioBtn.value) {
            case 'true':
              radio_value = true;
              break;
            case 'false':
              radio_value = false;
              break;
            default:
              radio_value = radioBtn.value;
              break;
          }
          SelectItemUI._save(SelectItemUI._setItem, radio_value);
          SelectItemUI._back();
        }
      }
    },

    _save:function(targetName, targetValue) {
      var set = {};
      set[targetName] = targetValue;
      settings.createLock().set(set);
    },

    _getParents: function(node, tagName) {
      console.log('find parent');
      var parent = node.parentNode;
      var tag = tagName.toUpperCase();
      if (parent.tagName == tag) {
        return parent;
      } else {
        return this._getParents(parent, tag);
      }
    }
  };

  exports.SelectItemUI = SelectItemUI;
}(this));
