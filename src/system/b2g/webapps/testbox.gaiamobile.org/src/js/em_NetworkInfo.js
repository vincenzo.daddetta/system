/*© 2017 KAI OS TECHNOLOGIES (HONG KONG) LIMITED, all rights reserved.*/
// ************************************************************************
// * File Name: em_NetworkInfo.js
// * Description: engmode -> Network Info.
// * Note:
// ************************************************************************

'use strict';

var isfirst = true;
var GSM    = 1,CDMA   = 2,LTE    = 3,WCDMA  = 4,TD_SCDMA  = 5;
var gNetworkInfo = {};
var MAX_CELL_LIST_NUM = 8;

var NONE = 0, CELL = 1, NCELL = 2, BAND = 3;
var current_info_list = [];
var current_info_type = NONE;

var NetworkInfo = {
  init: function NetworkInfo() {
    var _self = this;
    if (true === isfirst) {
      _self.addListeners();
      isfirst = false;
      var _mobileConnection = window.navigator.mozMobileConnections[0];
      if (!_mobileConnection) {
        dump('NetworkInfo: mozMobileConnection is null.');
      }

      var req = _mobileConnection.getEngModeData();

      req.onsuccess = function onsuccess() {
        var packet = req.result;
        dump('NetworkInfo getEngModeData ---> packet length:' + packet.length + ' data length:' + packet[0]);
        dump('NetworkInfo getEngModeData ---> data: ' + packet);
        /*packet = [66,1,6
                   ,4,1,460,0,37333,50732,31,99,1
                   ,4,1,461,0,37333,50731,31,99,2
                   ,4,0,462,0,37333,41291,7,99,3
                   ,4,0,463,0,37333,41281,9,99,4
                   ,4,0,464,0,37333,8682,8,99,5
                   ,4,0,465,0,37333,17004,9,99,6
                   ,2,4,44,65,1,85,65
                   ,1,2, 88]; */
        gNetworkInfo = _self.handleData(packet);
        _self.initUI();
      };

      req.onerror = function onerror() {
        dump('NetworkInfo: failed to get engmode data.');
      }
    }
  },

  initUI: function _initI() {
    // UARFCN
    if ( gNetworkInfo.uarfcn) {
      $('_UARFCN').hidden = false;
    }
  },

  addListeners: function _addListeners(){
    var _self = this;
    $('menuItem-cellInfomation').addEventListener('click', function() {
        current_info_type = CELL;
        current_info_list = gNetworkInfo.cell_info_list;
        _self.updateCellInfo();
        $('cellInfos_list_head').innerHTML = "Cell Info List";
    });

    $('menuItem-neighbourCellInfomation').addEventListener('click', function() {
        current_info_type = NCELL;
        current_info_list = gNetworkInfo.neighbourCell_info_list;
        _self.updateCellInfo();
        $('cellInfos_list_head').innerHTML = "Neighbour Info List";
    });

    $('menuItem-bandInfomation').addEventListener('click', function() {
        current_info_type = BAND;
        current_info_list = gNetworkInfo.band_info_list;
        _self.updateCellInfo();
        $('cellInfos_list_head').innerHTML = "Band Info List";
    });

    $('menuItem-attachInfomation').addEventListener('click', function() {
        $('_AttachInfoDiv').innerHTML = _self.showAttachInfo(gNetworkInfo.cs_attach_state
          , gNetworkInfo.ps_attach_state);
        $('_AttachInfoHead').innerHTML = "Attach Info";
    });

    $('menuItem-rrcState').addEventListener('click', function() {
        $('_AttachInfoDiv').innerHTML = _self.showRRCState(gNetworkInfo.nas_rrc_state);
        $('_AttachInfoHead').innerHTML = "RRC State";
    });

    $('menuItem-IMIS').addEventListener('click', function() {
      $('_AttachInfoHead').innerHTML = 'IMSI';
      _self.shwoImsiInfo();
    });

    $('menuItem-PDP').addEventListener('click', function() {
      $('_AttachInfoHead').innerHTML = "PDP&Data Icon";
      _self.updatePDPInfo();
    });

    $('menuItem-UARFCN').addEventListener('click', function() {
        $('_AttachInfoDiv').innerHTML = _self.getUARFCN();
        $('_AttachInfoHead').innerHTML = "UARFCN";
    });

    $('_cellinfoBack').addEventListener('click', function() {
        $('_cellInfoDiv').innerHTML = "";
    });
  },

  updatePDPInfo: function _updatepdp() {
    var ipAddress = [];
    var updateContent = $('_AttachInfoDiv');
    updateContent.innerHTML = "Can not get PDP info!!";
    var req = window.navigator.mozSettings.createLock().get("textbox.pdpinfo");
    req.onsuccess = function() {
      var pdpinfo = req.result['textbox.pdpinfo'];
      if (pdpinfo != undefined && pdpinfo != '') {
         updateContent.innerHTML = pdpinfo;
      }
    }
  },

  getUARFCN: function _getUARFCN(){
    if (gNetworkInfo.uarfcn){
      return gNetworkInfo.uarfcn;
    }
    return "";
  },

  shwoImsiInfo: function _shwoImsiInfo() {
    var imsis = [];
    var validCardCount = 0;
    var container = $('_AttachInfoDiv');
    var conns = navigator.mozMobileConnections;
    container.textContent = 'mozMobileConnections is not available.';
    if (conns) {
      var iccManager = navigator.mozIccManager;
      [].forEach.call(conns, (simcard, cardIndex) => {
        var iccId = simcard.iccId;
        dump('getEngModeData ---> SIM[' + cardIndex + ']' + ' iccId:' + iccId);
        var icc = iccManager.getIccById(iccId);
        if (icc !== null) {
          validCardCount++;
          imsis.push(icc.iccInfo.imsi);
        }
      });

      if (!imsis || imsis.length === 0) {
        container.textContent = 'IMSI is not available.';
      } else {
        imsis.forEach((imsi, index) => {
          var span = document.createElement('span');
          if (imsis.length > 1) {
            navigator.mozL10n.setAttributes(span,
              'imsis-with-index', {
                index: index + 1,
                imsi: imsi
              });
          } else {
            span.textContent = imei;
          }
          container.appendChild(span);
        });
      }
    }
  },

  updateCellInfo: function _updateCellInfo() {
  //  $('cellInfos-container').innerHTML = "";
    var _self = this;
    _self.clearInfoShow();
   if(current_info_list.length > 0) {
       for (var key in current_info_list) {
   /*     var a = document.createElement('a');
        a.id = 'a' + key;
        a.innerHTML = "Cell " + key;
        a.class = "menu-item";
        var li = document.createElement('li');
        li.appendChild(a);
        $('cellInfos-container').appendChild(li);
        $(a.id).tabIndex = 1;
        $(a.id).jrdFocus = true;
        $(a.id).href = '#root';  */   /// ??????
        if (key > MAX_CELL_LIST_NUM - 1) break;
        var id = "cell-a-" + key;
        if (current_info_type == BAND) {
          $(id).innerHTML = "Band " + key;
        } else if (current_info_type == NCELL){
          $(id).innerHTML = "Neighbour Cell " + key;
        } else {
           $(id).innerHTML = "Cell " + key;
        }
        $(id).parentNode.hidden = false;
        $(id).addEventListener('click', _self.clickHandler);
      }
    }
  },

  clickHandler: function _clickHandler(e) {
    var k = e.target.id.substr(e.target.id.length - 1, 1);
    if (current_info_type == BAND) {
      $('_cellInfoDiv').innerHTML =  NetworkInfo.showBandInfo(current_info_list[k]);
      $('_cellinfoHead').innerHTML = "Band " + k;
    } else {
      $('_cellInfoDiv').innerHTML = NetworkInfo.showCellInfo(current_info_list[k]);
      $('_cellinfoHead').innerHTML = "Cell " + k;
    }
  },

  clearInfoShow: function _clearInfoShow() {
    var _self = this;
    for (var i = 0; i < MAX_CELL_LIST_NUM ; i ++) {
      var id = "cell-a-" + i;
      $(id).parentNode.hidden = true;
      $(id).removeEventListener('click', _self.clickHandler);
    }
  },

  showRRCState: function _showRRCState(state) {
    var _self = this;
    var msg =  "=============================<br>"
          + "  RRC State : " + _self.getRRCStateString(state) + "  (" + state + ")" + "<br>"
    return msg;
  },

  getRRCStateString: function _getRRCStateString(state) {
    var str = "undefine!";
    switch (state) {
      case 0:
        str = "NAS_RRC_IDLE";
        break;
      case 1:
        str = "NAS_RRC_CONNECTED";
        break;
    }
    return str;
  },

  showAttachInfo: function _showAttachInfo(cs_state, ps_state) {
    var _self = this;
    var msg =  "=============================<br>"
          + "  cs_attach_state : " + _self.getCS_stateString(cs_state) + "  (0x0" + cs_state + ")" + "<br>"
          + "  ps_attach_state : " + _self.getPS_stateString(ps_state)
          + "  (0x0" + ps_state + ")"  + "<br>";
    return msg;
  },

  getPS_stateString: function _getPS_stateString(ps) {
     var str = "undefine!";
      switch (ps) {
        case 0x01:
          str = "PS_ATTACHED";
          break;
        case 0x02:
          str = "PS_DETACHED";
          break;
        case 0x00:
          str = "PS_UNKNOWN";
          break;
      }
      return str;
  },
  getCS_stateString: function _getCS_stateString(cs) {
     var str = "undefine!";
      switch (cs) {
        case 0x01:
          str = "CS_ATTACHED";
          break;
        case 0x02:
          str = "CS_DETACHED";
          break;
        case 0x00:
          str = "CS_UNKNOWN";
          break;
      }
      return str;
  },

  showBandInfo: function _showBandInfo(bandinfo){
    var _self = this;
    var msg = "error!";
    if (!bandinfo)  return msg;
    msg =  "=============================<br>"
          + "  rat : " + _self.getRATString(bandinfo.rat) + "  (0x0" + bandinfo.rat + ")" + "<br>"
          + "  active_band : " + _self.getActiveBandString(bandinfo.active_band) + "  (0x0" + bandinfo.active_band + ")"  + "<br>"
          + "  active_channel : " + bandinfo.active_channel + "<br>";
    return msg;
  },

  getActiveBandString: function _getActiveBandString(active_band) {
      var str = "undefine!";
      if (active_band >= 0 && active_band < 40) {
        str = "CDMA band classes";
      } else if ( active_band >= 40 && active_band < 80) {
        str = "GSM band classes";
      } else if ( active_band >= 80 && active_band < 92) {
        str = "WCDMA band classes";
      } else if ( active_band >= 120 && active_band < 162) {
        str = "LTE band classes";
      } else if ( active_band >= 200 && active_band < 206) {
        str = "TD-SCDMA band classes";
      }
      return str;
  },

  getRATString:function _getRAT(rat){
    var str = "undefine!";
      switch (rat) {
        case 0x01:
          str = "cdma2000\\textsuperscript{\\textregistered} 1X";
          break;
        case 0x02:
          str = "cdma2000\\textsuperscript{\\textregistered} HRPD (1xEV-DO)";
          break;
        case 0x03:
          str = "AMPS";
          break;
        case 0x04:
          str = "GSM";
          break;
        case 0x05:
          str = "UMTS";
          break;
        case 0x08:
          str = "LTE";
          break;
        case 0x09:
          str = "TD-SCDMA";
          break;
      }
      return str;
  },

  showCellInfo: function _showCellInfo(cell_info) {
    var _self = this;
    var msg = "error!";
    if (!cell_info) return msg;
    switch(cell_info.cellInfoType) {
      case GSM:
        msg = "=============================<br>"
              + " RAT(cellInfoType) : " + _self.getTypeString(cell_info.cellInfoType) + "<br>"
              + " PLMN : " + "mcc " + cell_info.mcc + "  mnc " + cell_info.mnc + "<br>"
              + " RSSI(signalStrength) : " + cell_info.signalStrength + "<br><br>"

              + "-----------------more-----------------<br>"
              + "lac : " + cell_info.lac + "<br>"
              + "cid : " + cell_info.cid + "<br>"
              + "bitErrorRate : " + cell_info.bitErrorRate + "<br>";
        break;
      case CDMA:
        msg = "=============================<br>"
              + " RAT(cellInfoType) : " + _self.getTypeString(cell_info.cellInfoType) + "<br>"
              + " RSSI [ <br>"
              + "    Cdma: dbm " + cell_info.signalStrengthCdma.dbm + "  ecio " + cell_info.signalStrengthCdma.ecio + "<br>"
              + "    Evdo: dbm " + cell_info.signalStrengthEvdo.dbm + "  ecio " + cell_info.signalStrengthEvdo.ecio
              + "   signalNoiseRatio " + cell_info.signalStrengthEvdo.signalNoiseRatio + "]<br><br>"

              + "-----------------more-----------------<br>"
              + "networkId : " + cell_info.networkId + "<br>"
              + "systemId : " + cell_info.systemId + "<br>"
              + "basestationId : " + cell_info.basestationId + "<br>"
              + "longitude : " + cell_info.longitude + "<br>"
              + "latitude : " + cell_info.latitude;
        break;
        case WCDMA:
          msg = "=============================<br>"
              + " RAT(cellInfoType) : " + _self.getTypeString(cell_info.cellInfoType) + "<br>"
              + " PLMN : " + "mcc " + cell_info.mcc + "  mnc " + cell_info.mnc + "<br>"
              + " Primary Scrambling Code : " + cell_info.psc + "<br>"
              + " RSSI(signalStrength) : " + cell_info.signalStrength + "<br><br>"

              + "-----------------more-----------------<br>"
              + "lac : " + cell_info.lac + "<br>"
              + "cid : " + cell_info.cid + "<br>"
              + "bitErrorRate : " + cell_info.bitErrorRate + "<br>";
          break;
        case LTE:
            msg = "=============================<br>"
              + " RAT(cellInfoType) : " + _self.getTypeString(cell_info.cellInfoType) + "<br>"
              + " PLMN : " + "mcc " + cell_info.mcc + "  mnc " + cell_info.mnc + "<br>"
              + " CQI : " +  cell_info.cqi + "<br>"
              + " CPICH Power & Quality : " + cell_info.rsrp + "<br>"
              + " RSSI(signalStrength) : " + cell_info.signalStrength + "<br><br>"

              + "-----------------more-----------------<br>"
              + "ci : " + cell_info.ci + "<br>"
              + "pci : " + cell_info.pci + "<br>"
              + "tac : " + cell_info.tac + "<br>";
              + "rsrq : " + cell_info.rsrq + "<br>"
              + "rssnr : " + cell_info.rssnr + "<br>"
              + "timingAdvance : " + cell_info.timingAdvance + "<br>";
          break;
        case TD_SCDMA:
          msg = "Contact the engineer!!"
          break;
        default:
          break;
    }
    return msg;
  },

  getTypeString:function _getTtypString(type){
    var str = "";
    switch (type) {
      case GSM:
        str = "GSM";
        break;
      case CDMA:
        str = "CDMA";
        break;
      case WCDMA:
        str = "WCDMA";
        break;
      case LTE:
        str = "LTE";
        break;
      case TD_SCDMA:
        str = "TD_SCDMA";
        break;
      default:
        break;
    }
    return str;
  },

  handleData: function _handleData(array) {
    var obj = {};
    var dataPointer = 0;
    if (!array || !array[0]) {
      dump("handleData data error");
      return obj;
    }

    obj.length = array[dataPointer++];
    if (obj.length != array.length - 1) {
      dump("handleData data length error");
      return obj;
    }
    /**
      NAS RRC state. Values: \n
      - NAS_RRC_IDLE (0) --  Status: Idle \n
      - NAS_RRC_CONNECTED (1) --  Status: Connected
     */
    obj.nas_rrc_state = array[dataPointer++];

    //cell number
    obj.cell_number = array[dataPointer++];

    // cell info
    obj.cell_info_list = [];
    obj.neighbourCell_info_list = [];
    for (var i = obj.cell_number - 1; i >= 0; i--) {
      var cell_info = {};
      cell_info.cellInfoType = array[dataPointer++];
      cell_info.registered = array[dataPointer++];
      dump("cellInfoType:" + cell_info.cellInfoType +
        "  registered:" + cell_info.registered +
        "  dataPointer:" + dataPointer);
      switch (cell_info.cellInfoType) {
        case GSM:
          cell_info.mcc = array[dataPointer++];
          cell_info.mnc = array[dataPointer++];
          cell_info.lac = array[dataPointer++];
          cell_info.cid = array[dataPointer++];
          cell_info.signalStrength = array[dataPointer++];
          cell_info.bitErrorRate = array[dataPointer++];
          break;

        case CDMA:
          cell_info.networkId = array[dataPointer++];
          cell_info.systemId = array[dataPointer++];
          cell_info.basestationId = array[dataPointer++];
          cell_info.longitude = array[dataPointer++];
          cell_info.latitude = array[dataPointer++];
          cell_info.signalStrengthCdma = {};
          cell_info.signalStrengthCdma.dbm = array[dataPointer++];
          cell_info.signalStrengthCdma.ecio = array[dataPointer++];
          cell_info.signalStrengthEvdo = {};
          cell_info.signalStrengthEvdo.dbm = array[dataPointer++];
          cell_info.signalStrengthEvdo.ecio = array[dataPointer++];
          cell_info.signalStrengthEvdo.signalNoiseRatio = array[dataPointer++];
          break;
        case WCDMA:
          cell_info.mcc = array[dataPointer++];
          cell_info.mnc = array[dataPointer++];
          cell_info.lac = array[dataPointer++];
          cell_info.cid = array[dataPointer++];
          cell_info.psc = array[dataPointer++];
          cell_info.signalStrength = array[dataPointer++];
          cell_info.bitErrorRate = array[dataPointer++];
          break;
        case LTE:
          cell_info.mcc = array[dataPointer++];
          cell_info.mnc = array[dataPointer++];
          cell_info.ci = array[dataPointer++];
          cell_info.pci = array[dataPointer++];
          cell_info.tac = array[dataPointer++];
          cell_info.signalStrength = array[dataPointer++];
          cell_info.rsrp = array[dataPointer++];
          cell_info.rsrq = array[dataPointer++];
          cell_info.rssnr = array[dataPointer++];
          cell_info.cqi = array[dataPointer++];
          cell_info.timingAdvance = array[dataPointer++];
          break;
        case TD_SCDMA:
          cell_info.mcc = array[dataPointer++];
          cell_info.mnc = array[dataPointer++];
          cell_info.lac = array[dataPointer++];
          cell_info.cid = array[dataPointer++];
          cell_info.cpid = array[dataPointer++];
          cell_info.rscp = array[dataPointer++];
          break;
        default:
          break;
      }

      if (cell_info.registered == 0) { // neighbourCell_info
        obj.neighbourCell_info_list.push(cell_info);
      } else {
        obj.cell_info_list.push(cell_info);
      }
    }

    //band number
    obj.band_number = array[dataPointer++];
    obj.band_info_list = [];
    for (var j = obj.band_number - 1; j >= 0; j--) {
      var bandinfo = {};
      /**
        Radio interface currently in use. Values:  \n
        - 0x01 -- cdma2000\textsuperscript{\textregistered} 1X             \n
        - 0x02 -- cdma2000\textsuperscript{\textregistered} HRPD (1xEV-DO) \n
        - 0x03 -- AMPS \n
        - 0x04 -- GSM \n
        - 0x05 -- UMTS \n
        - 0x08 -- LTE \n
        - 0x09 -- TD-SCDMA
        */
      bandinfo.rat = array[dataPointer++];
      /**
        Active band class (see Table @latexonly\ref{tbl:bandClass}@endlatexonly
        for details). Values: \n
        - 00 to 39   -- CDMA band classes  \n
        - 40 to 79   -- GSM band classes   \n
        - 80 to 91   -- WCDMA band classes \n
        - 120 to 161 -- LTE band classes   \n
        - 200 to 205 -- TD-SCDMA band classes
      */
      bandinfo.active_band = array[dataPointer++];
      bandinfo.active_channel = array[dataPointer++];
      obj.band_info_list.push(bandinfo);
    }

    /**<   Circuit-switched domain attach state of the mobile. Values: \n
       - 0x00 -- CS_UNKNOWN  -- Unknown or not applicable \n
       - 0x01 -- CS_ATTACHED -- Attached \n
       - 0x02 -- CS_DETACHED -- Detached
    */
    obj.cs_attach_state = array[dataPointer++];
    /**<   Packet-switched domain attach state of the mobile. Values: \n
       - 0x00 -- PS_UNKNOWN  -- Unknown or not applicable \n
       - 0x01 -- PS_ATTACHED -- Attached \n
       - 0x02 -- PS_DETACHED -- Detached
    */
    obj.ps_attach_state = array[dataPointer++];

    // UARFCN  case wcdma
    if (obj.cell_info_list[0] &&
        obj.cell_info_list[0].cellInfoType === WCDMA) {
      obj.uarfcn = array[dataPointer];
    }

    dump("handleData ok, dataPointer = " + dataPointer);
    return obj;
  }
};

NetworkInfo.init();
