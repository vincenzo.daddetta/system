/**
 * Created by xa on 7/31/14.
 */
$('menuItem-logcat').addEventListener('click', function()
{
  em_LogcatSettings();
});
var LogcatStatus = $('LogcatStatus');
var pwdckd = false;
function em_LogcatSettings() {
  function init() {
    var engmodeEx = navigator.engmodeExtension;
    if (engmodeEx) {
      var value = engmodeEx.readRoValue('persist.sys.tcllog.enable');
      if ('0' == value || '' == value) {
        $('logcat-input').checked = false;
        passwdstaus(false);
      }
      else {
        $('logcat-input').checked = true;
        passwdstaus(true);
      }
    }
    $('logcat-input').addEventListener('change', function(evt)
    {
      var passwd = $('passwd').value;
      if(('helpme' == passwd) || ('435763' == passwd) ||
         (true == pwdckd)){
        var input = evt.target;
        var value = input.checked;
        passwdstaus(value);
        $('passwd').value = '';
        debug('logcat-input value  ' + value);
        if (value) {
          LogcatStatus.textContent = 'Logcat auto running...!';
          setLogcatStatus(value);
        } else {
          LogcatStatus.textContent = 'Logcat Closed!';
          setLogcatStatus(value);
        }

      } else {
        $('logcat-input').checked = false;
        alert('Invalid password!!!\n Try Again');
      }
    });
  }
  function passwdstaus(value) {
    var pwditme = $('pwditem');
    pwdckd = value;
    if(value){
      pwditme.style.display='none';
    } else {
      pwditme.style.display='block';
    }
  }
  function setLogcatStatus(value) {
    var engmodeEx = navigator.engmodeExtension;
    var autoCommand;
    if (value) {
      autoCommand = '1';
    } else {
      autoCommand = '0';
    }
    if (engmodeEx) {
      var initRequest = engmodeEx.setPropertyLE('bg_service', autoCommand);
      initRequest.onsuccess = function() {
        if (value)
          alert('Logcat auto run, Open!');
        else
          alert('Logcat auto run, Closed!');
      };
    }
  }

  return init();
}