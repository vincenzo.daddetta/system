/* © 2017 KAI OS TECHNOLOGIES (HONG KONG) LIMITED All rights reserved.
 * This file or any portion thereof may not be reproduced or used in any manner
 * whatsoever without the express written permission of KAI OS TECHNOLOGIES
 * (HONG KONG) LIMITED. KaiOS is the trademark of KAI OS TECHNOLOGIES (HONG KONG)
 * LIMITED or its affiliate company and may be registered in some jurisdictions.
 * All other trademarks are the property of their respective owners.
 */
/* global TestItem */

'use strict';

const MMITESTRESULT = 'mmitest_result';

var TestResult = new TestItem();

TestResult.onInit = function() {
  this.content = document.getElementById('test-result-content');
  this.content.focus();
  this.showTestResult();
};

TestResult.showTestResult = function() {
  asyncStorage.getItem(MMITESTRESULT, (value) => {
    var testResult = JSON.parse(value);
    var result = '';

    if (testResult) {
      for (var name in testResult.items) {
        var colorClass;
        if (testResult.items[name] === 'pass') {
          colorClass = 'result-pass';
        } else if (testResult.items[name] === 'fail') {
          colorClass = 'result-fail';
        } else {
          colorClass = 'result-notest';
        }
        result += '<p class="' + colorClass + '">'
            + name + ': ' + testResult.items[name] + '</p>' ;
      }

      this.content.innerHTML = result;
    }
  });
};

TestResult.onHandleEvent = function(evt) {
  return false;
};

window.addEventListener('load', TestResult.init.bind(TestResult));
window.addEventListener('beforeunload', TestResult.uninit.bind(TestResult));
window.addEventListener('keydown', TestResult.handleKeydown.bind(TestResult));
