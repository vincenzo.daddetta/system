'use strict';

//script of logmanager app
(function (window, undefined) {

  //log position key
  const LOG_POSITION_KEY = 'logPosition';

  //log position
  var logPosition;

  var engmodeExtension = navigator.engmodeExtension;
  var isfileExist = false;
  var DEBUG = 1;
  var isProcessInitating = false;
  var _settings = window.navigator.mozSettings;
  function debug(s) {
    if (DEBUG) {
      dump('<LM_LOG> -*- logmanager -*-: ' + s + '\n');
    }
  }
  //keys in bugreport.html,dmesg.html,logcat.html,qxdm.html,tombstones.html
  //the pages have the similar structure
  //maybe need some configuration
  const LOG_TYPE_KEYS = ['tcpdump', 'logcat', 'qxdm', 'dmesg', 'initlog',
    'versioninfo', 'settingsapnvalue', 'nvvalue', 'propvalue',
    'clearlogs', 'tombstones', 'bugreport', 'crashlog'];

  const LOG_INIT_KEYS = ['dmesg', 'qxdm', 'logcat', 'tcpdump'];

  var self = this || window;
  //get element by id
  function $(id) {
    return document.getElementById(id);
  }

  function sleep(executeFunc, millisec) {
    let timer = null;
    let promise = new Promise((resolve) => {
      timer = setTimeout(() => {
        executeFunc();
        resolve();
      }, millisec);
    });

    promise.then(() => {
      clearTimeout(timer);
    });
  }

  function loadJSON(href, callback) {
    if (!callback) {
      return;
    }
    var xhr = new XMLHttpRequest();
    xhr.onerror = function() {
      console.error('Failed to fetch file: ' + href, xhr.statusText);
    };
    xhr.onload = () => {
      callback(xhr.response);
    };
    xhr.open('GET', href, true); // async
    xhr.responseType = 'json';
    xhr.send();
  }

  //get radio selected value
  function getRadioValue(name) {
    var radios = document.getElementsByName(name);
    for (var i = 0; i < radios.length; i++) {
      if (radios[i].checked) {
        return radios[i].value;
      }
    }
  }

  //set the radio value by it's name
  function setRadioValue(name, value) {
    var checkedRadios;
    var radios = document.getElementsByName(name);
    for (var i = 0; i < radios.length; i++) {
      if (radios[i].value === value) {
        radios[i].checked = 'checked';
        checkedRadios = radios[i];
      } else {
        radios[i].checked = '';
      }
    }
  }

  function getCustlogStatus(key) {
    if ('mms' === key) {
      var strCommand = 'mms.debugging.enabled';
      var value = engmodeExtension.readRoValue(strCommand);
      return ('true' === value);
    } else if ('fota' === key) {
      const FOTA_LOG_KEY = 'persist.sys.fotalog.enabled'
      var request = _settings.createLock().get(FOTA_LOG_KEY);
      request.onsuccess = function() {
        var enabled =  request.result[FOTA_LOG_KEY];
        $('fotalog-input').checked = enabled;
        return enabled;
      };
    }
  }

  function setCustlogStatus(key, value) {
    if ('mms' === key) {
      var strCommand = 'mms_debugging';
      if (engmodeExtension) {
        var initRequest = engmodeExtension.setPropertyLE(strCommand, value);
        initRequest.onsuccess = function() {
          debug('mms_debugging request success');
        };
      }
    } else if ('fota' === key) {
      var keystr = 'persist.sys.fotalog.enabled';
      var cset = {};
      cset[keystr] = value;
      _settings.createLock().set(cset);
    }
  }

  function getdiagcfgStatus() {
    var value = engmodeExtension.readRoValue('persist.sys.kaiosdiagcfg.enable');
    return ('1' === value);
  }

  function setdiagcfgStatus(value) {
    var autoCommand = 'kaios_diagcfg';
    if (engmodeExtension) {
      var mValue;
      mValue = value ? '1' : '0'
      var initRequest = engmodeExtension.setPropertyLE(autoCommand, mValue);
      initRequest.onsuccess = function() {
        debug('setdiagcfgStatus kaios_diagcfg request success');
      };
    }
  }

  function checkfilesExist(path) {
    var req = engmodeExtension.checkIsFileExist(path);
    req.onsuccess = function(e) {
      if ('EXIST' == e.target.result) {
        isfileExist = true;
      } else if ('NO_EXIST' == e.target.result) {
        isfileExist = false;
      }
    }
  }

  //include saving last modification time and position
  function onSuccess(key, request) {
    if (undefined == request.result[key]) {
      debug(JSON.stringify(request));
      return;
    }

    var date = request.result[key].timeStr;
    var relPosition = request.result[key].location;
    $(key + 'MdfTime').innerHTML = date;
    $(key + 'Position').innerHTML = relPosition;
    $(key + 'MdfTime').style.opacity = 1;
    $('loading').style.display = 'none';

    debug(JSON.stringify(request.result[key]));
  }

  function addfocuslist() {
    window.setTimeout(function() {
      var focusElements = document.querySelectorAll('[data-type="focusEnabled"]');
      debug('gaolu addfocuslist focusElements = ' + focusElements.toString());
      for (var item in focusElements) {
        if ('object' == typeof(focusElements[item])) {
          focusElements[item].lmFocus = true;
          focusElements[item].tabIndex = 1;
          debug('gaolu addfocuslist ' + item + ' = ' + focusElements[item].id);
          focusElements[0].focus();
        }
      }
    }, 200);
  }

  window.addEventListener('keydown',
    function initaddEventListerner(e) {
      var cmd = ConvertToCmd(e.key);
      debug('gaolu logmanager e.key = ' + e.key);
      if (!e.key) {
        return;
      }
      switch (e.key) {
        case 'Enter':
          debug('gaolu 1 cmd = ' + cmd)
          if (('lmcmYes' == cmd) || ('lmcmOk' == cmd)) {
            let input = e.target.querySelector('input:not([name="logPosition"])');
            if (input) {
              input.click();
            } else {
              e.target.click();
            }
            debug('gaolu logmanager el.name = ' + e.target.id);
          }
          break;
        case 'BrowserBack':
          debug('gaolu 2 cmd = ' + cmd);
          if ('lmcmNo' == cmd) {
            var isOpened = parent.document.body.classList.contains('pageChange');
            if (!isOpened) {
              debug('gaolu 2 click back to home ');
              window.close();
            } else {
              debug('gaolu 2 back ');
              parent.document.getElementById('window').style.display = 'block';
              window.setTimeout(function openTestPage() {
                var mvalue = document.getElementById('home2');
                dump('gaolu::document.mvalue id = ' + mvalue.id + ' value = ' + mvalue.value);
                parent.document.body.classList.remove('pageChange');
                parent.document.getElementById('nextWindow').style.display = 'none';
                parent.document.getElementById('nextFrame').src = '';

                dump('gaolu:document.activeElement ' + document.activeElement.id);
                if (null != mvalue) {
                  parent.document.getElementById(mvalue.value).focus();
                } else {
                  parent.document.getElementById('logPositionitem0').focus();
                }
                dump('gaolu after : document.activeElement ' + document.activeElement.id);
              }, 200);
            }
          }
          break;
        default:
          break;
      }
    });

  function init() {
    logPosition = getPosition();
    setRadioValue('logPosition', logPosition);
    for (var i = 0; i < LOG_TYPE_KEYS.length; i++) {
      if ($('window_' + LOG_TYPE_KEYS[i])) {
        //excute when page init
        var key = LOG_TYPE_KEYS[i];
        refeshfiletimes(key);
        if (!engmodeExtension) {
          break;
        }
        if ('qxdm' === key || 'logcat' === key ||
          'dmesg' === key || 'tcpdump' === key ||
          'initlog' === key) {
          var strPersist = 'persist.sys.kaios' + key + '.enable';
          var value = engmodeExtension.readRoValue(strPersist);
          if ('0' == value || '' == value) {
            $('loading').style.display = 'none';
            $(key + 'Run').innerHTML = 'Run';
          } else {
            debug('logmanager init loading...');
            window.setTimeout(function openTestPage() {
              $('loading').style.display = 'block';
              $(key + 'Run').innerHTML = 'Stop';
            }, 300);
          }
          if ('logcat' === key) {
            setProps('debug.console.enabled', true);
            setProps('wifi.debugging.enabled', true);
            var mmslogsStatus = getCustlogStatus('mms');
            $('mmslog-input').checked = mmslogsStatus
            $('mmslog-input').addEventListener('change', function(evt) {
              var value = evt.target.checked;
              setCustlogStatus('mms', value);
            });
            getCustlogStatus('fota');
            $('fotalog-input').addEventListener('change', function(evt) {
              var value = evt.target.checked;
              setCustlogStatus('fota', value);
            });
          }
          if ('qxdm' === key) {
            var diagcfgStatus = getdiagcfgStatus();
            $('diagcfg-input').checked = diagcfgStatus;
            if (diagcfgStatus) {
              $('diagcomm').innerHTML =
                'diag_mdlog -f /sdcard/diag.cfg';
            }
            checkfilesExist('/sdcard/diag.cfg');
            $('diagcfg-input').addEventListener('change', function(evt) {
              var value = evt.target.checked;
              if (true == value) {
                if (true == isfileExist) {
                  $('diagcomm').innerHTML =
                    'diag_mdlog -f /sdcard/diag.cfg';
                  setdiagcfgStatus(value);
                } else {
                  $('diagcfg-input').checked = false;
                  alert('The customized diag.cfg is\'t exit');
                }
              } else {
                $('diagcomm').innerHTML = 'diag_mdlog -f /vendor/diag.cfg';
                setdiagcfgStatus(value);
              }
            });
          }
          if ('initlog' === key) {
            let value = engmodeExtension.readRoValue('persist.sys.initkeys.enable');
            if (value && value !== '') {
              debug('current persist.sys.initkeys.enable = ' + value);
              $(value + 'input').checked = true;
            }
          }
        }
      }
    }
  }

  //save the log position
  function savePosition() {
    logPosition = getRadioValue('logPosition');
    localStorage.setItem(LOG_POSITION_KEY, logPosition);
  }

  //get the log position
  function getPosition() {
    var value = localStorage.getItem(LOG_POSITION_KEY);
    debug('getPosition value : ' + value);
    if (!value) {
      value = 'INTERNAL_SDCARD';   //change phone to internal_sdcard bug #641051
    }
    return value;
  }

  //bound the event of tags when page init
  function fireEvent() {
    if (!engmodeExtension) {
      debug('engmode Extension is not support.');
    }

    //transition when page change
    function changePage(name) {
      $('nextFrame').src = name + '.html';
      $('nextWindow').style.display = 'block';
      window.setTimeout(function openTestPage() {
        document.body.classList.add('pageChange');
        $('window').style.dislply = 'none';
        window.scrollTo(0, 0);
      }, 200);
    }

    //congqb@tcl.com add for external sdcard check
    function changePageOrAlert(name) {
      debug('changePageOrAlert');
      var mPosition = getPosition();
      //change phone to internal_sdcard && sdcard to external_sdcard bug #641051
      if (mPosition === 'INTERNAL_SDCARD') {
        changePage(name);
      } else if (mPosition === 'EXTERNAL_SDCARD') {
        debug('external-sdcard-branch');
        isExternalSdCardAvailable(name);
      } else {
        alert('neither select EXTERNAL_SDCARD nor INTERNAL_SDCARD');
      }
    }

    //congqb@tcl.com add for external sdcard check
    function isExternalSdCardAvailable(name) {
      var storages = navigator.getDeviceStorages('sdcard');
      debug('storages.length : ' + storages.length);

      function isExternalSdcard(element, index, array) {
        return (element.storageName === 'sdcard');
      }

      var index = storages.findIndex(isExternalSdcard);
      debug('storage--index : ' + index);
      if (index == -1 || storages.length < 2) {
        alert('The External SDcard on your device is not available');
      } else {
        debug('index--branch ');
        var request = storages[index].available();
        debug('request : ' + JSON.stringify(request));
        request.onsuccess = (e) => {
          // The result is a string
          if (e.target.result === 'available') {
            debug('The External SDCard on your device is available');
            changePage(name);
          } else if (e.target.result === 'unavailable') {
            changePage(name);
          } else {
            alert('The External SDCard on your device is ' +
              'shared and thus not available');
          }
        };
        request.onerror = function() {
          alert('Unable to get the space used by the External SDCard ');
        };
      }
    }

    let menus = Array.prototype.slice.call(
      document.querySelectorAll('li.contentBar'));

    if (menus.length > 0) {
      // Add click listeners to all list items with "href" attribute.
      menus.forEach((item) => {
        item.addEventListener('click', (evt) => {
          evt.preventDefault();
          let anchor = item.querySelector('a');
          if (anchor) {
            let name = anchor.getAttribute('href').replace('#', '');
            changePageOrAlert(name);
          }
        });
      });
    }

    //when click the radio button in index page
    //it will changes the log position of the system and save the status
    if ($('logPositionitem0')) {
      $('logPositionitem0').addEventListener('click', function() {
        $('logPosition0').checked = 'checked';
        debug('gaolu radio1 checked');
        savePosition();
      });
    }
    if ($('logPositionitem1')) {
      $('logPositionitem1').addEventListener('click', function() {
        $('logPosition1').checked = 'checked';
        debug('gaolu radio2 checked');
        savePosition();
      });
    }

    //when click the home button in all sub pages
    if ($('home')) {
      $('home').addEventListener('click', function() {
        parent.document.getElementById('window').style.display = 'block';
        window.setTimeout(function openTestPage() {
          var isOpened = parent.document.body.classList.contains('pageChange');
          if (!isOpened)
            return;
          var mvalue = document.getElementById('home2');
          debug('gaolu::document.mvalue id = ' + mvalue.id + ' value = ' + mvalue.value);
          parent.document.body.classList.remove('pageChange');
          parent.document.getElementById('nextWindow').style.display = 'none';
          parent.document.getElementById('nextFrame').src = '';

          debug('gaolu::::::::::::::::::::: document.activeElement ' + document.activeElement.id);
          if (null != mvalue) {
            parent.document.getElementById(mvalue.value).focus();
          } else {
            parent.document.getElementById('logPositionitem0').focus();
          }

          debug('gaolu after : document.activeElement ' + document.activeElement.id);
        }, 200);
      });
    }
    //do something when click run button and save the time when success do this
    for (var i = 0; i < LOG_TYPE_KEYS.length; i++) {
      if ($(LOG_TYPE_KEYS[i] + 'Run')) {
        var key = LOG_TYPE_KEYS[i];
        debug('fireEvent for LOG_TYPE_KEYS[' + i + '] = ' + LOG_TYPE_KEYS[i]);
        $(key + 'Run').addEventListener('click', function() {
          if (!isProcessInitating) {
            isProcessInitating = true;
            window.setTimeout(function() {
              isProcessInitating = false;
            }, 1500);
          } else {
            return;
          }
          switch ($(key + 'Run2').value) {
            case 'tcpdump':
            case 'logcat':
            case 'qxdm':
            case 'dmesg':
              lmlogKeyinfo(key);
              break;
            case 'initlog':
              initloginfo(key);
              break;
            case 'versioninfo':
              beginruning(key);
              Versioninfo(key);
              break;
            case 'settingsapnvalue':
              beginruning(key);
              SettingsAPN(key);
              break;
            case 'nvvalue':
              var SETTINGS_CHECKLIST = '/shared/resources/toolboxcustitems.json';
              beginruning(key);
              NvSettingsProp(key, SETTINGS_CHECKLIST, 'NV');
              break;
            case 'propvalue':
              var SETTINGS_CHECKLIST = '/shared/resources/toolboxcustitems.json';
              beginruning(key);
              NvSettingsProp(key, SETTINGS_CHECKLIST, 'Prop');
              break;
            case 'clearlogs':
              beginruning(key);
              clearalllogs(key);
              break;
            default:
              onlyRun(key);
              break;
          }
        });
      }
    }

    var oldClick = '';
    //call lm extention method run
    function callLMRun(key, type, position) {
      debug('callLMRun key = ' + key + ' type = ' + type);
      var mDevice = getStoragedevice();
      var path = mDevice + '/' + key;
      var request = engmodeExtension.createFileLE('DIRECTORY', path);
      request.onsuccess = function(e) {
        onSuccess(key, request);
      };
      request.onerror = function(e) {
        debug('error:' + JSON.parse(request.error.name).errorInfo + '\n');
        var errorType = JSON.parse(request.error.name).errorType;
        if (errorType == 'SourceFileNotExist') {
          alert('Log file is not exist!');
        } else if (errorType == 'IllegalFilePathToStoreLog') {
          alert('Store log to a wrong place');
        }
        $('loading').style.display = 'none';
        $(key + 'Run').onclick = oldClick;
      };
    }

    //click button only need run process
    function onlyRun(key) {
      oldClick = $(key + 'Run').onclick;
      $(key + 'Run').onclick = '';
      $('loading').style.display = 'block';
      $(key + 'MdfTime').style.opacity = 0;
      if (engmodeExtension) {
        debug('begin:' + $(key + 'Run2').value + ' position:' + logPosition);
        callLMRun(key, $(key + 'Run2').value, logPosition);
      }
    }
  }

  function getTimes() {
    var tempDate = new Date();
    var date_str = tempDate.toLocaleFormat('%Y%m%d_%H%M%S');
    return date_str;
  }

  function beginruning(key) {
    if ($(key + 'Run').innerHTML === 'Run') {
      $('loading').style.display = 'block';
      $(key + 'Run').innerHTML = 'Stop';
    } else {
      $('loading').style.display = 'none';
      $(key + 'Run').innerHTML = 'Run';
      refeshfiletimes(key);
    }
  }

  function refeshfiletimes(key) {
    let mDevice = getStoragedevice();
    let keyastr = '';

    switch (key) {
      case 'qxdm':
        keyastr = 'diag';
        break;
      case 'initlog':
        let value = engmodeExtension.readRoValue('persist.sys.initkeys.enable');
        if (value && value != '') {
          keyastr = value;
        }
        break;
      default:
        keyastr = key;
        break;
    }

    let filepath = mDevice + '/' + key + '/' + keyastr + '*';
    debug('Get the generated file time:' + filepath);
    engmodeExtension.getFilesLastTime('normal', [filepath], 1).then((result) => {
      $(key + 'MdfTime').innerHTML = result[keyastr].timeStr;
      $(key + 'Position').innerHTML = result[keyastr].location;
      let startButton = $(key + 'Run');
      switch (key) {
        case 'tombstones':
        case 'crashlog':
          startButton.innerHTML = 'Copy';
          break;
        case 'bugreport':
          startButton.innerHTML = 'Create';
          break;
        default:
          startButton.innerHTML = 'Run';
          break;
      }
      $('loading').style.display = 'none';
    });
  }

  function initloginfo(key) {
    if ($(key + 'Run').innerHTML === 'Run') {
      $('loading').style.display = 'block';
      $(key + 'Run').innerHTML = 'Stop';
      if (engmodeExtension) {
        let i = 0,
          length = LOG_INIT_KEYS.length;
        for (i; i < length; i++) {
          let logKey = LOG_INIT_KEYS[i];
          if ($(logKey + 'input').checked) {
            var autoCommand = 'kaios_initkeys';
            // Set init log storage location path
            let fileType = 'DIRECTORY';
            let path = getStoragedevice() + '/' + key + '/' + logKey;
            if ('qxdm' !== logKey) {
              fileType = 'FILE';
              path = path + '_' + getTimes() + '.log';
            }

            engmodeExtension.setPropertyLE(autoCommand, logKey).then(() => {
              debug('Set init log enable value: 1');
              return engmodeExtension.setKAIOSLogPara(key, ['enable', '1'], 2);
            }).then(() => {
              debug('Set init log location: ' + path);
              return engmodeExtension.setKAIOSLogPara(key, ['path', path], 2);
            }).then(() => {
              debug('Create the init log ' + fileType + ', path: ' + path);
              engmodeExtension.createFileLE(fileType, path);

              // XXX: Because the current API "engmodeExtension.createFileLE" would not return a
              // callback sometimes, we should use the timeout function to restart device.
              // If we create the file or folder failed, we would create the file or folder again
              // after the device was staring.
              // We have already set the log storage location path value into device.
              sleep(engmodeExtension.execCmdLE(['kaiosreboot'], 1), 3000);
            }).catch((err) => {
              debug('Set init log info failed: ' + err.name + ', ' + err.message);
            });
          }
        }
      }
    } else {
      $('loading').style.display = 'none';
      $(key + 'Run').innerHTML = 'Run';
      setProps('debug.console.enabled', false);
      setProps('wifi.debugging.enabled', false);
      if (engmodeExtension) {
        engmodeExtension.setKAIOSLogPara(key, ['enable', '0'], 2).then(() => {
          engmodeExtension.execCmdLE(['data_kaioslog_upper'], 1);
        });
        refeshfiletimes(key);
      }
    }
  }

  function lmlogKeyinfo(key) {
    var mDevice = getStoragedevice();
    var time = getTimes();
    var strPersist = 'persist.sys.kaios' + key + '.enable';
    if ($(key + 'Run').innerHTML === 'Run') {
      $('loading').style.display = 'block';
      $(key + 'Run').innerHTML = 'Stop';
      var path;
      if (key === 'qxdm') {
        path = mDevice + '/' + key;
        engmodeExtension.createFileLE('DIRECTORY', path);
      } else {
        path = mDevice + '/' + key + '/' + key + time + '.log';
        engmodeExtension.createFileLE('FILE', path);
      }
      var parmArray = new Array();
      parmArray.push('path');
      parmArray.push(path);
      var initPathRequest = engmodeExtension.setKAIOSLogPara(key, parmArray, 2);
      initPathRequest.then((result) => {
        if (engmodeExtension) {
          var parmArray = new Array();
          parmArray.push('enable');
          parmArray.push('1');
          var initRequest = engmodeExtension.setKAIOSLogPara(key, parmArray, 2);
          initRequest.onsuccess = function() {
            debug('start lmlogKeyinfo');
          };
        }
      }).catch((error) => {});
    } else {
      $('loading').style.display = 'none';
      $(key + 'Run').innerHTML = 'Run';
      setProps('debug.console.enabled', false);
      setProps('wifi.debugging.enabled', false);

      if (engmodeExtension) {
        var parmArray = new Array();
        parmArray.push('enable');
        parmArray.push('0');
        var initRequest = engmodeExtension.setKAIOSLogPara(key, parmArray, 2);
        initRequest.onsuccess = function() {
          debug('stop lmlogKeyinfo');
        };
      }
      refeshfiletimes(key);
    }
  }

  //get Versioninfo
  function getStoragedevice() {
    var mPosition = getPosition();
    if (mPosition === 'INTERNAL_SDCARD') {
      return '/data/usbmsc_mnt/logmanager';
    } else if (mPosition === 'EXTERNAL_SDCARD') {
      return '/sdcard/logmanager';
    }
  }

  var Versioninfo = function(key) {
    function getVersionInfo() {
      var mResult = '';
      var mDevice = getStoragedevice();
      var time = getTimes();
      var path = mDevice + '/' + key + '/' + key + time + '.log';
      engmodeExtension.createFileLE('FILE', path);

      var mVersionInfo = {
        'info': [
          {'key': 'System: ', 'value': '/system/system.ver'},
          {'key': 'Boot: ', 'value': '/boot.ver'},
          {'key': 'Data: ', 'value': '/data/userdata.ver'},
          {'key': 'Recovery: ', 'value': '/data/recovery.ver'},
          {'key': 'Modem: ', 'value': '/data/modem.ver'},
          {'key': 'SBL1: ', 'value': '/proc/modem_sbl'},
          {'key': 'TZ: ', 'value': '/proc/modem_tz'},
          {'key': 'RPM: ', 'value': '/proc/modem_rpm'},
          {'key': 'Cmdline: ', 'value': '/proc/cmdline'},
          {'key': 'Secro: ', 'value': '/proc/secro'},
          {'key': 'Study: ', 'value': '/data/study.ver'},
          {'key': 'Custpack: ', 'value': '/custpack/custpack.ver'},
          {'key': 'Tuning: ', 'value': '/proc/tuning'}
        ]
      };

      if (engmodeExtension) {
        var i;
        mResult = 'Version info:\n';
        var mVInfo = mVersionInfo.info;
        for (i = 0; i < mVInfo.length - 1; i++) {
          var value = engmodeExtension.fileReadLE(mVInfo[i].key);
          if (0 < value.length) {
            if ('\n' == value.substr(value.length - 1)) {
              mResult += mVInfo[i].key + value;
            } else {
              mResult += mVInfo[i].key + value + '\n';
            }
          }
        }
        debug('version info = ' + mResult);
        engmodeExtension.fileWriteLE(mResult, path, 'f');
        refeshfiletimes(key);
      }
    }

    getVersionInfo();
  };

  //get SettingsApn
  function SettingsAPN(key) {
    function getSettingsAPN() {
      var mDevice = getStoragedevice();
      var time = getTimes();
      var path = mDevice + '/' + key + '/' + key + time + '.log';
      engmodeExtension.createFileLE('FILE', path);
      var _settings = window.navigator.mozSettings;
      var mKey = 'ril.data.apnSettings';
      var req = _settings.createLock().get(mKey);
      req.onerror = req.onsuccess = function() {
        var value = req.result[mKey];
        var s = getobjstrItemsStr(value);
        debug(' SettingsAPN = ' + s);
        engmodeExtension.fileWriteLE('getSettingsAPN:\n' + s, path, 'f');
        refeshfiletimes(key);
      };

      function getobjstrItemsStr(o) {
        var r = [], i, j = 0, len;
        if (null == o) {
          return o;
        }
        if (typeof o == 'string') {
          return o;
        }
        if (typeof o == 'object') {
          if (!o.sort) {
            for (i in o) {
              if (typeof o[i] != 'function') {
                if (typeof o[i] != 'object') {
                  r[j++] = '';
                  r[j++] = i;
                  r[j++] = ' = ';
                  r[j++] = getobjstrItemsStr(o[i]);
                }
              }
            }
          } else {
            r[j++] = '';
            for (i = 0, len = o.length; i < len; ++i) {
              r[j++] = obj2str(o[i]);
              r[j++] = ',';
            }
            //maybe null
            r[len == 0 ? j : j - 1] = '\n';
          }
          return r.join('');
        }
        return o.toString();
      }

      function obj2str(o) {
        var r = [], i, j = 0, len;
        if (null == o) {
          return o;
        }
        if (typeof o == 'string') {
          return o;
        }
        if (typeof o == 'object') {
          if (!o.sort) {
            for (i in o) {
              if (typeof o[i] != 'function') {
                if (typeof o[i] != 'object') {
                  r[j++] = '\n';
                  r[j++] = i;
                  r[j++] = ' = ';
                }
                r[j++] = obj2str(o[i]);
              }
            }
          } else {
            r[j++] = '\n';
            for (i = 0, len = o.length; i < len; ++i) {
              r[j++] = obj2str(o[i]);
              r[j++] = ',';
            }
            r[len == 0 ? j : j - 1] = '';
          }
          return r.join('');
        }
        return o.toString();
      }
    }

    getSettingsAPN();
  }

  function NvSettingsProp(key, CHECKLIST, type) {
    debug('NvSettingsProp type = ' + type);
    var strSettingsInfo = 'SettingsInfo: \n';
    var strNvInfo = 'NVInfo: \n';
    var mDevice = getStoragedevice();
    var _settings = window.navigator.mozSettings;

    function getinfo(key, type) {
      debug('getinfo type = ' + type);
      loadJSON(CHECKLIST, function(data) {
        if (data) {
          if ('Prop' === type) {
            var time = getTimes();
            var path = mDevice + '/' + key + '/' + key + time + '.log';
            engmodeExtension.createFileLE('FILE', path);
            //var dataprefs = data.prefs;
            settingsinfo(path, _settings, data, 0);
          }
          if ('NV' === type) {
            var time = getTimes();
            var path = mDevice + '/' + key + '/' + key + time + '.log';
            engmodeExtension.createFileLE('FILE', path);
            nvinfo(path, data, 0);
          }
        }
      });
    }

    function settingsinfo(path, _settings, data, i) {
      //-------Step 1 get Settings value;--------
      debug('settingsinfo data = ' + data);
      var req = _settings.createLock().get('*');
      req.onsuccess = function(e) {
        var result = req.result;
        strSettingsInfo += '\n All of Settings Prop\n';
        for (var attr1 in result) {
          var defaultValue = engmodeExtension.readJson(attr1);
          strSettingsInfo += 'key:           ' + attr1 + '\n';

          strSettingsInfo += 'Default Value: ';
          var resultstr = defaultValue;
          if (typeof resultstr == 'object') {
            for (var attr2 in resultstr) {
              var tmp = resultstr[attr2]
              if (typeof resultstr == 'object') {
                for (var attr3 in tmp) {
                  strSettingsInfo += attr3 + ':';
                  strSettingsInfo += tmp[attr3] + '; ';
                }
              } else if (typeof tmp != 'function') {
                strSettingsInfo += attr2 + ':';
                strSettingsInfo += tmp + '; ';
              }
            }
            strSettingsInfo += '\n';
          } else {
            strSettingsInfo += resultstr + '\n';
          }

          strSettingsInfo += 'True Value:    ';
          var resultstr = result[attr1];
          if (typeof resultstr == 'object') {
            for (var attr2 in resultstr) {
              var tmp = resultstr[attr2]
              if (typeof resultstr == 'object') {
                for (var attr3 in tmp) {
                  strSettingsInfo += attr3 + ':';
                  strSettingsInfo += tmp[attr3] + '; ';
                }
              } else if (typeof tmp != 'function') {
                strSettingsInfo += attr2 + ':';
                strSettingsInfo += tmp + '; ';
              }
            }
            strSettingsInfo += '\n';
          } else {
            strSettingsInfo += resultstr + '\n';
          }
        }
        strSettingsInfo += '\n Custmization of Settings Prop\n';
        var datasettings = data.settings;
        for (var attr in datasettings) {
          var keystr = datasettings[attr].key;
          strSettingsInfo += 'Key = ' + keystr + '\n';
          strSettingsInfo += 'Value = ' + result[keystr] + '\n';
          strSettingsInfo += 'DefValue = ' + datasettings[attr].defvalue + '\n';
          strSettingsInfo += 'description = ' +
            datasettings[attr].description + '\n';
        }
        buildprops(path, data);
      };
    }

    function buildprops(path, data) {
      //-------Step 2 get Props value;--------
      debug('buildprops data = ' + data);
      var databuildprops = data.buildprops;
      strSettingsInfo += 'Props:\n';

      for (var attr in databuildprops) {
        var mKey = databuildprops[attr].key;
        strSettingsInfo += '\n Key = ' + mKey + '\n';
        strSettingsInfo += 'Value = ' + engmodeExtension.readRoValue(mKey);
        strSettingsInfo += 'DefValue = ' + databuildprops[attr].defvalue + '\n';
        strSettingsInfo += 'description = ' +
          databuildprops[attr].description + '\n';
      }
      engmodeExtension.fileWriteLE(strSettingsInfo, path, 'f');
      refeshfiletimes(key);
    }

    function nvinfo(path, nv, i) {
      var mKey = parseInt(nv[i].id);
      var req = engmodeExtension.readNvitemEx(mKey);

      req.onerror = req.onsuccess = function() {
        var nvResultArr = null;
        if (null != req.result) {
          nvResultArr = req.result.join();
        }
        strNvInfo += '\nNV Item = ' + nv[i].id + '\n';
        strNvInfo += 'Path = ' + nv[i].path + '\n';
        strNvInfo += 'Defvalue = ' + nv[i].defvalue + '\n';
        strNvInfo += 'Value = ' + nvResultArr + '\n';

        if (i < nv.length - 1) {
          nvinfo(path, nv, i + 1);
        } else if (i == prop.length - 1) {
          engmodeExtension.fileWriteLE(strNvInfo, path, 'f');
          refeshfiletimes(key);
        }
      };
    }

    getinfo(key, type);
  }

  function clearalllogs(key) {
    var mDevice = getStoragedevice();
    var autoCommand = 'delete_kaioslog_file';

    var parmArray = new Array();
    parmArray.push(autoCommand);
    parmArray.push(mDevice + '/*');

    var request = engmodeExtension.execCmdLE(parmArray, 2);
    request.onsuccess = function(e) {
      $(key + 'MdfTime').style.opacity = 1;
      $(key + 'Run').innerHTML = 'Run';
      $('loading').style.display = 'none';
      $('clearlogcontent').innerHTML = 'Command:<br>' +
        'Removed the logs in' + mDevice;
    };
    request.onerror = function() {
      debug('error:' + JSON.parse(request.error.name).errorInfo + '\n');
    };
  }

  function setProps(key, value) {
    var _settings = window.navigator.mozSettings;
    var cset = {};
    cset[key] = value;
    _settings.createLock().set(cset);
  }

  init();
  addfocuslist();
  fireEvent();

})(window);
