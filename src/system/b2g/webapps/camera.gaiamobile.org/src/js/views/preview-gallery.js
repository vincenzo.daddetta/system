/* jshint -W098 */
define(['require','debug','lib/panzoom','lib/orientation','MediaFrame','lib/bind','attach','view'],function(require) {


/**
 * Dependencies
 */

var debug = require('debug')('view:preview-gallery');
var addPanAndZoomHandlers = require('lib/panzoom');
var orientation = require('lib/orientation');
var MediaFrame = require('MediaFrame');
var bind = require('lib/bind');
var attach = require('attach');
var View = require('view');

/**
 * Constants
 */

var SWIPE_DISTANCE_THRESHOLD = window.innerWidth / 3; // pixels
var SWIPE_VELOCITY_THRESHOLD = 1.0;                   // pixels/ms

var SWIPE_DURATION = 250;   // How long to animate the swipe
var FADE_IN_DURATION = 500; // How long to animate the fade in after swipe
var ZOOM_LEVEL_MAX = 9;
var ZOOM_LEVEL_MIN = 0;
/**
 * Locals
 */

navigator.hasFeature('device.capability.volume-key').then((hasVolumeKey) => {
  window.hasVolumeKey = hasVolumeKey;
});

return View.extend({
  name: 'preview-gallery',
  className: 'offscreen',
  curFocusIndex: 0,
  mode: 'preview',   // 'preview', 'zoom', 'options', 'delete'
  type: 'photo',
  fullscreen: false,
  volumeIsAdjusting: false,

  initialize: function(options) {
    this.rtl = options.rtl;
    this.onKeyDown = this.onKeyDown.bind(this);
    debug('initialized');
  },

  render: function() {
    this.el.innerHTML = this.template();
    this.els.frameContainer = this.find('.js-frame-container');
    this.els.mediaFrame = this.find('.js-media-frame');
    this.els.countText = this.find('.js-count-text');

    this.els.optionMenu = this.find('.js-preview-menu');
    this.els.optionList = this.find('.js-preview-menu ul');
    this.els.volumeItem = this.find('.js-volume-item');
    this.els.shareItem = this.find('.js-share-item');
    this.els.deleteItem = this.find('.js-delete-item');
    this.els.wallPaperItem = this.find('.js-set-as-wallpaper');
    this.els.dialog = this.find('.js-confirm-dialog');
    this.els.dialogContent = this.find('.js-confirm-dialog p');
    this.els.optionMenu.setAttribute('role', 'heading');
    this.els.optionMenu.setAttribute('aria-labelledby', 'previewOptions');
    this.els.left = this.find('.controls-left .softkey');
    this.els.middle = this.find('.controls-middle');
    this.els.right = this.find('.controls-right .softkey');
    setTimeout(() => { this.el.focus(); }, 0);

    var self = this;
    this.els.volumeItem.selected = function() {
      self.sendOut('volume', 'show');
    };
    this.els.shareItem.selected = function() {
      self.sendOut('share');
    };
    this.els.deleteItem.selected = function() {
      self.sendOut('delete');
    };
    this.els.wallPaperItem.selected = () => { this.changeToSetwallpaper()};

    // Configure the MediaFrame component
    this.configure();

    // Clean up
    delete this.template;

    debug('rendered');
    return this.bindEvents();
  },

  bindEvents: function() {
    bind(this.el, 'keydown', this.onKeyDown);

    // The standard accessible control for sliders is arrow up/down keys.
    // Our screenreader synthesizes those events on swipe up/down gestures.
    return this;
  },

  configure: function() {
    this.currentIndex = this.lastIndex = 0;

    this.frame = new MediaFrame(this.els.mediaFrame, true, this.maxPreviewSize,true);
    this.frame.video.onloading = this.onVideoLoading;
    this.frame.video.onplaying = this.onVideoPlaying;
    this.frame.video.onpaused = this.onVideoPaused;
    this.frame.video.onFooterShow = this.onVideoFooterShow;
    this.frame.video.onFooterHide = this.onVideoFooterHide;
    this.frame.video.onOptionsShow = this.onVideoOptionsShow;
    this.frame.video.onDeleteConfirm = this.onVideoDeleteConfirm;
    addPanAndZoomHandlers(this.frame, this.swipeCallback);
  },

  onKeyDown: function(e) {
    if (this.exitFullScreen(e.key)) {
      e.preventDefault();
      return;
    }

    switch (e.key) {
      case 'ArrowLeft':
        if (this.mode === 'zoom') {
          this.move('left');
        } else {
          if (!this.frame.video.fullScreen && !this.frame.video.playing) {
            this.emit('swipe', 'left');
          }
        }
        break;
      case 'ArrowRight':
        if (this.mode === 'zoom') {
          this.move('right');
        } else {
          if (!this.frame.video.fullScreen && !this.frame.video.playing) {
            this.emit('swipe', 'right');
          }
        }
        break;
      case 'ArrowUp':
        if (this.mode === 'zoom') {
          this.move('up');
        } else if (this.mode === 'options') {
          this.next(false, 'current');
        } else if (this.mode === 'preview' && this.volumeIsAdjusting) {
          this.sendOut('volume', 'up');
        } else {
          if (!window.hasVolumeKey && this.type === 'video') {
            if (this.videoPlaying) {
              this.frame.video.pause();
            }
            this.sendOut('volume', 'show');
          }
        }
        break;
      case 'ArrowDown':
        if (this.mode === 'zoom') {
          this.move('down');
        } else if (this.mode === 'options') {
          this.next(true, 'current');
        } else if (this.mode === 'preview' && this.volumeIsAdjusting) {
          this.sendOut('volume', 'down');
        } else {
          if (!window.hasVolumeKey && this.type === 'video') {
            if (this.videoPlaying) {
              this.frame.video.pause();
            }
            this.sendOut('volume', 'show');
          }
        }
        break;
      case 'SoftLeft':
        if (this.mode === 'zoom') {
          this.zoom('out');
        } else if (this.mode === 'options') {
          //
        } else if (this.mode === 'delete') {
          this.hideDeleteConfirm();
        } else if (this.mode === 'wallpaper') {
          this.returnFromWallpaper();
        } else if (this.mode === 'preview') {
          if (this.type === 'photo') { this.startZoom(); }
          if (this.type === 'video') { this.enterFullScreen(); }
        }
        break;
      case 'SoftRight':
        if (this.mode === 'zoom') {
          this.zoom('in');
        } else if (this.mode === 'options') {
          //
        } else if (this.mode === 'delete') {
          this.doDelete();
        } else if (this.mode === 'wallpaper') {
          this.doSetWallpaper();
        } else if (this.optionMenuDeleteShownOnly) {
          this.sendOut('delete');
        } else {
          this.showOptionsMenu();
        }
        break;
      case 'Backspace':
        e.preventDefault();
        if (this.mode === 'zoom') {
          this.exitZoom();
        } else if (this.mode === 'options') {
          this.hideOptionsMenu();
        } else if (this.mode === 'delete') {
          this.hideDeleteConfirm();
        } else if (this.mode === 'wallpaper') {
          this.returnFromWallpaper();
        } else {
          this.emit('click:back');
        }
        break;
      case 'Enter':
        e.stopPropagation();
        if (this.mode === 'preview') {
          if (this.type === 'video') { this.playOrPauseVideo(); }
        } else if (this.mode === 'options') {
          var item = this.find('.current');
          this.hideOptionsMenu();
          item.selected();
        }
        break;
    }
  },

  next(isNext, c) {
    var el = this.find(`.${c}`);
    var next = isNext ? el.nextElementSibling : el.previousElementSibling;
    if (!next) {
      next = isNext ? el.parentNode.firstElementChild : el.parentNode.lastElementChild;
    }
    el.classList.remove(c);
    next.classList.add(c);
    next.scrollIntoView(false);
  },

  optionsAction: function(type) {
    this.showOptionsMenu(type);
  },

  setMode(mode) {
    this.mode = mode;
    this.set('mode', mode);
    this.updateSoftkey(mode);
  },

  startZoom: function() {
    this.set('zoom', true);
    this.set('zoom-level', 0);
    this.setMode('zoom');

    if (this.frame && this.frame.displayingPreview) {
      this.frame.displayingPreview = false;
    }
  },

  exitZoom: function() {
    this.set('zoom', false);
    this.frame.reset();
    this.setMode('preview');
  },

  enterFullScreen: function() {
    if (this.videoPlaying && !this.fullscreen ) {
      this.fullscreen = true;
      this.set('fullscreen', true);
    }
  },

  exitFullScreen: function(key) {
    const KEYS = ['SoftLeft', 'Enter', 'SoftRight', 'Backspace',
                  'ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'];
    if (this.fullscreen && (!key || KEYS.indexOf(key) !== -1)) {
      this.fullscreen = false;
      this.set('fullscreen', false);
      return true;
    }

    return false;
  },

  updateSoftkey: function(mode) {
    mode = mode || this.mode;
    this.optionMenuDeleteShownOnly = app.inSecureMode
        && (((this.type === 'video') && hasVolumeKey && (mode === 'preview'))
            || ((this.type === 'photo') && (mode === 'preview')));
    const keys = {
      left: {
        'preview': 'zoom',
        'zoom': 'zoom-out',
        'options': 'none',
        'delete': 'cancel',
        'wallpaper': 'cancel'
      },
      middle: {
        'preview': 'none',
        'zoom': 'none',
        'options': 'select-button',
        'delete': 'none',
        'wallpaper': 'none'
      },
      right: {
        'preview': this.optionMenuDeleteShownOnly ? 'delete' : 'options',
        'zoom': 'zoom-in',
        'options': 'none',
        'delete': 'delete',
        'wallpaper': 'set'
      }
    };

    this.updateL10nId(this.els.left, keys.left[mode]);
    this.updateL10nId(this.els.middle, keys.middle[mode]);
    this.updateL10nId(this.els.right, keys.right[mode]);
  },

  updateL10nId(elem, id) {
    if (id) {
      elem.setAttribute('data-l10n-id', id);
    } else {
      elem.removeAttribute('data-l10n-id');
    }
  },

  changeToSetwallpaper: function() {
    this.setMode('wallpaper');
    this.els.countText.setAttribute('hidden', 'true');
  },

  returnFromWallpaper: function() {
    this.setMode('preview');
    this.els.countText.removeAttribute('hidden');
  },

  zoom: function(direction) {
    var frame = this.frame;
    if (!frame) return;

    var scale = frame.fit.scale;
    var scaleDelta = parseFloat(frame.image.getAttribute('data-scale-delta'));
    var zoomInTimes = parseInt(frame.image.getAttribute('data-zoom-in'));
    var zoomOutTimes = parseInt(frame.image.getAttribute('data-zoom-out'));

    if (direction === 'in') {
      if (zoomInTimes < ZOOM_LEVEL_MAX) {
        zoomInTimes++;
        zoomOutTimes--;
        scale += scaleDelta;
      }
    } else if (direction === 'out') {
      if (zoomOutTimes < ZOOM_LEVEL_MIN) {
        zoomInTimes--;
        zoomOutTimes++;
        scale -= scaleDelta;
      }
    }

    this.set('zoom-level', zoomInTimes);
    frame.image.setAttribute('data-zoom-in', zoomInTimes);
    frame.image.setAttribute('data-zoom-out', zoomOutTimes);
    frame.zoomFrame(scale, 0, 0, 200);
  },

  move: function(dir) {
    var dx = 0, dy = 0, diff = 30;
    switch (dir) {
      case 'up': dy += diff; break;
      case 'down': dy -= diff; break;
      case 'left': dx += diff; break;
      case 'right': dx -= diff; break;
    }
    this.frame.pan(dx, dy);
  },

  playOrPauseVideo: function() {
    if (this.videoPlaying) {
      this.frame.video.pause();
    } else {
      this.frame.video.play();
    }
  },

  template: function() {
    return `
      <div class="frame-container js-frame-container">
        <div class="media-frame js-media-frame"></div>
      </div>
      <div class="count-text js-count-text p-pri"></div>
      <div class="controls js-controls h5">
        <div class="controls-left">
          <div class="softkey" data-l10n-id="zoom"></div>
        </div>
        <div class="controls-middle csk" data-icon="play"></div>
        <div class="controls-right">
          <div class="softkey" data-l10n-id="options"></div>
        </div>
      </div>
      <div class="preview-menu js-preview-menu">
        <div class="menu-container">
          <h1 class="menu-title" id="previewOptions" data-l10n-id="options"></h1>
          <ul>
            <li class="js-volume-item option-item p-pri" tabindex="0"
                data-l10n-id="volume"></li>
            <li class="js-share-item option-item p-pri" tabindex="0"
                data-l10n-id="share"></li>
            <li class="js-set-as-wallpaper option-item p-pri" tabindex="0"
                data-l10n-id="set-as-wallpaper"></li>
            <li class="js-delete-item option-item p-pri" tabindex="0"
                data-l10n-id="delete"></li>
          </ul>
        </div>
      </div>
      <div class="confirm-dialog js-confirm-dialog">
        <div class="dialog-container">
          <p data-l10n-id="delete-photo?" />
        </div>
      </div>
    </div>
    `
  },

  swipeCallback: function(swipeAmount, swipeVelocity) {
    var self = this;

    if (swipeVelocity === undefined) {
      // If this is not the end of the gesture, then just move the media
      // frame left or right by the specified amount.
      this.els.frameContainer.style.transform =
        'translate(' + swipeAmount + 'px, 0)';
      return;
    }

    // If we were passed a velocity, then this is the end of the gesture
    // and we have to figure out whether we are going to go to the
    // next item, the previous item, or remain on the current item.
    var direction, translation;

    // Should we move to the previous item?
    if (swipeAmount > 0 && swipeVelocity > 0 &&     // Same sign and
        this.withinRightBound() &&                    // has previous item and
        (swipeAmount > SWIPE_DISTANCE_THRESHOLD ||  // distance big enough or
         swipeVelocity > SWIPE_VELOCITY_THRESHOLD)) // speed fast enough
    {
      direction = 'right';
      translation = '100%';
    }
    // Should we move to the next item?
    else if (swipeAmount <= 0 && swipeVelocity <= 0 &&
             this.withinLeftBound() &&
             (swipeAmount < -SWIPE_DISTANCE_THRESHOLD ||
              swipeVelocity < -SWIPE_VELOCITY_THRESHOLD)) {
      direction = 'left';
      translation = '-100%';
    }

    // If we're not moving either left or right, just animate the
    // item to its starting point and we're done.
    if (!direction) {
      animate('transform', 'translate(0,0)', SWIPE_DURATION);
      return;
    }

    // If we get here, we are going to slide the current item off the screen
    // and display the next or previous item.

    // First, stop the video if there is one and it is playing.
    if (this.videoPlaying) {
      this.onVideoPaused();
    }

    // Now animate the item off the screen
    animate('transform', 'translate(' + translation + ', 0)',
            SWIPE_DURATION,
            function() {
              // Once we're off screen ask the controller to
              // switch to the new image.
              self.emit('swipe', direction);

              // On the next redraw, put the frame back on the screen
              // but make it opaque
              window.requestAnimationFrame(function() {
                // Next, make the frame invisible
                // and put it back on the screen
                self.els.frameContainer.style.opacity = 0;
                self.els.frameContainer.style.transform = 'translate(0,0)';

                // Then on the frame after that, animate the opacity to
                // make it visible again.
                window.requestAnimationFrame(function() {
                  animate('opacity', 1, FADE_IN_DURATION);
                });
              });
            });

    // A helper function to animate the specified CSS property to the
    // specified value for the specified duration. When the animation
    // is complete, reset everything and call the done callback
    function animate(property, value, duration, done) {
      var e = self.els.frameContainer;
      e.addEventListener('transitionend', onTransitionEnd);
      e.style.transitionProperty = property;
      e.style.transitionDuration = duration + 'ms';
      e.style[property] = value;

      function onTransitionEnd() {
        e.removeEventListener('transitionend', onTransitionEnd);
        delete e.style.transitionProperty;
        delete e.style.transitionDuration;
        if (done) {
          done();
        }
      }
    }
  },

  sendOut: function(name, option) {
    if (this.videoPlaying) { return; }
    this.emit('click:' + name, option);
  },

  open: function() {
    window.addEventListener('resize', this.onResize);
    orientation.unlock();
    this.el.classList.remove('offscreen');
  },

  close: function() {
    window.removeEventListener('resize', this.onResize);
    orientation.lock();
    this.el.classList.add('offscreen');
    this.frame.clear();
  },

  updateCountText: function(current, total) {
    this.currentIndex = current;
    this.lastIndex = total;
    this.els.countText.textContent = (total - current + 1) + '/' + total;
    // this.els.countText.focus();

    // Ensure that the information about the number of thumbnails is provided to
    // the screen reader.
    this.els.mediaFrame.setAttribute('data-l10n-id', 'media-frame');
    this.els.mediaFrame.setAttribute('data-l10n-args', JSON.stringify(
      { total: total, current: current }));
  },

  onResize: function() {
    // And we have to resize the frame (and its video player)
    this.frame.resize();
    if (this.frame.displayingVideo) {
      this.frame.video.setPlayerSize();
    }
  },

  showImage: function(image) {
    this.frame.displayImage(
      image.blob,
      image.width,
      image.height,
      image.preview,
      image.rotation,
      image.mirrored);

    this.type = 'photo';
    this.set('video', false);
  },

  showVideo: function(video) {
    this.frame.displayVideo(
      video.blob,
      video.poster.blob,
      video.width,
      video.height,
      video.rotation);

    this.type = 'video';
    this.set('video', 'paused');
  },

  onVideoLoading: function() {
    this.emit('loadingvideo', 'loadingVideo');
  },

  onVideoPlaying: function() {
    if (this.videoPlaying) {
      return;
    }

    this.videoPlaying = true;
    this.els.countText.setAttribute('hidden', 'true');
    this.els.left.setAttribute('data-l10n-id', 'fullscreen');
    this.els.middle.setAttribute('data-icon', 'pause');
    this.emit('playingvideo');
    this.set('video', 'playing');
  },

  onVideoPaused: function() {
    if (!this.videoPlaying) {
      return;
    }

    this.videoPlaying = false;
    this.els.countText.removeAttribute('hidden');
    // Not that good, should move 'zoom' elsewhere
    this.els.left.setAttribute('data-l10n-id', 'zoom');
    this.els.middle.setAttribute('data-icon', 'play');
    this.set('video', 'paused');
    this.exitFullScreen();
  },

  onVideoFooterShow: function() {
    this.els.countText.setAttribute('hidden', 'true');
  },

  onVideoFooterHide: function() {
    this.els.countText.removeAttribute('hidden');
  },

  onVideoOptionsShow: function() {
    this.optionsAction('video');
  },

  onVideoDeleteConfirm: function() {
    this.sendOut('delete');
  },

  updateOptionsMenu: function(type) {
    var arrItems = Array.prototype.slice.call(this.els.optionList.children);
    var isContainWallPaperItem = arrItems.includes(this.els.wallPaperItem);
    var isContaincVolumeItem = arrItems.includes(this.els.volumeItem);
    if (type === 'video') {
      if (isContainWallPaperItem) {
        this.els.optionList.removeChild(this.els.wallPaperItem);
      }
      if (isContaincVolumeItem && hasVolumeKey) {
        this.els.optionList.removeChild(this.els.volumeItem);
      }
      if (!isContaincVolumeItem && !hasVolumeKey) {
        this.els.optionList.appendChild(this.els.volumeItem);
      }
    } else if (type === 'photo') {
      if (isContaincVolumeItem) {
        this.els.optionList.removeChild(this.els.volumeItem);
      }
      if (!isContainWallPaperItem) {
        this.els.optionList.appendChild(this.els.wallPaperItem);
      }
    }
    if (app.inSecureMode && arrItems.includes(this.els.shareItem)) {
      this.els.optionList.removeChild(this.els.shareItem);
    }
  },

  showOptionsMenu: function(type) {
    type = type || this.type;
    if (type === 'video') {
      this.frame.video.pause();
    }
    this.updateOptionsMenu(type);
    this.els.optionMenu.classList.add('visible');
    this.els.volumeItem.classList.remove('current');
    this.els.shareItem.classList.remove('current');
    this.els.deleteItem.classList.remove('current');
    this.els.wallPaperItem.classList.remove('current');
    this.els.optionList.children[0].classList.add('current');
    this.els.optionList.children[0].focus();
    this.setMode('options');
  },

  hideOptionsMenu: function() {
    this.curFocusIndex = 0;
    this.els.optionMenu.classList.remove('visible');
    this.setMode('preview');
    this.el.focus();
  },

  showDeleteConfirm: function() {
    this.els.dialog.classList.add('visible');
    this.els.dialogContent.setAttribute('data-l10n-id', `delete-${this.type}?`);
    this.setMode('delete');
  },

  hideDeleteConfirm: function() {
    this.els.dialog.classList.remove('visible');
    this.setMode('preview');
  },

  doDelete: function() {
    this.hideDeleteConfirm();
    this.emit('delete');
  },

  doSetWallpaper: function() {
    this.sendOut('setwallpaper');
    this.returnFromWallpaper();
  },

  withinRightBound: function() {
    if (this.rtl) {
      return this.currentIndex < this.lastIndex;
    }
    return this.currentIndex > 1;
  },

  withinLeftBound: function() {
    if (this.rtl) {
      return this.currentIndex > 1;
    }
    return this.currentIndex < this.lastIndex;
  },
});

});
